var gulp         = require('gulp'),
    sass         = require('gulp-sass');

var config = {
    src           : './web/css/source/style.scss',
    dest          : './web/css/'
};

var module_config = {
    src           : './web/css/source/modules.scss',
    dest          : './web/css/'
};


// Compile CSS
gulp.task('styles', function () {
    var stream = gulp
        .src([config.src])
        .pipe(sass().on('error', sass.logError));

    return stream
        .pipe(gulp.dest('./web/css/'));
});

gulp.task('module', function () {
    var stream = gulp
        .src([module_config.src])
        .pipe(sass().on('error', sass.logError));

    return stream
        .pipe(gulp.dest('./web/css/'));
});
gulp.task('watch', [ 'styles', 'module'], function (){
    // Other watchers
});