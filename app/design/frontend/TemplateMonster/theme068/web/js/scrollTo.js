define([
    'jquery',
    'slick',
    'domReady!'
], function ($) {
    (function () {
        var bannerSlick = $(".js-hook__banners-slick, .js-hook__blog-slick"),
            homepage = $('.cms-home');

        //Inits slick sliders for mobile
        function mobileOnlySlider() {
            bannerSlick.slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                loop: true,
                dots: true
            });
        }

        if (window.innerWidth < 768) {
            mobileOnlySlider();
        }

        $(window).resize(function () {
            if (window.innerWidth < 768) {
                if (!bannerSlick.hasClass('slick-initialized')) {
                    mobileOnlySlider();
                }
            } else {
                if (bannerSlick.hasClass('slick-initialized')) {
                    bannerSlick.slick('unslick');
                }
            }
        });

        $('*[data-link-scroll]').click(function (event) {
            event.preventDefault();
            var top = $($(this).attr('data-link-scroll')).position().top - 40;
            $('html, body').animate({scrollTop: top}, 1000);
        })

        // Scrolls to the section on click
        $('.js-hook__next-section-scroller').click(function (event) {
            event.preventDefault();

            var section = $('.' + $(this).attr('data-section'));
            $('html, body').animate({
                scrollTop: section.offset().top - 100
            }, 1000);

        })

        //Changes place of scroll button (on home)
        //Makes it scroll-top and scroll-down dependency from scroll place
        if(homepage.length){

            $(window).scroll(function() {
                var scroller = $('.js-hook__next-section-scroller'),
                    topSection = $('.hero-banner'),
                    mobileDevice = window.matchMedia( "(max-width: 768px)" );

                if(scroller.length){
                    var headerHeight = mobileDevice ? $('.page-header').height() : $('.navigation').height();
                    offset = topSection.offset().top + topSection.height() - headerHeight;

                    if ($(this).scrollTop() > offset) {
                        scroller.attr('data-section','page-wrapper').addClass('next-section-scroller--fixed');
                    } else {
                        scroller.attr('data-section','block-products-list').removeClass('next-section-scroller--fixed');
                    }
                }
            });
            
        }
    })();
});