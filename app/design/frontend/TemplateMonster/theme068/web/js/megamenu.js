define([
    'jquery',
    'domReady!'
], function($){

    //Hides submenu
    $('.submenu').mouseleave(function(){
        $(this).fadeOut(100).attr('aria-expanded', false).attr('aria-hidden', true);
    });

    $('body').click(function (e) {
        if( window.innerWidth <= 992 && $(event.target).is($(".megamenu-submenu > a.megamenu-submenu"))){
            e.preventDefault();
            $(event.target).parent().toggleClass('open');
        }
    });

    $(window).resize(function (e) {
        if( window.innerWidth <= 992) {
            $('.sm-header-nav-wrap').removeClass('isStuck').css({
                'position': 'static'
            })
        }
    })
});