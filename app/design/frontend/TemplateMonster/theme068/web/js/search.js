define([
    'jquery',
    'domReady!'
], function ($) {

    var blockSearch = $('.minisearch'),
        blockSearchInput = blockSearch.find('input'),
        headerSearchBtn = blockSearch.find('.js-hook__search-isStuck, .actions .search'),
        headerSearchControl = blockSearch.find('.field.search, .custom-search__form');

    headerSearchBtn.removeAttr('disabled');

    headerSearchBtn.click(function (event) {
        event.preventDefault();
        headerSearchBtn.toggleClass('close');
        headerSearchBtn.parent().prev().fadeToggle(200);
    });

    $('body, html').mouseup(function(e) {
        if($(e.target).parents('.minisearch').length === 0) {
            headerSearchBtn.removeClass('close');
            headerSearchControl.fadeOut(200);
        }
    });

    /* Sync input values to different block search */
    blockSearchInput.change(function (event) {
       blockSearchInput.val(event.target.value);
    });

    blockSearchInput.val(blockSearchInput.first().value);

});
