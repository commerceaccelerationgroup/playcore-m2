define([
    'jquery',
    'ofi',
    'domReady!'
], function ($) {
  /**
   * This function is object-fit-images polyfill initiation
   * @see {@link https://github.com/bfred-it/object-fit-images|documentation} for polyfill documentation
    */
  $(function () { objectFitImages() });
});
