define([
    'jquery',
    'domReady!'
], function ($) {

    var Form = function (target) {
        this.target = target;
        this.type = target.attr('id');
        this.API = '/rest/V1/endpoints/leads';
        this.data = {'newsletter_subscription': false};
    };

    Form.prototype = {
        loader: $('#page-preloader'),

        fillData: function () {
            this.target.serializeArray().forEach(function (item) {
                if(item.name === 'newsletter_subscription') {
                    this.data[item.name] = item.value === 'true';
                } else {
                    this.data[item.name] = item.value;
                }
            }.bind(this));

            if(this.data['first_name'] && this.data['last_name']) {
                this.data['name'] = this.data['first_name'] + ' ' + this.data['last_name'];
            }

            if(this.type === 'newsletter-validate-detail') {
                this.data['newsletter_subscription'] = true;
            }
        },

        /*
         * This function changes names for form fields to fit salesforce field names.
         */
        changeFormDataSF: function () {
            var unsubscribedCheckbox = this.target.find('input[name="00N4100000aSvep"]');

            this.target.find('input, select, textarea').each(function (i, element) {
                var currId = $(element).attr('id');
                if (currId) {
                    $(element).attr('name', currId);
                }
            }.bind(this));

            if(this.data['newsletter_subscription']) {
                unsubscribedCheckbox.removeAttr('checked').val('0');
            } else {
                unsubscribedCheckbox.attr('checked','checked').val('1');
            }

            this.target.find('input[name="retURL"]').val(window.location.hostname + '/thank-you');
        },

        submit: function () {
            this.loader.addClass('opacity').css({ opacity: 1, display:'block'});
            $.ajax({
                type: "POST",
                url: this.API,
                contentType: 'application/json',
                data: JSON.stringify(this.data)
            }).done(function (response) {
                this.loader.removeClass('opacity').css({ opacity: 0, display:'none'});
                if(response.status !== 'fail') {
                    this.changeFormDataSF();
                    this.target.submit();
                } else {
                    this.showError(response);
                }
            }.bind(this));
        },

        showError: function(response) {
            $('.page.messages')
                .html('')
                .append(
                    '<div class="messages"><div class="message message-error">'+ response['error_message'] +'</div></div>'
                );
        }
    };

    $('.sf-submit-btn').click(function (event) {
        var uiForm = $(event.target).parents('form');
        if (uiForm.valid()) {
            var form = new Form(uiForm);
            form.fillData();
            form.submit();
        }
    });

    $('.form input, select').keydown(function (event) {
        var form = $(this).parents('.form'),
            sfSubmitBtn = form.find('.sf-submit-btn');

        if(sfSubmitBtn.length > 0 && event.keyCode === 13) {
            event.preventDefault();
        }
    });
});