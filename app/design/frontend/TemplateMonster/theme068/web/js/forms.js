define([
    'jquery',
    'domReady!'
], function ($) {

    var select = $('select');

    toggleSelect(select);

    select.change(function () {
        toggleSelect($(this));
    });

    function toggleSelect(select) {
        if (select.val() !== '') {
            select.removeClass('placeholder');
        } else {
            select.addClass('placeholder');
        }
    }
});