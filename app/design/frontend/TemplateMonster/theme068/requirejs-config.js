var config = {
    paths: {

        slick:'js/slick.min',
        ofi: 'js/ofi.min'
    },
    shim: {
        slick: {
            deps: ['jquery']
        },
        ofi: {
            deps: ['jquery']
        }
    },
    deps: [
        "js/search",
        "js/scrollTo",
        "js/bugherd",
        "js/ObjectFitInit",
        "js/megamenu",
        "js/forms",
        "js/submit"
    ],
}
