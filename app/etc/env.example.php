<?php
return array (
  'backend' =>
  array (
    'frontName' => '<front_name>',
  ),
  'crypt' =>
  array (
    'key' => 'f62cd9e772fbacaddbd1af15e0006ae7',
  ),
  'session' =>
  array (
    'save' => 'redis',
    'redis' =>
    array (
      'host' => 'redis-server',
      'port' => '6379',
      'password' => '',
      'timeout' => '2.5',
      'persistent_identifier' => '',
      'database' => '5',
      'compression_threshold' => '2048',
      'compression_library' => 'gzip',
      'log_level' => '1',
      'max_concurrency' => '6',
      'break_after_frontend' => '5',
      'break_after_adminhtml' => '30',
      'first_lifetime' => '600',
      'bot_first_lifetime' => '60',
      'bot_lifetime' => '7200',
      'disable_locking' => '0',
      'min_lifetime' => '15552000',
      'max_lifetime' => '25920000',
    ),
  ),
  'db' =>
  array (
    'table_prefix' => '',
    'connection' =>
    array (
      'default' =>
      array (
        'host' => '<mysql_server>',
        'dbname' => '<mysql_db_name>',
        'username' => '<mysql_db_name>',
        'password' => '<mysql_db_name>',
        'model' => 'mysql4',
        'engine' => 'innodb',
        'initStatements' => 'SET NAMES utf8;',
        'active' => '1',
      ),
      'custom' =>
      array (
        'host' => '<mysql_server>',
        'dbname' => '<mysql_db_name>',
        'username' => '<mysql_db_name>',
        'password' => '<mysql_db_name>',
        'model' => 'mysql4',
        'engine' => 'innodb',
        'initStatements' => 'SET NAMES utf8;',
        'active' => '1'
      ),
    ),
  ),
  'resource' =>
  array (
    'default_setup' =>
    array (
      'connection' => 'default',
    ),
    'custom' => 
    array(
      'connection' => 'custom'
    ),
  ),
  'x-frame-options' => 'SAMEORIGIN',
  'MAGE_MODE' => 'developer',
  'cache_types' =>
  array (
    'config' => 1,
    'layout' => 1,
    'block_html' => 1,
    'collections' => 1,
    'reflection' => 1,
    'db_ddl' => 1,
    'eav' => 1,
    'customer_notification' => 1,
    'full_page' => 1,
    'config_integration' => 1,
    'config_integration_api' => 1,
    'translate' => 1,
    'config_webservice' => 1,
    'ec_cache' => 1,
    'compiled_config' => 1,
  ),
  'install' =>
  array (
    'date' => 'Fri, 15 Jun 2018 07:50:19 +0000',
  ),
  'cache' =>
  array (
    'frontend' =>
    array (
      'default' =>
      array (
        'backend' => 'Cm_Cache_Backend_Redis',
        'backend_options' =>
        array (
          'server' => 'redis-server',
          'database' => '3',
          'port' => '6379',
        ),
      ),
    ),
  ),
);
