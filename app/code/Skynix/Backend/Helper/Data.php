<?php
/**
 * Created by Skynix Team.
 * User: oleksii bulba
 * Date: 26.06.18
 * Time: 11:39
 */

namespace Skynix\Backend\Helper;


use Magento\Directory\Model\RegionFactory;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use \Magento\Framework\App\Config\ScopeConfigInterface;

class Data extends AbstractHelper
{
    const PATH_CONFIG_PHONE            = 'general/store_information/phone';
    const PATH_CONFIG_COUNTRY          = 'general/store_information/country_id';
    const PATH_CONFIG_REGION           = 'general/store_information/region_id';
    const PATH_CONFIG_POSTCODE         = 'general/store_information/postcode';
    const PATH_CONFIG_CITY             = 'general/store_information/city';
    const PATH_CONFIG_STREET_1         = 'general/store_information/street_line1';

    const PATH_CONFIG_FOOTER_COPYRIGHT = 'design/footer/copyright';

    /**
     *  @var WriterInterface
     */
    protected $configWriter;

    /**
     * @var RegionFactory
     */
    protected $regionFactory;

    public function __construct(
        Context $context,
        WriterInterface $configWriter,
        RegionFactory $regionFactory
    ) {
        $this->configWriter = $configWriter;
        $this->regionFactory = $regionFactory;
        parent::__construct($context);
    }


    /**
     * Sets default config value by path
     *
     * @param string $path
     * @param string $value
     */
    public function setDefaultConfig($path, $value)
    {
        $this->configWriter->save($path, $value, ScopeConfigInterface::SCOPE_TYPE_DEFAULT);
    }

    /**
     * Gets default config by path
     *
     * @param string $path
     * @return string
     */
    public function getDefaultConfig($path)
    {
        return $this->scopeConfig->getValue($path, ScopeConfigInterface::SCOPE_TYPE_DEFAULT);
    }

    /**
     * Returns Region id by Region Name
     *
     * @param $regionName
     * @param $countryId
     * @return int|string
     */
    public function getRegionIdByName($regionName, $countryId)
    {
        return $this->regionFactory
            ->create()
            ->loadByName($regionName, $countryId)
            ->getId();
    }

    public function getConfiguratorJson($array) {
        $result = ['', ''];
        for ($i = 0; $i < count($array); $i++) {
            $result[0] .= $i > 0 ? ',' : '';
            $result[0] .= '\\"' . $array[$i] . '\\":\\"' . ($i + 1) . '\\"';
            $result[1] .= $i > 0 ? ',' : '';
            $result[1] .= $array[$i];
        }
        return $result;
    }
}