<?php
/**
 * Created by Skynix Team.
 * User: oleksii bulba
 * Date: 20.06.18
 * Time: 14:10
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Skynix_Backend',
    __DIR__
);
