<?php
/**
 * Created by PhpStorm.
 * User: tetianadmytrenko
 * Date: 6/22/18
 * Time: 6:13 PM
 */

namespace Skynix\Backend\Block\Header;


use Magento\Framework\View\Element\Template;
use Skynix\Backend\Helper\Data;

class Phone extends Template
{

    /**
     * @var Data
     */
    protected $helper;

    public function __construct(
        Template\Context $context,
        Data $helper,
        array $data = []
    ) {
        $this->helper = $helper;
        parent::__construct($context, $data);
    }

    /**
     * Retrieves company phone from configuration
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->helper->getDefaultConfig(Data::PATH_CONFIG_PHONE);
    }
}