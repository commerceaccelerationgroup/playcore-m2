<?php
/**
 * Created by Skynix Team.
 * User: oleksii bulba
 * Date: 12.07.18
 * Time: 12:56
 */

namespace Skynix\Backend\Block\Html;


class Breadcrumbs extends \Magento\Theme\Block\Html\Breadcrumbs
{
    public function getCrumbs()
    {
        return $this->_crumbs;
    }

    public function getBaseUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl();
    }
}