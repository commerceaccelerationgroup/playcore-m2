<?php
/**
 * Created by Skynix Team.
 * User: oleksii bulba
 * Date: 23.08.18
 * Time: 16:07
 */

namespace Skynix\Backend\Block\Catalog\Product;


use Magento\Catalog\Block\Product\View;

/**
 * Product page title
 * @method setPageTitle(string $title)
 * @package Skynix\Backend\Block\Catalog\Product
 */
class Title extends View
{
    protected $_product;

    protected function _beforeToHtml()
    {
        $this->_product = $this->getProduct();
        if ($this->_product && $this->_product->getName()) {
            $this->setPageTitle($this->_product->getName());
        }
        return $this;
    }

}