<?php
/**
 * Created by Skynix Team.
 * User: oleksii bulba
 * Date: 04.07.18
 * Time: 12:03
 */

namespace Skynix\Backend\Block;


use Magento\Directory\Model\ResourceModel\Region\CollectionFactory;
use Magento\Framework\View\Element\Template;
use Skynix\Backend\Helper\Data;

class Footer extends Template
{
    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var CollectionFactory
     */
    protected $regionCollectionFactory;

    public function __construct(
        Template\Context $context,
        array $data = [],
        Data $helper,
        CollectionFactory $regionCollectionFactory
    ) {
        $this->helper = $helper;
        $this->regionCollectionFactory = $regionCollectionFactory;
        parent::__construct($context, $data);
    }

    public function getRegionNameById($regionId)
    {
        return $this->regionCollectionFactory
            ->create()
            ->addFieldToFilter('main_table.region_id', ['eq' => $regionId])
            ->getFirstItem()
            ->getName();
    }

    /**
     * Retrieves company region id from configuration
     *
     * @return string
     */
    public function getRegionId()
    {
        return $this->helper->getDefaultConfig(Data::PATH_CONFIG_REGION);
    }

    /**
     * Retrieves company city from configuration
     *
     * @return string
     */
    public function getCity()
    {
        return $this->helper->getDefaultConfig(Data::PATH_CONFIG_CITY);
    }

    /**
     * Retrieves company street from configuration
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->helper->getDefaultConfig(Data::PATH_CONFIG_STREET_1);
    }

    /**
     * Retrieves company postcode from configuration
     *
     * @return string
     */
    public function getPostcode()
    {
        return $this->helper->getDefaultConfig(Data::PATH_CONFIG_POSTCODE);
    }

    /**
     * Retrieves company phone from configuration
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->helper->getDefaultConfig(Data::PATH_CONFIG_PHONE);
    }

    /**
     * Retrieves company copyright message from configuration
     *
     * @return string
     */
    public function getCopyright()
    {
        return $this->helper->getDefaultConfig(Data::PATH_CONFIG_FOOTER_COPYRIGHT);
    }


}