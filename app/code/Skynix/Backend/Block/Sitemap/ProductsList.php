<?php
/**
 * Created by Skynix Team.
 * User: oleksii bulba
 * Date: 10.08.18
 * Time: 17:16
 */

namespace Skynix\Backend\Block\Sitemap;


use Magento\Catalog\Model\ResourceModel\Product\Collection as ProductCollection;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Framework\View\Element\Template\Context;
use Vsourz\HtmlSitemap\Block\ProductsList as VsourzProductsList;

class ProductsList extends VsourzProductsList
{
    const CATEGORIES_TO_HIDE = ['staff'];

    /**
     * @var CategoryFactory
     */
    protected $categoryFactory;

    /**
     * @var CategoryCollectionFactory
     */
    protected $categoryCollectionFactory;

    public function __construct(
        ProductCollection $collection,
        ProductCollectionFactory $productCollectionFactory,
        Context $context,
        CategoryFactory $categoryFactory,
        CategoryCollectionFactory $categoryCollectionFactory
    ) {
        parent::__construct($collection, $productCollectionFactory, $context);

        $this->categoryFactory = $categoryFactory;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
    }

    /**
     * Retrieves all products
     *
     * @return ProductCollection
     */
    public function getProductsAllCollection()
    {
        $this->_productsCollection = $this->filterProductsCollection(parent::getProductsAllCollection(), self::CATEGORIES_TO_HIDE);

        return $this->_productsCollection;
    }

    /**
     * Retrieves all products with name starts from $char or non-letter simbol (if $char === '#')
     *
     * @param $char
     * @return ProductCollection
     */
    public function getProductsCollection($char)
    {
        $this->_productsCollection = $this->filterProductsCollection(parent::getProductsCollection($char), self::CATEGORIES_TO_HIDE);

        return $this->_productsCollection;
    }

    /**
     * Filters product collection: hides all product from $categories categories
     *
     * @param ProductCollection $productsCollection
     * @param array $categories
     * @return ProductCollection
     */
    protected function filterProductsCollection(ProductCollection $productsCollection, $categories)
    {
        $hideCategoriesIds = $this->categoryCollectionFactory
            ->create()
            ->addAttributeToFilter('url_key', $categories)
            ->load()
            ->getLoadedIds();
        return $productsCollection->addCategoriesFilter(['nin' => [$hideCategoriesIds]]);
    }
}