<?php
/**
 * Created by Skynix Team.
 * User: oleksii bulba
 * Date: 15.08.18
 * Time: 12:42
 */

namespace Skynix\Backend\Block\Blog\Post;


use TemplateMonster\Blog\Block\Post\PostList as TemplateMonsterPostList;

class PostList extends TemplateMonsterPostList
{
    const DEFAULT_SORT_DIRECTION = 'desc';
}