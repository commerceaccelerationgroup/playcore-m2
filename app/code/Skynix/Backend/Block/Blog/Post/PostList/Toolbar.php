<?php
/**
 * Created by Skynix Team.
 * User: oleksii bulba
 * Date: 15.08.18
 * Time: 12:31
 */

namespace Skynix\Backend\Block\Blog\Post\PostList;


use Skynix\Backend\Block\Blog\Post\PostList;
use TemplateMonster\Blog\Block\Post\PostList\Toolbar as TemplateMonsterPostListToolbar;

class Toolbar extends TemplateMonsterPostListToolbar
{
    const DEFAULT_POSTS_LIMIT = 9;

    protected $_direction = PostList::DEFAULT_SORT_DIRECTION;

    public function getDefaultLimit()
    {
        return self::DEFAULT_POSTS_LIMIT;
    }

}