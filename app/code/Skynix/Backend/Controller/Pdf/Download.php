<?php
/**
 * Created by Skynix Team.
 * User: sergey marchuk
 * Date: 7/30/18
 * Time: 3:26 PM
 */

namespace Skynix\Backend\Controller\Pdf;
use Magento\Framework\Exception\NotFoundException;

use Magento\Framework\App\Filesystem\DirectoryList;

class Download extends \Magento\Framework\App\Action\Action
{
    const PATH_TO_FILE = 'pub/media/downloadable/process.pdf';

    protected $directory_list;
    protected $fileFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        DirectoryList $directory_list,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory
    ) {
        $this->directory_list = $directory_list;
        $this->fileFactory = $fileFactory;
        parent::__construct($context);
    }


    public function execute()
    {
        if(file_exists(self::PATH_TO_FILE)) {
            $pdf = file_get_contents(self::PATH_TO_FILE);

            header('Content-Type: application/pdf');
            header('Cache-Control: public, must-revalidate, max-age=0'); // HTTP/1.1
            header('Pragma: public');
            header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
            header('Content-Length: '.strlen($pdf));
            header('Content-Disposition: inline; filename="'. self::PATH_TO_FILE .'";');
            ob_clean();
            flush();
            echo $pdf;
        }else{
            //redirect to 404 page
            throw new NotFoundException(__('File not found.'));
        }
    }
}
