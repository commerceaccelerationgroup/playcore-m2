<?php
/**
 * Created by Skynix Team.
 * User: oleksii bulba
 * Date: 27.08.18
 * Time: 19:11
 */

namespace Skynix\Backend\Model\Catalog;


class Product extends \Magento\Catalog\Model\Product
{
    public function getCategoryId()
    {
        $categoryId = parent::getCategoryId();
        $categoryIds = $this->getCategoryIds();

        if (in_array($categoryId, $categoryIds)) {
            return $categoryId;
        }

        return $categoryIds[0];
    }

}