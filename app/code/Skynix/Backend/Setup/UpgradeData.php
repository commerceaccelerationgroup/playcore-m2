<?php
/**
 * Created by Skynix Team.
 * User: oleksii bulba
 * Date: 20.06.18
 * Time: 19:24
 */

namespace Skynix\Backend\Setup;

use Magento\Catalog\Api\CategoryLinkManagementInterface;
use Magento\Catalog\Helper\Category as CategoryHelper;
use Magento\Catalog\Helper\Product as ProductHelper;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\ResourceModel\Category\Collection as CategoryCollection;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;
use Magento\Cms\Model\Block;
use Magento\Cms\Model\BlockFactory;
use Magento\Cms\Model\Page;
use Magento\Cms\Model\PageFactory;
use Magento\Framework\App\State;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManager;
use Magento\UrlRewrite\Model\ResourceModel\UrlRewriteCollection;
use Magento\UrlRewrite\Model\ResourceModel\UrlRewriteCollectionFactory;
use Magento\UrlRewrite\Model\UrlRewrite;
use Skynix\Backend\Helper\Data;
use TemplateMonster\Blog\Helper\Data as TemplateMonsterHelperData;

class UpgradeData implements UpgradeDataInterface
{

    /**
     * @var CategoryFactory
     */
    protected $categoryFactory;

    /**
     * @var CategoryCollectionFactory
     */
    protected $categoryCollectionFactory;

    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * @var ProductCollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * @var CategoryLinkManagementInterface
     */
    protected $categoryLinkManagement;

    /**
     * @var StoreManager
     */
    protected $storeManager;

    /**
     * @var Store
     */
    protected $store;

    /**
     * @var Page
     */
    protected $page;

    /**
     * @var PageFactory
     */
    protected $pageFactory;

    /**
     * @var BlockFactory
     */
    protected $blockFactory;

    /**
     * @var State
     */
    protected $state;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var UrlRewriteCollectionFactory
     */
    protected $urlRewritesCollectionFactory;

    public function __construct(
        Page $page,
        PageFactory $pageFactory,
        BlockFactory $blockFactory,
        CategoryFactory $categoryFactory,
        CategoryCollectionFactory $categoryCollectionFactory,
        ProductFactory $productFactory,
        ProductCollectionFactory $productCollectionFactory,
        CategoryLinkManagementInterface $categoryLinkManagement,
        UrlRewriteCollectionFactory $urlRewriteCollectionFactory,
        StoreManager $storeManager,
        State $state,
        Data $helper
    ) {
        $this->page = $page;
        $this->pageFactory = $pageFactory;
        $this->blockFactory =$blockFactory;
        $this->categoryFactory = $categoryFactory;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->productFactory = $productFactory;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->categoryLinkManagement = $categoryLinkManagement;
        $this->urlRewritesCollectionFactory = $urlRewriteCollectionFactory;
        $this->storeManager = $storeManager;
        $this->state = $state;
        $this->helper = $helper;
    }

    /**
     * Upgrades data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '0.0.4') < 0) {
            $phone = '817-380-4215';
            $this->helper->setDefaultConfig(Data::PATH_CONFIG_PHONE, $phone);
        }
        if (version_compare($context->getVersion(), '0.0.5') < 0) {
            $aboutPageContent   = <<<ABOUT
<div class="about-content">
    <div class="about-content__section">
        <p class="about-content__paragraph">Worlds of Wow can assist with stage design, theming for various ages and stages, and any other design idea you may have. We provide original artwork and graphics to bring your ideas to life! With over 12 years experience and more than 400 completed projects, we are the experts when it comes to theming and play. Whether you are a multi-site, a mobile church or just giving your space a whole new look, we know what it takes from the very first conversation as we dream together to the very last nail put into place. We love the creative process of working with you through the development of concepts, characters, elements and stories. It’s just our role to bring this vision to life. Our themes are always original, never borrowed from check in, to hallways, to large group rooms, our themes are designed to tell a story and create destinations where it becomes the place kids want to be.</p>
        <p class="about-content__paragraph">Themes are different for each and every partner. At Worlds of Wow, we take you through a creative brief to fully understand your DNA, culture, vision and goals. Many of our staff have previously served in pastoral roles within the local body so we truly understand where you’re coming from. Our background in the theme park industry allows us to bring the coolest, newest play features to your location. We include several types of play attractions such as custom slides, rock climbing walls, soft contained play, interactives and more. Indoor playgrounds are a huge hit with everyone – from Pre-K to elementary-aged kids to their parents.</p>
        <p class="about-content__paragraph">These attractions are a wonderful and creative way to reach out to the community with a safe, fun destination for families all during the week. We design, fabricate and install soft contained, multi-level indoor play attractions for kids up to12 years old. We also make sure our projects are handicap accessible, meet ADA requirements and all the latest safety standards in the industry.</p>
        <p class="about-content__paragraph">We are one of the very few companies in the United States specializing in custom theming and play and we strive to have all of our materials are made in the USA! We know that when you get the kids, you get the parents, too. That’s why churches have us create interactive and engaging play attractions such as indoor soft playgrounds, slides, edutainment and more. We are excited to visit with you and create engaging ways to tell a story that illustrates your vision and mission on the walls of your buildings.</p>
        <p class="about-content__paragraph">Worlds of Wow is a great company to partner with when you are looking to transform a space that impacts kids and families.</p>
        <p class="about-content__paragraph">The truth is, we are all still kids at heart! So when we are invited into your space to begin dreaming with you, we can’t help ourselves the creative juices just start flowing and an original theme design comes to life.</p>
    </div>
    <div class="about-content__section">
        <h2 class="about-content__caption">Ever wonder how the WOW gets created?</h2>
        <p class="about-content__paragraph">That’s a question we love to answer! So much passion, time and talent goes into each and every project we get to be a part of. From the very first conversation to the final piece put into place, our team gives 100% each step of the way! When our customers sit back and take in the final masterpiece, it brings us tremendous joy to hear them say, “WOW” The great thing about working with Worlds of Wow! is that because we have such a talented and experienced team, our designs can be as simple or as over-the-top, realistic , illustrative, as 1-dimensional or 3-dimensional as you like. It truly depends on your heart’s desire for your space we just get to come along for the ride!</p>
        <p class="about-content__paragraph">When partnering with Wow we strive to exhibit these characteristics Expertise, Relationship, Focus, Technology, Professionalism, to reflect every phase on every project in every situation when partnering with Worlds of Wow.</p>
        <p class="about-content__paragraph">“How does someone start the process of working with Worlds of Wow?”</p>
    </div>
</div>
ABOUT;
            $termsPageContent   = <<<TERMS
<div class="terms-content">
    <div class="terms-content__section">
        <h3 class="terms-content__caption">1. Acceptance of Terms</h3>
        <p class="terms-content__paragraph">Worlds of Wow provides content and services within the domain and subdomains of worldsofwow.com (collectively referred to as the “site”). By accessing this site, you are agreeing to be bound by these Terms of Use, all applicable laws and regulations, and agree that you are responsible for compliance with any applicable local laws. If you do not agree with any of these terms, you are prohibited from using or accessing this site.</p>
        <p class="terms-content__paragraph">Any claim relating to Worlds of Wow’s web site shall be governed by the laws of the State of Texas without regard to its conflict of law provisions.</p>
    </div>

    <div class="terms-content__section">
        <h3 class="terms-content__caption">2. Disclaimer</h3>
        <p class="terms-content__paragraph">The content on Worlds of Wow’s web site is provided "as is." Worlds of Wow makes no warranties, expressed or implied, including without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights. Worlds of Wow does not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on its web site or to materials and sites linked to or from this site.</p>
        <p class="terms-content__paragraph">Further, the content on this website may contain technical, typographical, or photographic errors. Worlds of Wow does not guarantee any of the content on its website to be complete, accurate, or up-to-date. Accessing this site’s content, as well as downloading any contained or linked content is done at your own discretion. Worlds of Wow may update or revise site content at any time without notice, but does not commit to make such changes.</p>
    </div>

     
    <div class="terms-content__section">
        <h3 class="terms-content__caption">3. Limitations</h3>
        <p class="terms-content__paragraph">Neither Worlds of Wow, nor its affiliates, will be held liable for any damages due to the use or access, or inability to use or access, the content contained within this website. In jurisdictions where there are restrictions on implied warranties, or limitations of liability are not allowed, these limitations may not apply.</p>
    </div>

    <div class="terms-content__section">
        <h3 class="terms-content__caption">4. Copyright</h3>
        <p class="terms-content__paragraph">©2017 Worlds of Wow All Rights Reserved.</p>
        <p class="terms-content__paragraph">By accessing this site and its content acknowledge and agree that all content contained within the site is protected by copyrights, trademarks, service marks, patents, and other intellectual property laws. You are only permitted to use the content as authorized by us, or its specific provider.</p>
        <p class="terms-content__paragraph">Specifically, you may: temporarily download one copy of the content on Worlds of Wow's web site for personal, non-commercial transitory viewing only. This is the grant of a license, not a transfer of title.</p>
        <p class="terms-content__paragraph">Specifically, you may not: modify or copy the content, use for any commercial purpose or display, attempt to decompile or reverse engineer any software on Worlds of Wow’s site, remove any copyright or other notations from the content, nor transfer the content to another person or “mirror” the content to another server.</p>
    </div>
    
    <div class="terms-content__section">
        <h3 class="terms-content__caption">5. Privacy Policy</h3>
        <p class="terms-content__paragraph">This policy exists to inform you, the website visitor, how we (Worlds of Wow) collect and use information gained from the use of this site, and other means of communication.</p>
    </div>

    <div class="terms-content__section">
        <h3 class="terms-content__caption">6. Information We Collect</h3>
        <p class="terms-content__paragraph">Worlds of Wow collects data throughout the course of each visitor’s time on the site. This data includes:</p>
        <div class="terms-content__paragraph terms-bullets">
            <ul class="terms-bullets__list">
                <li>the domain from which you access the internet</li>
                <li>the date and time of your access to the pages within our site</li>
                <li>the address of the site you used to link to or access our site</li>
            </ul>
        </div>
        <p class="terms-content__paragraph">This information is collected for statistical purposes to asses what information on our site is most/least popular, what pages may need updated, and performance of our web site in comparison to previous and industry trends.</p>
        <p class="terms-content__paragraph">In addition, Worlds of Wow collects personally identifiable information when you specifically choose to provide such information. This personally identifiable information may be used for any of the following:</p>
        <div class="terms-content__paragraph terms-bullets">
            <ul class="terms-bullets__list">
                <li>to respond to specific requests for correspondence or materials</li>
                <li>to register visitors for contests, email subscriptions, surveys, and other online activities</li>
                <li>to fulfill prizes</li>
                <li>to send notices of promotions and offers from Worlds of Wow</li>
                <li>to process employment applications and/or resumés</li>
            </ul>
        </div>
    </div>
    
    <div class="terms-content__section">
        <h3 class="terms-content__caption">7. How Your Information May Be Shared</h3>
        <p class="terms-content__paragraph">Worlds of Wow will not sell, rent, loan, or transfer personally identifiable information except as noted in this policy.</p>
        <p class="terms-content__paragraph">Worlds of Wow may share personally identifiable information to parties outside of Worlds of Wow in the following circumstances:</p>
        <div class="terms-content__paragraph terms-bullets">
            <ul class="terms-bullets__list">
                <li>in the event of, or as part of preliminary due diligence related to, a corporate sale, merger, acquisition, or similar event</li>
                <li>working with third party companies to support the Site’s technical operation or execute a specific promotion or program (such as providing responses to any Comments Page, conduct surveys, or maintain a database of visitor information, etc. </li>
                <li>in connection with processing submitted resume and/or employment application, Worlds of Wow and its affiliates may disclose your Personal Information to any other third party necessary to further process your resume and/or employment application</li>
            </ul>
        </div>
    </div>

    <div class="terms-content__section">
        <h3 class="terms-content__caption"></h3>
        <p class="terms-content__paragraph">When choosing to Opt-In to digital communications from Worlds of Wow you are authorizing the use of your email address and other personally identifiable information to be used for the purposes of:</p>
        <div class="terms-content__paragraph terms-bullets">
            <ul class="terms-bullets__list">
                <li>notification of company and industry related news and events</li>
                <li>marketing and promotional messages about our company, products, services, and events</li>
            </ul>
        </div>
    </div>
</div>
TERMS;
            $privacyPageContent = <<<PAGE
<div class="privacy-content">
    <div class="privacy-content__section">
        <p class="privacy-content__paragraph">This privacy policy sets out how uses and protects any information that you give when you use this website. is committed to ensuring that your privacy is protected. Should we ask you to provide certain information by which you can be identified when using this website, then you can be assured that it will only be used in accordance with this privacy statement. may change this policy from time to time by updating this page. You should check this page from time to time to ensure that you are happy with any changes.</p>
    </div>
    
    <div class="privacy-content__section">
        <h2 class="privacy-content__caption">What we collect</h2>
        <p class="privacy-content__paragraph">We may collect the following information:</p>
        <div class="privacy-content__paragraph privacy-bullets">
            <ul>
                <li>name</li>
                <li>contact information including email address</li>
                <li>demographic information such as postcode, preferences and interests</li>
                <li>other information relevant to customer surveys and/or offers</li>
            </ul>
        </div>
        <p class="privacy-content__paragraph">For the exhaustive list of cookies we collect see the <a href="#list">List of cookies we collect</a> section.</p>
    </div>
    
    <div class="privacy-content__section">
        <h2 class="privacy-content__caption">What we do with the information we gather</h2>
        <p class="privacy-content__paragraph">We require this information to understand your needs and provide you with a better service,and in particular for the following reasons:</p>
        <div class="privacy-content__paragraph privacy-bullets">
            <ul class="privacy-bullets__list">
                <li>Internal record keeping.</li>
                <li>We may use the information to improve our products and services.</li>
                <li>We may periodically send promotional emails about new products, special offers or other information which we think you may find interesting using the email address which you have provided.</li>
                <li>From time to time, we may also use your information to contact you for market research purposes. We may contact you by email, phone, fax or mail. We may use the information to customise the website according to your interests.</li>
            </ul>
        </div>
    </div>
    
    <div class="privacy-content__section">
        <h2 class="privacy-content__caption">Security</h2>
        <p class="privacy-content__paragraph">We are committed to ensuring that your information is secure. In order to prevent unauthorised access or disclosure, we have put in place suitable physical, electronic and managerial procedures to safeguard and secure the information we collect online.</p>
    </div>
    
    <div class="privacy-content__section">
        <h2 class="privacy-content__caption">How we use cookies</h2>
        <p class="privacy-content__paragraph">A cookie is a small file which asks permission to be placed on your computer's hard drive. Once you agree, the file is added and the cookie helps analyse web traffic or lets you know when you visit a particular site. Cookies allow web applications to respond to you as an individual. The web application can tailor its operations to your needs, likes and dislikes by gathering and remembering information about your preferences.</p>
        <p class="privacy-content__paragraph">We use traffic log cookies to identify which pages are being used. This helps us analyse data about web page traffic and improve our website in order to tailor it to customer needs. We only use this information for statistical analysis purposes and then the data is removed from the system.</p>
        <p class="privacy-content__paragraph">Overall, cookies help us provide you with a better website, by enabling us to monitor which pages you find useful and which you do not. A cookie in no way gives us access to your computer or any information about you, other than the data you choose to share with us. You can choose to accept or decline cookies. Most web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. This may prevent you from taking full advantage of the website.</p>
    </div>
    
    <div class="privacy-content__section">
        <h2 class="privacy-content__caption">Links to other websites</h2>
        <p class="privacy-content__paragraph">Our website may contain links to other websites of interest. However, once you have used these links to leave our site, you should note that we do not have any control over that other website. Therefore, we cannot be responsible for the protection and privacy of any information which you provide whilst visiting such sites and such sites are not governed by this privacy statement. You should exercise caution and look at the privacy statement applicable to the website in question.</p>
    </div>
    
    <div class="privacy-content__section">
        <h2 class="privacy-content__caption">Controlling your personal information</h2>
        <p class="privacy-content__paragraph">You may choose to restrict the collection or use of your personal information in the following ways:</p>
        <div class="privacy-content__paragraph privacy-bullets">
            <ul class="privacy-bullets__list">
                <li>Whenever you are asked to fill in a form on the website, look for the box that you can click to indicate that you do not want the information to be used by anybody for direct marketing purposes</li>
                <li>If you have previously agreed to us using your personal information for direct marketing purposes, you may change your mind at any time by writing to or emailing us at owner@example.com</li>
            </ul>
        </div>
        <p class="privacy-content__paragraph">We will not sell, distribute or lease your personal information to third parties unless we have your permission or are required by law to do so. We may use your personal information to send you promotional information about third parties which we think you may find interesting if you tell us that you wish this to happen.</p>
        <p class="privacy-content__paragraph">You may request details of personal information which we hold about you under the Data Protection Act 1998. A small fee will be payable. If you would like a copy of the information held on you please write to .</p>
        <p class="privacy-content__paragraph">If you believe that any information we are holding on you is incorrect or incomplete, please write to or email us as soon as possible, at the above address. We will promptly correct any information found to be incorrect.</p>
    </div>
    
    <div class="privacy-content__section">
        <h2 class="privacy-content__caption"><a name="list"></a>List of cookies we collect</h2>
        <p class="privacy-content__paragraph">The table below lists the cookies we collect and what information they store.</p>
        <table class="privacy-table">
            <thead>
                <tr>
                    <th>COOKIE name</th>
                    <th>COOKIE Description</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>CART</th>
                    <td>The association with your shopping cart.</td>
                </tr>
                <tr>
                    <th>CATEGORY_INFO</th>
                    <td>Stores the category info on the page, that allows to display pages more quickly.</td>
                </tr>
                <tr>
                    <th>COMPARE</th>
                    <td>The items that you have in the Compare Products list.</td>
                </tr>
                <tr>
                    <th>CURRENCY</th>
                    <td>Your preferred currency</td>
                </tr>
                <tr>
                    <th>CUSTOMER</th>
                    <td>An encrypted version of your customer id with the store.</td>
                </tr>
                <tr>
                    <th>CUSTOMER_AUTH</th>
                    <td>An indicator if you are currently logged into the store.</td>
                </tr>
                <tr>
                    <th>CUSTOMER_INFO</th>
                    <td>An encrypted version of the customer group you belong to.</td>
                </tr>
                <tr>
                    <th>CUSTOMER_SEGMENT_IDS</th>
                    <td>Stores the Customer Segment ID</td>
                </tr>
                <tr>
                    <th>EXTERNAL_NO_CACHE</th>
                    <td>A flag, which indicates whether caching is disabled or not.</td>
                </tr>
                <tr>
                    <th>FRONTEND</th>
                    <td>You sesssion ID on the server.</td>
                </tr>
                <tr>
                    <th>GUEST-VIEW</th>
                    <td>Allows guests to edit their orders.</td>
                </tr>
                <tr>
                    <th>LAST_CATEGORY</th>
                    <td>The last category you visited.</td>
                </tr>
                <tr>
                    <th>LAST_PRODUCT</th>
                    <td>The most recent product you have viewed.</td>
                </tr>
                <tr>
                    <th>NEWMESSAGE</th>
                    <td>Indicates whether a new message has been received.</td>
                </tr>
                <tr>
                    <th>NO_CACHE</th>
                    <td>Indicates whether it is allowed to use cache.</td>
                </tr>
                <tr>
                    <th>PERSISTENT_SHOPPING_CART</th>
                    <td>A link to information about your cart and viewing history if you have asked the site.</td>
                </tr>
                <tr>
                    <th>POLL</th>
                    <td>The ID of any polls you have recently voted in.</td>
                </tr>
                <tr>
                    <th>POLLN</th>
                    <td>Information on what polls you have voted on.</td>
                </tr>
                <tr>
                    <th>RECENTLYCOMPARED</th>
                    <td>The items that you have recently compared.</td>
                </tr>
                <tr>
                    <th>STF</th>
                    <td>Information on products you have emailed to friends.</td>
                </tr>
                <tr>
                    <th>STORE</th>
                    <td>The store view or language you have selected.</td>
                </tr>
                <tr>
                    <th>USER_ALLOWED_SAVE_COOKIE</th>
                    <td>Indicates whether a customer allowed to use cookies.</td>
                </tr>
                <tr>
                    <th>VIEWED_PRODUCT_IDS</th>
                    <td>The products that you have recently viewed.</td>
                </tr>
                <tr>
                    <th>WISHLIST</th>
                    <td>An encrypted list of products added to your Wishlist.</td>
                </tr>
                <tr>
                    <th>WISHLIST_CNT</th>
                    <td>The number of items in your Wishlist.</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
PAGE;
            $faqsPageContent    = <<<FAQS
<div class="faq-content">

    <div class="faq-content__section">
        <h3 class="faq-content__question">What Does Wow Do?</h3>
        <p class="faq-content__answer-paragraph">
                We’re crazy-passionate about creating destinations to reach kids and families. We believe that church should be a fun place that kids want to go, so we design ridiculously cool, fun environments that impact kids and families of all ages! How we work with churches is unique, but straight forward. At the heart of Worlds of Wow is our experience in ministry (including former church staff) and our Kingdom-minded focus. We start by understanding the unique DNA of your church and ministry, think through priorities and options and end up with custom concepts and real-world budget numbers. In addition to amazing themes, Worlds of Wow also designs and fabricates indoor playground equipment for churches. Fact is, we’re the only company in the church market that can design, fabricate and install both themed environments and play attractions together. This provides easy one stop solution and budget savings!
        </p>
        <p class="faq-content__answer-paragraph">
                We have some of the most talented artists and play designers around. When combined, our themed play attractions become a kids’ favorite-thing-of-all-time. Having one team design, fabricate and install your theme and play attractions is a huge benefit because there’s a big cost savings of working with one company throughout your project, not to mention that cohesively-themed play environments are ridiculously cool for kids
        </p>
    </div>

    <div class="faq-content__section">
        <h3 class="faq-content__question">How Long Does it Take to Complete a Play Space?</h3>
        <p class="faq-content__answer-paragraph">Short answer: In as little as four months from start to finish for most projects. Long answer: We work on projects anywhere from 4–18 months prior to opening date. Typical artwork turnaround time is 2-3 months and installation is usually booked 3-6 months in advance. Most installations take 12-14 days depending on the size and scope of the project.</p>
        <p class="faq-content__answer-paragraph">Bonus: We can even have our crews work evenings to ensure you stay open during business hours.</p>
    </div>
   
    <div class="faq-content__section">
        <h3 class="faq-content__question">How Much Does a Playground Cost?</h3>
        <p class="faq-content__answer-paragraph">We work on projects from $20,000 to over $1M dollars. We value helping customers be good stewards of their resources. We always strive to put the maximum Wow factor into every play theme project!</p>
        <p class="faq-content__answer-paragraph">There are three factors in budget ranges:</p>
        <p class="faq-content__answer-paragraph">1. More is more. The more theming and/or play, the more it will cost.</p>
        <p class="faq-content__answer-paragraph">2. Artwork density. Rolling hills and blue skies are one thing; heavily themed, detailed artwork takes more time and costs more.</p>
        <p class="faq-content__answer-paragraph">3. How far does it come off the wall? Generally speaking, the more 3D layers or effects, the more it ‘comes off the wall’ and the more it costs.</p>
        <p class="faq-content__answer-paragraph">We work together with you right from the start so that what we design will fit your estimated budget with no surprises. While there are lots of budget range options, Worlds of Wow is highly competitive and, most importantly, the best value for custom theming.</p>
    </div>
   
    <div class="faq-content__section">
        <h3 class="faq-content__question">Can You Travel to *insert your city here*?</h3>
        <p class="faq-content__answer-paragraph">We’re a Texas-based company (Dallas/Fort Worth) but we have teams all over the country that travel anywhere. We also incorporate the latest technology to communicate and collaborate together throughout the project.</p>
        <p class="faq-content__answer-paragraph">We’ve done installations from Washington state to Florida and from California to New York and everywhere in between. We’re still looking forward to doing a project in Hawaii however.</p>
    </div>

    <div class="faq-content__section">
        <h3 class="faq-content__question">What Play/Theming Services Do You Offer?</h3>
        <p class="faq-content__answer-paragraph">
                We are a full turnkey resource able to handle all aspects of your project from beginning to end. We provide:
        </p>

        <div class="faq-content__answer-paragraph faq-bullets">
                <p class="faq-bullets__caption">Design:</p>
                <ul class="faq-bullets__list">
                    <li>Master Planning</li>
                    <li>Phasing Plan</li>
                    <li>Site & Schematic Design</li>
                    <li>Budget Allocation</li>
                    <li>Storyline Development</li>
                    <li>Concept Design</li>
                    <li>Signage</li>
                    <li>Original Art Design</li>
                    <li>Logo Development</li>
                </ul>
                <p class="faq-bullets__caption">Production:</p>
                <ul class="faq-bullets__list">
                    <li>Quality Assurance</li>
                    <li>3D Fabrication</li>
                    <li>2D Relief Fabrication</li>
                    <li>Color Selections</li>
                    <li>Site Inspections</li>
                    <li>Modular Play Attractions</li>
                    <li>Soft Foam Play</li>
                </ul>
                <p class="faq-bullets__caption">Installation:</p>
                <ul class="faq-bullets__list">
                    <li>Airbrushed Murals</li>
                    <li>Lighting</li>
                    <li>Play Attractions</li>
                </ul>
            </div>
    </div>
    <div class="faq-content__section">
        <h3 class="faq-content__question">Worlds of Wow is ‘green’ and enviro-friendly? Really?</h3>
        <p class="faq-content__answer-paragraph">It’s not just our predisposition for the color green; we’re literally going green with our environmentally-friendly changes and improvements. Here’s a fun take on describing the steps Worlds of Wow is making to go green.</p>
        <p class="faq-content__secondary-caption">Paperless Office.</p>
        <p class="faq-content__answer-paragraph">It seems the Internet may be the greatest single greatest tree-saving invention. With all of the electronic communication, project management and file sharing, we’ve gone almost 100% paperless. As a result, our office paper shredder is getting lonely.</p>
        <p class="faq-content__secondary-caption">Materials.</p>
        <p class="faq-content__answer-paragraph">We’re using recycled products in our manufacturing shops and changing to materials that are eco-friendly. The polystyrene we use for 3D sculpts doesn’t include Ozone CFC’s and HCFC’s. The medium density fiber board we use is phase 2 carb compliant and 100% recovered and recycled materials.</p>
        <p class="faq-content__secondary-caption">Paint.</p>
        <p class="faq-content__answer-paragraph">Even our paint is becoming eco-friendly, as we’ve switched all of our primers, airbrush paints and top coat paints to a water-borne acrylic paint and clear coat. We are proud to have implemented these initial steps to provide the same high-quality, huge WOW-factor environments with a bit more “earth friendly” spin!</p>
    </div>
</div>
FAQS;

            $pagesDataUpdate = [
                'about'                => [
                    'url_key'     => 'about',
                    'title'       => 'About',
                    'content'     => $aboutPageContent,
                    'page_layout' => '1column',
                    'store_id'    => [0]
                ],
                'faqs'                 => [
                    'url_key'     => 'faqs',
                    'title'       => 'FAQ\'s',
                    'content'     => $faqsPageContent,
                    'page_layout' => '1column',
                    'store_id'    => [0]
                ],
                'privacy-policy-cookie-restriction-mode'       => [
                    'url_key'     => 'privacy-policy-cookie-restriction-mode',
                    'title'       => 'Privacy Policy',
                    'content'     => $privacyPageContent,
                    'page_layout' => '1column',
                    'store_id'    => [0]
                ],
                'terms-and-conditions' => [
                    'url_key'     => 'terms-and-conditions',
                    'title'       => 'Terms and Conditions',
                    'content'     => $termsPageContent,
                    'page_layout' => '1column',
                    'store_id'    => [0]
                ],
            ];

            $this->updateCmsPages($pagesDataUpdate);

            $countryId = 'US';
            $regionId  = 57; // Texas region
            $city      = 'Denton';
            $street    = '1800 Shady Oaks Drive';
            $postCode  = '76205';
            $copyright = '&copy; 2018 Worlds of Wow!, A PlayCore Company. All Rights Reserved.';

            $this->helper->setDefaultConfig(Data::PATH_CONFIG_COUNTRY, $countryId);
            $this->helper->setDefaultConfig(Data::PATH_CONFIG_REGION, $regionId);
            $this->helper->setDefaultConfig(Data::PATH_CONFIG_CITY, $city);
            $this->helper->setDefaultConfig(Data::PATH_CONFIG_STREET_1, $street);
            $this->helper->setDefaultConfig(Data::PATH_CONFIG_POSTCODE, $postCode);
            $this->helper->setDefaultConfig(Data::PATH_CONFIG_FOOTER_COPYRIGHT, $copyright);
        }
        if (version_compare($context->getVersion(), '0.0.6') < 0) {
            $categoriesData = [
                'market'          => [
                    'url_key'          => 'market',
                    'name'             => 'Market',
                    'meta_title'       => 'Market',
                    'display_mode'     => Category::DM_PRODUCT,
                    'is_active'        => true,
                    'is_anchor'        => false,
                    'include_in_menu'  => true,
                    'mm_turn_on'       => 1,
                    'mm_css_class'     => 'menu-colored-item menu-colored-item--red megamenu-submenu',
                ],
                'facility'        => [
                    'url_key'          => 'facility',
                    'name'             => 'Facility',
                    'meta_title'       => 'Facility',
                    'display_mode'     => Category::DM_PRODUCT,
                    'is_active'        => true,
                    'is_anchor'        => false,
                    'include_in_menu'  => true,
                    'mm_turn_on'       => 1,
                    'mm_css_class'     => 'menu-colored-item menu-colored-item--blue megamenu-submenu',
                ],
                'themes'          => [
                    'url_key'          => 'themes',
                    'name'             => 'Themes',
                    'meta_title'       => 'Themes',
                    'display_mode'     => Category::DM_PRODUCT,
                    'is_active'        => true,
                    'is_anchor'        => false,
                    'include_in_menu'  => true,
                    'mm_turn_on'       => 1,
                    'mm_css_class'     => 'menu-colored-item menu-colored-item--green megamenu-submenu',
                ],
                'request_a_quote' => [
                    'url_key'          => 'request-a-quote',
                    'name'             => 'Request a quote',
                    'meta_title'       => 'Request a quote',
                    'display_mode'     => Category::DM_PAGE,
                    'is_active'        => true,
                    'is_anchor'        => false,
                    'include_in_menu'  => true,
                    'mm_turn_on'       => 0,
                    'mm_css_class'     => 'menu-colored-item menu-colored-item--yellow',
                ],
                'contact_us'      => [
                    'url_key'          => 'contacts',
                    'name'             => 'Contact',
                    'meta_title'       => 'Contact',
                    'display_mode'     => Category::DM_PAGE,
                    'is_active'        => true,
                    'is_anchor'        => false,
                    'include_in_menu'  => true,
                    'mm_turn_on'       => 0,
                    'mm_css_class'     => 'menu-colored-item menu-colored-item--black',
                ],
            ];
            $this->updateCategories($categoriesData);
            $this->fixProductWebsites($this->getProductCollection());
        }
        if (version_compare($context->getVersion(), '0.0.7') < 0) {
            $portfolioCategoryData = [
                'url_key'          => 'portfolio',
                'name'             => 'Portfolio',
                'meta_title'       => 'Portfolio',
                'display_mode'     => Category::DM_PRODUCT,
                'is_active'        => true,
                'is_anchor'        => false,
                'include_in_menu'  => false,
                'meta_description' => '',
                'description'      => '',
                'mm_turn_on'       => false,
            ];
            $productCategoriesData = [
                'church-play'                    => ['portfolio', 'market'],
                'pediatric-dental-office-design' => ['portfolio', 'market'],
                'family-entertainment-centers'   => ['portfolio', 'market'],
                'military-recreation-centers'    => ['portfolio', 'market'],
                'ymca-design'                    => ['portfolio', 'market'],
                'retail-centers'                 => ['portfolio', 'market'],
                'concept-services'               => ['portfolio', 'facility'],
                'theming'                        => ['portfolio', 'facility'],
                '3d-art-and-characters'          => ['portfolio', 'facility'],
                'digital-wallcoverings'          => ['portfolio', 'facility'],
                'airbrushing'                    => ['portfolio', 'facility'],
                'indoor-and-outdoor-playgrounds' => ['portfolio', 'facility'],
                'signage-and-logos'              => ['portfolio', 'facility'],
                'kids-stage-design'              => ['portfolio', 'facility'],
                'trees-nature-forest'            => ['portfolio', 'themes'],
                'trains-lanes-automobiles'       => ['portfolio', 'themes'],
                'ocean-aquatic'                  => ['portfolio', 'themes'],
                'castle-fairytale'               => ['portfolio', 'themes'],
                'animals'                        => ['portfolio', 'themes'],
                'geometric'                      => ['portfolio', 'themes'],
            ];

            // WOW-44 Create Portfolio Category
            $this->createCategory($portfolioCategoryData);
            $this->fixProductCategories($productCategoriesData);

            $this->helper->setDefaultConfig(TemplateMonsterHelperData::CONFIG_PATH_MENU_ACTIVE, 0);
            $this->helper->setDefaultConfig('catalog/seo/product_url_suffix', '');
            $this->helper->setDefaultConfig('catalog/seo/category_url_suffix', '');
        }
        if (version_compare($context->getVersion(), '0.0.8') < 0) {
            $aboutPageContent        = <<<ABOUT
<div class="page-content">
    <div class="page-section">
        <p class="page-section__paragraph">Worlds of Wow can assist with stage design, theming for various ages and stages, and any other design idea you may
            have. We provide original artwork and graphics to bring your ideas to life! With over 12 years experience and
            more than 400 completed projects, we are the experts when it comes to theming and play. Whether you are a multi-site,
            a mobile church or just giving your space a whole new look, we know what it takes from the very first conversation
            as we dream together to the very last nail put into place. We love the creative process of working with you through
            the development of concepts, characters, elements and stories. It’s just our role to bring this vision to life.
            Our themes are always original, never borrowed from check in, to hallways, to large group rooms, our themes are
            designed to tell a story and create destinations where it becomes the place kids want to be.</p>
        <p class="page-section__paragraph">Themes are different for each and every partner. At Worlds of Wow, we take you through a creative brief to fully
            understand your DNA, culture, vision and goals. Many of our staff have previously served in pastoral roles within
            the local body so we truly understand where you’re coming from. Our background in the theme park industry allows
            us to bring the coolest, newest play features to your location. We include several types of play attractions
            such as custom slides, rock climbing walls, soft contained play, interactives and more. Indoor playgrounds are
            a huge hit with everyone – from Pre-K to elementary-aged kids to their parents.</p>
        <p class="page-section__paragraph">These attractions are a wonderful and creative way to reach out to the community with a safe, fun destination for
            families all during the week. We design, fabricate and install soft contained, multi-level indoor play attractions
            for kids up to12 years old. We also make sure our projects are handicap accessible, meet ADA requirements and
            all the latest safety standards in the industry.</p>
        <p class="page-section__paragraph">We are one of the very few companies in the United States specializing in custom theming and play and we strive to
            have all of our materials are made in the USA! We know that when you get the kids, you get the parents, too.
            That’s why churches have us create interactive and engaging play attractions such as indoor soft playgrounds,
            slides, edutainment and more. We are excited to visit with you and create engaging ways to tell a story that
            illustrates your vision and mission on the walls of your buildings.</p>
        <p class="page-section__paragraph">Worlds of Wow is a great company to partner with when you are looking to transform a space that impacts kids and
            families.</p>
        <p class="page-section__paragraph">The truth is, we are all still kids at heart! So when we are invited into your space to begin dreaming with you,
            we can’t help ourselves the creative juices just start flowing and an original theme design comes to life.</p>
    </div>
    <div class="page-section">
        <h2 class="page-section__caption">Ever wonder how the WOW gets created?</h2>
        <p class="page-section__paragraph">That’s a question we love to answer! So much passion, time and talent goes into each and every project we get to
            be a part of. From the very first conversation to the final piece put into place, our team gives 100% each step
            of the way! When our customers sit back and take in the final masterpiece, it brings us tremendous joy to hear
            them say, “WOW” The great thing about working with Worlds of Wow! is that because we have such a talented and
            experienced team, our designs can be as simple or as over-the-top, realistic , illustrative, as 1-dimensional
            or 3-dimensional as you like. It truly depends on your heart’s desire for your space we just get to come along
            for the ride!</p>
        <p class="page-section__paragraph">When partnering with Wow we strive to exhibit these characteristics Expertise, Relationship, Focus, Technology, Professionalism,
            to reflect every phase on every project in every situation when partnering with Worlds of Wow.
        </p>
        <p class="page-section__paragraph">“How does someone start the process of working with Worlds of Wow?”</p>
    </div>
</div>
ABOUT;
            $termsPageContent        = <<<TERMS
<div class="page-content">
    <div class="page-section">
        <h2 class="page-seciton__caption">1. Acceptance of page</h2>
        <p class="page-section__paragraph">Worlds of Wow provides content and services within the domain and subdomains of worldsofwow.com (collectively
            referred to as the “site”). By accessing this site, you are agreeing to be bound by these page of Use, all
            applicable laws and regulations, and agree that you are responsible for compliance with any applicable local
            laws. If you do not agree with any of these page, you are prohibited from using or accessing this site.</p>
        <p class="page-section__paragraph">Any claim relating to Worlds of Wow’s web site shall be governed by the laws of the State of Texas without regard
            to its conflict of law provisions.</p>
    </div>

    <div class="page-section">
        <h2 class="page-seciton__caption">2. Disclaimer</h2>
        <p class="page-section__paragraph">The content on Worlds of Wow’s web site is provided "as is." Worlds of Wow makes no warranties, expressed or
            implied, including without limitation, implied warranties or conditions of merchantability, fitness for a
            particular purpose, or non-infringement of intellectual property or other violation of rights. Worlds of
            Wow does not warrant or make any representations concerning the accuracy, likely results, or reliability
            of the use of the materials on its web site or to materials and sites linked to or from this site.</p>
        <p class="page-section__paragraph">Further, the content on this website may contain technical, typographical, or photographic errors. Worlds of
            Wow does not guarantee any of the content on its website to be complete, accurate, or up-to-date. Accessing
            this site’s content, as well as downloading any contained or linked content is done at your own discretion.
            Worlds of Wow may update or revise site content at any time without notice, but does not commit to make such
            changes.</p>
    </div>


    <div class="page-section">
        <h2 class="page-seciton__caption">3. Limitations</h2>
        <p class="page-section__paragraph">Neither Worlds of Wow, nor its affiliates, will be held liable for any damages due to the use or access, or inability
            to use or access, the content contained within this website. In jurisdictions where there are restrictions
            on implied warranties, or limitations of liability are not allowed, these limitations may not apply.</p>
    </div>

    <div class="page-section">
        <h2 class="page-seciton__caption">4. Copyright</h2>
        <p class="page-section__paragraph">©2017 Worlds of Wow All Rights Reserved.</p>
        <p class="page-section__paragraph">By accessing this site and its content acknowledge and agree that all content contained within the site is protected
            by copyrights, trademarks, service marks, patents, and other intellectual property laws. You are only permitted
            to use the content as authorized by us, or its specific provider.</p>
        <p class="page-section__paragraph">Specifically, you may: temporarily download one copy of the content on Worlds of Wow's web site for personal,
            non-commercial transitory viewing only. This is the grant of a license, not a transfer of title.</p>
        <p class="page-section__paragraph">Specifically, you may not: modify or copy the content, use for any commercial purpose or display, attempt to
            decompile or reverse engineer any software on Worlds of Wow’s site, remove any copyright or other notations
            from the content, nor transfer the content to another person or “mirror” the content to another server.</p>
    </div>

    <div class="page-section">
        <h2 class="page-seciton__caption">5. Privacy Policy</h2>
        <p class="page-section__paragraph">This policy exists to inform you, the website visitor, how we (Worlds of Wow) collect and use information gained
            from the use of this site, and other means of communication.</p>
    </div>

    <div class="page-section">
        <h2 class="page-seciton__caption">6. Information We Collect</h2>
        <p class="page-section__paragraph">Worlds of Wow collects data throughout the course of each visitor’s time on the site. This data includes:</p>
        <div class="page-section__paragraph page-bullets">
            <ul class="page-bullets__list">
                <li>the domain from which you access the internet</li>
                <li>the date and time of your access to the pages within our site</li>
                <li>the address of the site you used to link to or access our site</li>
            </ul>
        </div>
        <p class="page-section__paragraph">This information is collected for statistical purposes to asses what information on our site is most/least popular,
            what pages may need updated, and performance of our web site in comparison to previous and industry trends.</p>
        <p class="page-section__paragraph">In addition, Worlds of Wow collects personally identifiable information when you specifically choose to provide
            such information. This personally identifiable information may be used for any of the following:</p>
        <div class="page-section__paragraph page-bullets">
            <ul class="page-bullets__list">
                <li>to respond to specific requests for correspondence or materials</li>
                <li>to register visitors for contests, email subscriptions, surveys, and other online activities</li>
                <li>to fulfill prizes</li>
                <li>to send notices of promotions and offers from Worlds of Wow</li>
                <li>to process employment applications and/or resumés</li>
            </ul>
        </div>
    </div>

    <div class="page-section">
        <h2 class="page-seciton__caption">7. How Your Information May Be Shared</h2>
        <p class="page-section__paragraph">Worlds of Wow will not sell, rent, loan, or transfer personally identifiable information except as noted in this
            policy.</p>
        <p class="page-section__paragraph">Worlds of Wow may share personally identifiable information to parties outside of Worlds of Wow in the following
            circumstances:</p>
        <div class="page-section__paragraph page-bullets">
            <ul class="page-bullets__list">
                <li>in the event of, or as part of preliminary due diligence related to, a corporate sale, merger, acquisition,
                    or similar event</li>
                <li>working with third party companies to support the Site’s technical operation or execute a specific promotion
                    or program (such as providing responses to any Comments Page, conduct surveys, or maintain a database
                    of visitor information, etc. </li>
                <li>in connection with processing submitted resume and/or employment application, Worlds of Wow and its affiliates
                    may disclose your Personal Information to any other third party necessary to further process your
                    resume and/or employment application</li>
            </ul>
        </div>
    </div>

    <div class="page-section">
        <h2 class="page-seciton__caption"></h2>
        <p class="page-section__paragraph">When choosing to Opt-In to digital communications from Worlds of Wow you are authorizing the use of your email
            address and other personally identifiable information to be used for the purposes of:</p>
        <div class="page-section__paragraph page-bullets">
            <ul class="page-bullets__list">
                <li>notification of company and industry related news and events</li>
                <li>marketing and promotional messages about our company, products, services, and events</li>
            </ul>
        </div>
    </div>
</div>
TERMS;
            $privacyPageContent      = <<<PAGE
<div class="page-content">
    <div class="page-section">
        <p class="page-section__paragraph">
            Privacy Policy
        </p>
        <p class="page-section__paragraph">Effective date: May 17, 2018
        </p>
        <p class="page-section__paragraph">PlayCore Wisconsin, Inc. ("us", "we", or "our") operates the
            <a href="www.worldsofwow.com">www.worldsofwow.com</a> website (the "Service").</p>
        <p class="page-section__paragraph">This page informs you of our policies regarding the collection, use, and disclosure of personal data when you use
            our Service and the choices you have associated with that data.
        </p>
        <p class="page-section__paragraph">We use your data to provide and improve the Service. By using the Service, you agree to the collection and use of
            information in accordance with this policy. Unless otherwise defined in this Privacy Policy, terms used in this
            Privacy Policy have the same meanings as in our Terms and Conditions, accessible from
            <a href="https://www.worldsofwow.com/terms-and-conditions">https://www.worldsofwow.com/terms-and-conditions</a>
        </p>
    </div>
    <div class="page-section">
        <h2 class="page-section__caption">Definitions</h2>
        <p class="page-section__secondary-caption">Service</p>
        <p class="page-section__paragraph">Service is the
            <a href="https://worldsofwow.com">worldsofwow.com</a> website operated by PlayCore Wisconsin, Inc.</p>
        <p class="page-section__secondary-caption">Personal Data</p>
        <p class="page-section__paragraph">Personal Data means data about a living individual who can be identified from those data (or from those and other
            information either in our possession or likely to come into our possession).</p>
        <p class="page-section__secondary-caption">Usage Data</p>
        <p class="page-section__paragraph">Usage Data is data collected automatically either generated by the use of the Service or from the Service infrastructure
            itself (for example, the duration of a page visit).</p>
        <p class="page-section__secondary-caption">Cookies</p>
        <p class="page-section__paragraph">Cookies are small pieces of data stored on your device (computer or mobile device).</p>
        <p class="page-section__secondary-caption">Data Controller</p>
        <p class="page-section__paragraph">Data Controller means the natural or legal person who (either alone or jointly or in common with other persons) determines
            the purposes for which and the manner in which any personal information are, or are to be, processed. For the
            purpose of this Privacy Policy, we are a Data Controller of your Personal Data.</p>
        <p class="page-section__secondary-caption">Data Processors (or Service Providers)</p>
        <p class="page-section__paragraph">Data Processor (or Service Provider) means any natural or legal person who processes the data on behalf of the Data
            Controller. We may use the services of various Service Providers in order to process your data more effectively.
        </p>
        <p class="page-section__secondary-caption">Data Subject (or User)</p>
        <p class="page-section__paragraph">Data Subject is any living individual who is using our Service and is the subject of Personal Data.</p>
    </div>

    <div class="page-section">
        <h2 class="page-section__caption">Information Collection And Use</h2>
        <p class="page-section__paragraph">We collect several different types of information for various purposes to provide and improve our Service to you.
        </p>
        <p class="page-section__secondary-caption">Types of Data Collected</p>
        <p class="page-section__third-caption">Personal Data</p>
        <p class="page-section__paragraph">While using our Service, we may ask you to provide us with certain personally identifiable information that can be
            used to contact or identify you ("Personal Data"). Personally identifiable information may include, but is not
            limited to:</p>
        <div class="page-section__paragraph page-bullets">
            <ul class="page-bullets__list">
                <li>Email address</li>
                <li>First name and last name</li>
                <li>Phone number</li>
                <li>Country, Address, State, Province, ZIP/Postal code, City</li>
                <li>Cookies and Usage Data</li>
            </ul>
        </div>
        <p class="page-section__paragraph">We may use your Personal Data to contact you with newsletters, marketing or promotional materials and other information
            that may be of interest to you. You may opt out of receiving any, or all, of these communications from us by
            following the unsubscribe link or instructions provided in any email we send.</p>
        <p class="page-section__third-caption">Usage Data</p>
        <p class="page-section__paragraph">We may also collect information how the Service is accessed and used ("Usage Data"). This Usage Data may include
            information such as your computer's Internet Protocol address (e.g. IP address), browser type, browser version,
            the pages of our Service that you visit, the time and date of your visit, the time spent on those pages, unique
            device identifiers and other diagnostic data.</p>
        <p class="page-section__third-caption">Tracking Cookies Data</p>
        <p class="page-section__paragraph">We use cookies and similar tracking technologies to track the activity on our Service and hold certain information.</p>
        <p class="page-section__paragraph">Cookies are files with small amount of data which may include an anonymous unique identifier. Cookies are sent to
            your browser from a website and stored on your device. Tracking technologies also used are beacons, tags, and
            scripts to collect and track information and to improve and analyze our Service.</p>
        <p class="page-section__paragraph">You can instruct your browser to refuse all cookies or to indicate when a cookie is being sent. However, if you do
            not accept cookies, you may not be able to use some portions of our Service.</p>
        <p class="page-section__paragraph">Examples of Cookies we use:</p>
        <div class="page-section__paragraph page-bullets">
            <ul class="page-bullets__list">
                <li>
                    <strong>Session Cookies.</strong> We use Session Cookies to operate our Service.</li>
                <li>
                    <strong>Preference Cookies.</strong> We use Preference Cookies to remember your preferences and various settings.
                </li>
                <li>
                    <strong>Security Cookies.</strong> We use Security Cookies for security purposes.</li>
            </ul>
        </div>
    </div>

    <div class="page-section">
        <h2 class="page-section__caption">Use of Data</h2>
        <p class="page-section__paragraph">PlayCore Wisconsin, Inc. uses the collected data for various purposes:</p>
        <div class="page-section__paragraph page-bullets">
            <ul class="page-bullets__list">
                <li>To provide and maintain our Service</li>
                <li>To notify you about changes to our Service</li>
                <li>To allow you to participate in interactive features of our Service when you choose to do so</li>
                <li>To provide customer support</li>
                <li>To gather analysis or valuable information so that we can improve our Service</li>
                <li>To monitor the usage of our Service</li>
                <li>To detect, prevent and address technical issues</li>
                <li>To process any employment applications and/or resumes</li>
                <li>To provide you with news, special offers and general information about other goods, services and events which
                    we offer that are similar to those that you have already purchased or enquired about unless you have
                    opted not to receive such information</li>
            </ul>
        </div>
    </div>

    <div class="page-section">
        <h2 class="page-section__caption">Legal Basis for Processing Personal Data Under General Data Protection Regulation (GDPR)</h2>
        <p class="page-section__paragraph">If you are from the European Economic Area (EEA), PlayCore Wisconsin, Inc. legal basis for collecting and using the
            personal information described in this Privacy Policy depends on the Personal Data we collect and the specific
            context in which we collect it.</p>
        <p class="page-section__paragraph">PlayCore Wisconsin, Inc. may process your Personal Data because:</p>
        <div class="page-section__paragraph page-bullets">
            <ul class="page-bullets__list">
                <li>We need to perform a contract with you</li>
                <li>You have given us permission to do so</li>
                <li>The processing is in our legitimate interests and it's not overridden by your rights</li>
                <li>To comply with the law</li>
            </ul>
        </div>
    </div>

    <div class="page-section">
        <h2 class="page-section__caption">Retention of Data</h2>
        <p class="page-section__paragraph">PlayCore Wisconsin, Inc. will retain your Personal Data only for as long as is necessary for the purposes set out
            in this Privacy Policy. We will retain and use your Personal Data to the extent necessary to comply with our
            legal obligations (for example, if we are required to retain your data to comply with applicable laws), resolve
            disputes, and enforce our legal agreements and policies.</p>
        <p class="page-section__paragraph">PlayCore Wisconsin, Inc. will also retain Usage Data for internal analysis purposes. Usage Data is generally retained
            for a shorter period of time, except when this data is used to strengthen the security or to improve the functionality
            of our Service, or we are legally obligated to retain this data for longer time periods.</p>
    </div>

    <div class="page-section">
        <h2 class="page-section__caption">Transfer Of Data</h2>
        <p class="page-section__paragraph">Your information, including Personal Data, may be transferred to — and maintained on — computers located outside
            of your state, province, country or other governmental jurisdiction where the data protection laws may differ
            than those from your jurisdiction.</p>
        <p class="page-section__paragraph">If you are located outside United States and choose to provide information to us, please note that we transfer the
            data, including Personal Data, to United States and process it there.</p>
        <p class="page-section__paragraph">Your consent to this Privacy Policy followed by your submission of such information represents your agreement to
            that transfer.</p>
        <p class="page-section__paragraph">PlayCore Wisconsin, Inc. will take all steps reasonably necessary to ensure that your data is treated securely and
            in accordance with this Privacy Policy and no transfer of your Personal Data will take place to an organization
            or a country unless there are adequate controls in place including the security of your data and other personal
            information.
        </p>
    </div>

    <div class="page-section">
        <h2 class="page-section__caption">Disclosure Of Data</h2>
        <p class="page-section__secondary-caption">Disclosure for Law Enforcement</p>
        <p class="page-section__paragraph">Under certain circumstances, PlayCore Wisconsin, Inc. may be required to disclose your Personal Data if required
            to do so by law or in response to valid requests by public authorities (e.g. a court or a government agency).</p>
        <p class="page-section__secondary-caption">Legal Requirements</p>
        <p class="page-section__paragraph">PlayCore Wisconsin, Inc. may disclose your Personal Data in the good faith belief that such action is necessary to:
        </p>
        <div class="page-section__paragraph page-bullets">
            <ul class="page-bullets__list">
                <li>To comply with a legal obligation</li>
                <li>To protect and defend the rights or property of PlayCore Wisconsin, Inc.</li>
                <li>To prevent or investigate possible wrongdoing in connection with the Service</li>
                <li>To protect the personal safety of users of the Service or the public</li>
                <li>To protect against legal liability</li>
            </ul>
        </div>
    </div>

    <div class="page-section">
        <h2 class="page-section__caption">Security Of Data</h2>
        <p class="page-section__paragraph">The security of your data is important to us, but remember that no method of transmission over the Internet, or method
            of electronic storage is 100% secure. While we strive to use commercially acceptable means to protect your Personal
            Data, we cannot guarantee its absolute security.</p>
    </div>

    <div class="page-section">
        <h2 class="page-section__caption">"Do Not Track" Signals</h2>
        <p class="page-section__paragraph">We do not support Do Not Track ("DNT"). Do Not Track is a preference you can set in your web browser to inform websites
            that you do not want to be tracked.</p>
        <p class="page-section__paragraph">You can enable or disable Do Not Track by visiting the Preferences or Settings page of your web browser.</p>
    </div>

    <div class="page-section">
        <h2 class="page-section__caption">Your Data Protection Rights Under General Data Protection Regulation (GDPR)</h2>
        <p class="page-section__paragraph">If you are a resident of the European Economic Area (EEA), you have certain data protection rights. PlayCore Wisconsin,
            Inc. aims to take reasonable steps to allow you to correct, amend, delete, or limit the use of your Personal
            Data.</p>
        <p class="page-section__paragraph">If you wish to be informed what Personal Data we hold about you and if you want it to be removed from our systems,
            please contact us.</p>
        <p class="page-section__paragraph">In certain circumstances, you have the following data protection rights:</p>
        <p class="page-section__paragraph">
            <strong>The right to access, update or to delete the information we have on you.</strong> Whenever made possible, you
            can access, update or request deletion of your Personal Data directly within your account settings section. If
            you are unable to perform these actions yourself, please contact us to assist you.</p>
        <p class="page-section__paragraph">
            <strong>The right of rectification.</strong> You have the right to have your information rectified if that information
            is inaccurate or incomplete.</p>
        <p class="page-section__paragraph">
            <strong>The right to object.</strong> You have the right to object to our processing of your Personal Data.</p>
        <p class="page-section__paragraph">
            <strong>The right of restriction.</strong> You have the right to request that we restrict the processing of your personal
            information.</p>
        <p class="page-section__paragraph">
            <strong>The right to data portability.</strong> You have the right to be provided with a copy of the information we have
            on you in a structured, machine-readable and commonly used format.</p>
        <p class="page-section__paragraph">
            <strong>The right to withdraw consent.</strong> You also have the right to withdraw your consent at any time where PlayCore
            Wisconsin, Inc. relied on your consent to process your personal information.</p>
        <p class="page-section__paragraph">Please note that we may ask you to verify your identity before responding to such requests.</p>
        <p class="page-section__paragraph">You have the right to complain to a Data Protection Authority about our collection and use of your Personal Data.
            For more information, please contact your local data protection authority in the European Economic Area (EEA).</p>
    </div>

    <div class="page-section">
        <h2 class="page-section__caption">Service Providers</h2>
        <p class="page-section__paragraph">We may employ third party companies and individuals to facilitate our Service ("Service Providers"), to provide the
            Service on our behalf, to perform Service-related services or to assist us in analyzing how our Service is used.</p>
        <p class="page-section__paragraph">These third parties have access to your Personal Data only to perform these tasks on our behalf and are obligated
            not to disclose or use it for any other purpose.</p>
        <p class="page-section__secondary-caption">Analytics</p>
        <p class="page-section__paragraph">We may use third-party Service Providers to monitor and analyze the use of our Service.</p>
        <p class="page-section__paragraph page-section__paragraph--bold">Google Analytics</p>
        <p class="page-section__paragraph">Google Analytics is a web analytics service offered by Google that tracks and reports website traffic. Google uses
            the data collected to track and monitor the use of our Service. This data is shared with other Google services.
            Google may use the collected data to contextualize and personalize the ads of its own advertising network. You
            can opt-out of having made your activity on the Service available to Google Analytics by installing the Google
            Analytics opt-out browser add-on. The add-on prevents the Google Analytics JavaScript (ga.js, analytics.js, and
            dc.js) from sharing information with Google Analytics about visits activity. For more information on the privacy
            practices of Google, please visit the Google Privacy Terms web page:
            <a href="http://www.google.com/intl/en/policies/privacy/">http://www.google.com/intl/en/policies/privacy/</a>
        </p>
        <p class="page-section__secondary-caption">Behavioral Remarketing</p>
        <p class="page-section__paragraph">PlayCore Wisconsin, Inc. uses remarketing services to advertise on third party websites to you after you visited
            our Service. We and our third-party vendors use cookies to inform, optimize and serve ads based on your past
            visits to our Service.</p>
        <p class="page-section__paragraph page-section__paragraph--bold">Google AdWords</p>
        <p class="page-section__paragraph">Google AdWords remarketing service is provided by Google Inc. You can opt-out of Google Analytics for Display Advertising
            and customize the Google Display Network ads by visiting the Google Ads Settings page:
            <a href="http://www.google.com/settings/ads">http://www.google.com/settings/ads</a>
            Google also recommends installing the Google Analytics Opt-out Browser Add-on -
            <a href="https://tools.google.com/dlpage/gaoptout">https://tools.google.com/dlpage/gaoptout</a> - for your web browser. Google Analytics Opt-out Browser Add-on
            provides visitors with the ability to prevent their data from being collected and used by Google Analytics. For
            more information on the privacy practices of Google, please visit the Google Privacy Terms web page:
            <a href="http://www.google.com/intl/en/policies/privacy/">http://www.google.com/intl/en/policies/privacy/</a>
        </p>
        <p class="page-section__paragraph page-section__paragraph--bold">Facebook</p>
        <p class="page-section__paragraph">Facebook remarketing service is provided by Facebook Inc. You can learn more about interest-based advertising from
            Facebook by visiting this page:
            <a href="https://www.facebook.com/help/164968693837950">https://www.facebook.com/help/164968693837950</a> To opt-out from Facebook's interest-based ads follow these
            instructions from Facebook:
            <a href="https://www.facebook.com/help/568137493302217">https://www.facebook.com/help/568137493302217</a> Facebook adheres to the Self-Regulatory Principles for Online
            Behavioral Advertising established by the Digital Advertising Alliance. You can also opt-out from Facebook and
            other participating companies through the Digital Advertising Alliance in the USA
            <a href="http://www.aboutads.info/choices/">http://www.aboutads.info/choices/</a>, the Digital Advertising Alliance of Canada in Canada http://youradchoices.ca/
            or the European Interactive Digital Advertising Alliance in Europe
            <a href="http://www.youronlinechoices.eu/">http://www.youronlinechoices.eu/</a>, or opt-out using your mobile device settings. For more information on the
            privacy practices of Facebook, please visit Facebook's Data Policy:
            <a href="https://www.facebook.com/privacy/explanation">https://www.facebook.com/privacy/explanation</a>
        </p>
    </div>

    <div class="page-section">
        <h2 class="page-section__caption">Links To Other Sites</h2>
        <p class="page-section__paragraph">Our Service may contain links to other sites that are not operated by us. If you click on a third party link, you
            will be directed to that third party's site. We strongly advise you to review the Privacy Policy of every site
            you visit.</p>
        <p class="page-section__paragraph">We have no control over and assume no responsibility for the content, privacy policies or practices of any third
            party sites or services.</p>
    </div>

    <div class="page-section">
        <h2 class="page-section__caption">Children's Privacy</h2>
        <p class="page-section__paragraph">Our Service does not address anyone under the age of 18 ("Children").</p>
        <p class="page-section__paragraph">We do not knowingly collect personally identifiable information from anyone under the age of 18. If you are a parent
            or guardian and you are aware that your Children has provided us with Personal Data, please contact us. If we
            become aware that we have collected Personal Data from children without verification of parental consent, we
            take steps to remove that information from our servers.</p>
    </div>

    <div class="page-section">
        <h2 class="page-section__caption">Changes To This Privacy Policy</h2>
        <p class="page-section__paragraph">We may update our Privacy Policy from time to time. We will notify you of any changes by posting the new Privacy
            Policy on this page.</p>
        <p class="page-section__paragraph">We will let you know via email and/or a prominent notice on our Service, prior to the change becoming effective and
            update the "effective date" at the top of this Privacy Policy.</p>
        <p class="page-section__paragraph">You are advised to review this Privacy Policy periodically for any changes. Changes to this Privacy Policy are effective
            when they are posted on this page.</p>
    </div>

    <div class="page-section">
        <h2 class="page-section__caption">Contact Us</h2>
        <p class="page-section__paragraph">If you have any questions about this Privacy Policy, please contact us:</p>
        <div class="page-section__paragraph page-bullets">
            <ul class="page-bullets__list">
                <li>By email: webadmin@playcore.com</li>
                <li>By visiting this page on our website: https://www.playcore.com/contact</li>
                <li>By phone number: (423) 265-7529 EXT: 5894</li>
                <li>By mail:</li>
                <li class="page-bullets__sublist">
                    <ul class="page-bullets__list page-bullets__list--secondary">
                        <li>544 Chestnut Street, Chattanooga, TN 37402, United States</li>
                        <li>Attn: Corporate Director of Digital Engagement</li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
PAGE;
            $ourProcessPageContent   = <<<PROCCESS
<div class="page-content">
	<div class="page-section">
		<h2 class="page-section__caption">Design Consultation</h2>
		<p class="page-section__paragraph">This gets the creativity started. Once you sign your creative agreement, we do an onsite and/or virtual consultation to
			generate a creative brief.</p>
		<p class="page-section__paragraph">We become familiar with your space, vision, style, budget and more. Typically it's a one day visit that includes meetings
			and lots of fun.</p>
	</div>
	<div class="page-section">
		<h2 class="page-section__caption">Concept Phase</h2>
		<p class="page-section__paragraph">Dreams & ideas come to life on paper. We start with sketches and seek your input. Sometimes we hit a home run the first
			time, sometimes it takes a few tries to get it just right.</p>
		<p class="page-section__paragraph">We then wrap up with a production agreement. This is a written document of what's in the concept sketches and renderings,
			and the work that will be completed in your space.</p>
	</div>
	<div class="page-section">
		<h2 class="page-section__caption">Design Phase</h2>
		<p class="page-section__paragraph">Our design team takes the production agreement, rough sketches and concept elements and converts them into elevation drawings.
			This is where the rough sketches snap into focus and becomes a polished work of art!</p>
	</div>
	<div class="page-section">
		<h2 class="page-section__caption">Production Art Phase</h2>
		<p class="page-section__paragraph">Visions come to life in this phase as we physically produce the final artwork to be placed on your walls, creating your
			ridiculously cool original theme.</p>
	</div>
	<div class="page-section">
		<h2 class="page-section__caption">Fabrication Phase</h2>
		<p class="page-section__paragraph">We handcraft each of the unique pieces for your project at this point. Our install team produces the 2D appliques, 3D elements
			and play attractions for the final installation.</p>
	</div>
	<div class="page-section">
		<h2 class="page-section__caption">Installation Phase</h2>
		<p class="page-section__paragraph">Our teams are coordinated, the unique elements are shipped to your site and installation begins! The size of the project
			always dictates our time on your site, typically 5 - 14 days.</p>
		<p class="page-section__paragraph">During this phase you will see the transformation taking place. The final step is the sign off form indicating the project
			is complete. Our goal is to make you very, very happy!</p>
	</div>
	<div class="page-section">
		<h2 class="page-section__caption">Enjoyment Phase</h2>
		<p class="page-section__paragraph">This is where you and your team enjoy the amazing environment that you and Worlds of Wow created together.</p>
		<p class="page-section__paragraph">Through the successful completion of each project, our hope and mission is to equip the local church to impact their community
			by providing environments that foster life change.</p>
	</div>
	<div class="page-section page-section--centered">
		<a class="page-section__download-btn" href="{{media url="downloadable/process.pdf"}}" target="_Blank">Click Here to Download Our Process</a>
	</div>
	
	<hr>
	<div class="page-section page-section--centered">
		<h2 class="page-section__caption">
			So what are you waiting for? Let's get started today!
		</h2>
	</div>
</div>
PROCCESS;
            $reviewsPageContent      = <<<REVIWES
<div class="page-content">
    <div class="page-section">
        <iframe class="page-video" width="560" height="315" src="https://www.youtube.com/embed/whcimfzTYUY" frameborder="0" allowfullscreen=""></iframe>
        <p class="page-section__paragraph">"We experienced a 35% increase in overall attendance. The increase is attributed to the new facility environment
            which includes new spaces, great decor, state-of-the-art security/check-in system and a super location."</p>
        <p class="page-section__paragraph--small">- Ruth Charlson, Second Baptist Baytown in Baytown TX</p>
    </div>
    <div class="page-section">
        <iframe class="page-video" width="560" height="315" src="https://www.youtube.com/embed/c9-CuewB9Hc" frameborder="0" allowfullscreen=""></iframe>
        <p class="page-section__paragraph">"I just wanted to drop you a note to say thank you. You don't know me personally, but I wanted to thank you and your
            staff for your service, commitment and love for this community. It is making an impact. Your work is bearing
            fruit. So thank you! Be encouraged :) You are making a difference. Keep it up :) With Gratitude, Erika"
        </p>
    </div>
    <div class="page-section">
        <iframe class="page-video" width="560" height="315" src="https://www.youtube.com/embed/TwbHA2uBV4o" frameborder="0" allowfullscreen=""></iframe>
        <p class="page-section__paragraph">"It is not an uncommon sight to see families taking pictures of their children by the scenes. This has helped change
            the attitude of the workers and parents alike in a positive way."</p>
        <p class="page-section__paragraph--small">-Mark Harrison with Spring Baptist Church in Spring, TX</p>
    </div>
    <div class="page-section">
        <iframe class="page-video" width="560" height="315" src="https://www.youtube.com/embed/5jSgkZAJiCk" frameborder="0" allowfullscreen=""></iframe>
        <iframe class="page-video" width="560" height="315" src="https://www.youtube.com/embed/kGPBSFR69aQ" frameborder="0" allowfullscreen=""></iframe>
        <p class="page-section__paragraph">"Such a pleasure to be able to have Worlds of WOW do our renovation. The team that came in were absolutely phenomenal
            artist and great people to work with."</p>
        <p class="page-section__paragraph--small">- Paulette Bowman, Eden Westside Baptist Church</p>
    </div>
</div>
REVIWES;

            $layout = '1column';
            $pagesDataUpdate = [
                'about'                => [
                    'url_key'         => 'about',
                    'title'           => 'About',
                    'content_heading' => 'About',
                    'content'         => $aboutPageContent,
                    'page_layout'     => $layout,
                    'store_id'        => [0]
                ],
                'faqs'                 => [
                    'url_key'         => 'faqs',
                    'title'           => 'FAQ\'s',
                    'content_heading' => 'FAQ\'s',
                    'content'         => $faqsPageContent,
                    'page_layout'     => $layout,
                    'store_id'        => [0]
                ],
                'privacy-policy'       => [
                    'url_key'         => 'privacy-policy',
                    'title'           => 'Privacy Policy',
                    'content_heading' => 'Privacy Policy',
                    'content'         => $privacyPageContent,
                    'page_layout'     => $layout,
                    'store_id'        => [0]
                ],
                'terms-and-conditions' => [
                    'url_key'         => 'terms-and-conditions',
                    'title'           => 'Terms and Conditions',
                    'content_heading' => 'Terms and Conditions',
                    'content'         => $termsPageContent,
                    'page_layout'     => $layout,
                    'store_id'        => [0]
                ],
                'process'              => [
                    'url_key'         => 'process',
                    'title'           => 'Our Process',
                    'content_heading' => 'Our Process',
                    'content'         => $ourProcessPageContent,
                    'page_layout'     => $layout,
                    'store_id'        => [0]
                ],
                'reviews'              => [
                    'url_key'         => 'reviews',
                    'title'           => 'Reviews',
                    'content_heading' => 'Reviews',
                    'content'         => $reviewsPageContent,
                    'page_layout'     => $layout,
                    'store_id'        => [0]
                ],
                'no-route'             => [
                    'url_key'         => 'no-route',
                    'page_layout'     => $layout,
                ]
            ];
            $this->updateCmsPages($pagesDataUpdate);
            $this->updateCategories([
                'facility'        => [
                    'url_key'          => 'facility',
                    'name'             => 'Facility',
                    'meta_title'       => 'Facility',
                    'display_mode'     => Category::DM_PRODUCT,
                    'is_active'        => true,
                    'is_anchor'        => false,
                    'include_in_menu'  => true,
                    'mm_turn_on'       => 1,
                    'mm_css_class'     => 'menu-colored-item menu-colored-item--blue megamenu-submenu',
                ],
                'contact_us'      => [
                    'url_key'          => 'contacts',
                    'name'             => 'Contact',
                    'meta_title'       => 'Contact',
                    'display_mode'     => Category::DM_PAGE,
                    'is_active'        => false,
                    'is_anchor'        => false,
                    'include_in_menu'  => false,
                    'mm_turn_on'       => 0,
                    'mm_css_class'     => '',
                ],
            ]);
            $this->fixUrlRewrites();
        }
        if (version_compare($context->getVersion(), '0.0.9') < 0) {
            $this->updateStaticBlocks([
                'market'   => [
                    'title'     => 'Market',
                    'stores'    => [0],
                    'is_active' => true,
                    'content'   => '<img src="{{media url="wysiwyg/IMG_3496.jpg"}}" alt="" width="200px" height="250px" />',
                ],
                'facility' => [
                    'title'     => 'Facility',
                    'stores'    => [0],
                    'is_active' => true,
                    'content'   => '<img src="{{media url="wysiwyg/IMG_3497.jpg"}}" alt="" />',
                ],
                'themes'   => [
                    'title'     => 'Themes',
                    'stores'    => [0],
                    'is_active' => true,
                    'content'   => '<img src="{{media url="wysiwyg/IMG_3495.jpg"}}" alt="" />',
                ],
            ]);
            $this->updateMegamenu([
                'market'          => [
                    'url_key' => 'market',
                    'mm_turn_on'  => true,
                    'configurator' => [
                        [
                            'width' => 4,
                            'css_class' => 'megamenu-submenu-column',
                        ],
                        [
                            'width' => 4,
                            'css_class' => 'megamenu-submenu-column',
                        ],
                        [
                            'width' => 4,
                            'css_class' => 'megamenu-submenu-image',
                        ],
                    ]
                ],
                'facility'        => [
                    'url_key' => 'facility',
                    'mm_turn_on'  => true,
                    'configurator' => [
                        [
                            'width' => 6,
                            'css_class' => 'megamenu-submenu-column',
                        ],
                        [
                            'width' => 6,
                            'css_class' => 'megamenu-submenu-image',
                        ],
                    ]
                ],
                'themes'          => [
                    'url_key' => 'themes',
                    'mm_turn_on'  => true,
                    'configurator' => [
                        [
                            'width' => 6,
                            'css_class' => 'megamenu-submenu-column',
                        ],
                        [
                            'width' => 6,
                            'css_class' => 'megamenu-submenu-image',
                        ],
                    ]
                ],
                'request_a_quote' => [
                    'url_key' => 'request-a-quote',
                    'mm_turn_on'  => false,
                    'configurator' => false,
                ],
            ]);
            $this->helper->setDefaultConfig('catalog/frontend/grid_per_page_values', '8,12');
            $this->helper->setDefaultConfig('catalog/frontend/grid_per_page', '8');
        }
        if (version_compare($context->getVersion(), '0.0.10') < 0) {
            $homePageContent = <<<HOME
<div class="hero-banner">
    <img src="{{media url="wysiwyg/hero.png"}}" alt="herobanner">
    <div class="hero-banner__content">
        <div class="hero-banner__content-text">
                <span class="hero-banner__text">
                    Creating Cool, Fun Environments for Kids and Families
                </span>
            <a href="{{config path="web/secure/base_url"}}request-a-quote" class="hero-banner__link">Get started now!</a>
        </div>
    </div>
    <a href="#" data-section="block-products-list" class="next-section-scroller js-hook__next-section-scroller">
        <span class="next-section-scroller__icon"></span>
        <span class="next-section-scroller__arrow"></span><span class="next-section-scroller__arrow"></span>
    </a>
</div>

<!-- NEWEST CREATIONS widget here -->
{{widget type="Skynix\CatalogWidget\Block\Product\ProductsList" title="NEWEST CREATIONS" show_pager="0" show_link="1" products_count="4" template="Skynix_CatalogWidget::product/widget/content/grid.phtml" id_path="category/6" conditions_encoded="^[`1`:^[`type`:`Magento||CatalogWidget||Model||Rule||Condition||Combine`,`aggregator`:`all`,`value`:`1`,`new_child`:``^],`1--1`:^[`type`:`Magento||CatalogWidget||Model||Rule||Condition||Product`,`attribute`:`category_ids`,`operator`:`==`,`value`:`3`^]^]"}}

<div class="about-block">
    <div class="about-block__image">
        <img src="{{media url="wysiwyg/2efbdc_edcea12a31aa4ad38747c2816bf0edda_mv2_d_4032_3024_s_4_2.jpg"}}">
    </div>
    <div class="about-block__container">
        <div class="about-block__header">ABOUT WORLDS OF WOW</div>
        <div class="about-block__descr">Worlds of Wow! is a great company to partner with when you are looking to create themed environments and children’s playgrounds that engage kids and families. Churches, Pediatric Dental Offices, Family Entertainment Centers, Military Recreation Centers and YMCA's are where we do our best work.</div>
        <a href="/about" class="about-block__btn">View More</a>
    </div>
    <a href="#" data-section="block-banners" class="about-block__arrow js-hook__next-section-scroller"><img src="{{media url="wysiwyg/arrow-pointing-to-left.svg"}}"> DOWN</a>
</div>

<div class="widget block block-banners banner-items-wrapper">
    <ul class="banner-items js-hook__banners-slick">
        <li class="banner-item">
            <div class="banner-item-content">
                <img class="page-header__bg" src="{{media url="wysiwyg/homepage-our-work.jpg"}}">
                <div class="banner__content banner__content--red">
                    <div class="banner__content-text">
                        <h3 class="banner__title">Our work</h3>
                        <span class="banner__text">
                            We create ridiculously cool environments for kids and families. Take a look at some of our most popular themes.
                        </span>
                        <a href="/themes" class="banner__link">Learn more</a>
                    </div>
                </div>
            </div>
        </li>
        <li class="banner-item">
            <div class="banner-item-content">
                <img class="page-header__bg" src="{{media url="wysiwyg/homepage-about-us.jpg"}}">
                <div class="banner__content banner__content--green">
                    <div class="banner__content-text">
                        <h3 class="banner__title">About us</h3>
                        <span class="banner__text">
                            With over 600 completed projects across the country, we are the experts in themed environments and play.
                        </span>
                        <a href="/about" class="banner__link">Learn more</a>
                    </div>
                </div>
            </div>
        </li>
        <li class="banner-item" aria-hidden="true">
            <div class="banner-item-content">
                <img class="page-header__bg" src="{{media url="wysiwyg/designer-at-worlds-of-wow.jpg"}}">
                <div class="banner__content banner__content--blue">
                    <div class="banner__content-text">
                        <h3 class="banner__title">Our services</h3>
                        <span class="banner__text">
                            From concept to design to installation, we are your one-stop shop for theming and indoor playgrounds.
                        </span>
                        <a href="/process" class="banner__link">See All Services</a>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>

<div class="view-block">
    <div class="view-block-item">
        <div class="view-block-item__img">
            <img src="{{media url="wysiwyg/kids-indoor-toddler-play.jpg"}}">
        </div>
        <div class="view-block-item__content">
            <header class="view-block-item__header">VIEW BY THEME</header>
            <p class="view-block-item__p">Donec sed odio dui. Nullam quis risus eget<br> urna mollis ornare vel eu leo.</p>
            <a href="/themes" class="wow-button wow-button--yellow">View More</a>
        </div>
    </div>
    <div class="view-block-item">
        <div class="view-block-item__img">
            <img src="{{media url="wysiwyg/kelly-sikkema-155209-unsplash.jpg"}}">
        </div>
        <div class="view-block-item__content">
            <header class="view-block-item__header">VIEW BY CLIENTS</header>
            <p class="view-block-item__p">Donec sed odio dui. Nullam quis risus eget<br> urna mollis ornare vel eu leo.</p>
            <a href="#" class="wow-button wow-button--red">View More</a>
        </div>
    </div>
</div>

<!-- Latest blog posts widget here -->
{{widget type="TemplateMonster\Blog\Block\Widget\PostList" widget_status="1" title="Latest blog posts" post_amount="4" post_amount_per_row="4" is_enable_carousel="0" template="widget/post_list.phtml"}}
HOME;

            $this->page->load('home')
                ->setContent($homePageContent)
                ->setContentHeading('')
                ->setLayoutUpdateXml('<move element="form.subscribe" destination="page.wrapper" after="main.content"/>');
            $this->page->save();
        }
        if (version_compare($context->getVersion(), '0.0.11') < 0) {
            $faqsPageContent = <<<FAQS
<div class="page-content">
    <div class="page-section">
        <h2 class="page-section__caption">What Does Wow Do?</h2>
        <p class="page-section__paragraph">
            We’re crazy-passionate about creating destinations to reach kids and families. We believe that church should be a fun place
            that kids want to go, so we design ridiculously cool, fun environments that impact kids and families of all ages!
            How we work with churches is unique, but straight forward. At the heart of Worlds of Wow is our experience in
            ministry (including former church staff) and our Kingdom-minded focus. We start by understanding the unique DNA
            of your church and ministry, think through priorities and options and end up with custom concepts and real-world
            budget numbers. In addition to amazing themes, Worlds of Wow also designs and fabricates indoor playground equipment
            for churches. Fact is, we’re the only company in the church market that can design, fabricate and install both
            themed environments and play attractions together. This provides easy one stop solution and budget savings!
        </p>
        <p class="page-section__paragraph">
            We have some of the most talented artists and play designers around. When combined, our themed play attractions become a
            kids’ favorite-thing-of-all-time. Having one team design, fabricate and install your theme and play attractions
            is a huge benefit because there’s a big cost savings of working with one company throughout your project, not
            to mention that cohesively-themed play environments are ridiculously cool for kids
        </p>
    </div>
    <div class="page-section">
        <h2 class="page-section__caption">How Long Does it Take to Complete a Play Space?</h2>
        <p class="page-section__paragraph">Short answer: In as little as four months from start to finish for most projects. Long answer: We work on projects
            anywhere from 4–18 months prior to opening date. Typical artwork turnaround time is 2-3 months and installation
            is usually booked 3-6 months in advance. Most installations take 12-14 days depending on the size and scope of
            the project.</p>
        <p class="page-section__paragraph">Bonus: We can even have our crews work evenings to ensure you stay open during business hours.</p>
    </div>
    <div class="page-section">
        <h2 class="page-section__caption">How Much Does a Playground Cost?</h2>
        <p class="page-section__paragraph">We work on projects from $20,000 to over $1M dollars. We value helping customers be good stewards of their resources.
            We always strive to put the maximum Wow factor into every play theme project!</p>
        <p class="page-section__paragraph">There are three factors in budget ranges:</p>
        <p class="page-section__paragraph">1. More is more. The more theming and/or play, the more it will cost.</p>
        <p class="page-section__paragraph">2. Artwork density. Rolling hills and blue skies are one thing; heavily themed, detailed artwork takes more time
            and costs more.</p>
        <p class="page-section__paragraph">3. How far does it come off the wall? Generally speaking, the more 3D layers or effects, the more it ‘comes off the
            wall’ and the more it costs.</p>
        <p class="page-section__paragraph">We work together with you right from the start so that what we design will fit your estimated budget with no surprises.
            While there are lots of budget range options, Worlds of Wow is highly competitive and, most importantly, the
            best value for custom theming.</p>
    </div>
    <div class="page-section">
        <h2 class="page-section__caption">Can You Travel to *insert your city here*?</h2>
        <p class="page-section__paragraph">We’re a Texas-based company (Dallas/Fort Worth) but we have teams all over the country that travel anywhere. We also
            incorporate the latest technology to communicate and collaborate together throughout the project.</p>
        <p class="page-section__paragraph">We’ve done installations from Washington state to Florida and from California to New York and everywhere in between.
            We’re still looking forward to doing a project in Hawaii however.</p>
    </div>
    <div class="page-section">
        <h2 class="page-section__caption">What Play/Theming Services Do You Offer?</h2>
        <p class="page-section__paragraph">
            We are a full turnkey resource able to handle all aspects of your project from beginning to end. We provide:
        </p>
        <div class="page-section__paragraph page-bullets">
            <p class="page-bullets__caption">Design:</p>
            <ul class="page-bullets__list">
                <li>Master Planning</li>
                <li>Phasing Plan</li>
                <li>Site &amp; Schematic Design</li>
                <li>Budget Allocation</li>
                <li>Storyline Development</li>
                <li>Concept Design</li>
                <li>Signage</li>
                <li>Original Art Design</li>
                <li>Logo Development</li>
            </ul>
            <p class="page-bullets__caption">Production:</p>
            <ul class="page-bullets__list">
                <li>Quality Assurance</li>
                <li>3D Fabrication</li>
                <li>2D Relief Fabrication</li>
                <li>Color Selections</li>
                <li>Site Inspections</li>
                <li>Modular Play Attractions</li>
                <li>Soft Foam Play</li>
            </ul>
            <p class="page-bullets__caption">Installation:</p>
            <ul class="page-bullets__list">
                <li>Airbrushed Murals</li>
                <li>Lighting</li>
                <li>Play Attractions</li>
            </ul>
        </div>
    </div>
    <div class="page-section">
        <h2 class="page-section__caption">Worlds of Wow is ‘green’ and enviro-friendly? Really?</h2>
        <p class="page-section__paragraph">It’s not just our predisposition for the color green; we’re literally going green with our environmentally-friendly
            changes and improvements. Here’s a fun take on describing the steps Worlds of Wow is making to go green.</p>
        <p class="page-section__secondary-caption">Paperless Office.</p>
        <p class="page-section__paragraph">It seems the Internet may be the greatest single greatest tree-saving invention. With all of the electronic communication,
            project management and file sharing, we’ve gone almost 100% paperless. As a result, our office paper shredder
            is getting lonely.</p>
        <p class="page-section__secondary-caption">Materials.</p>
        <p class="page-section__paragraph">We’re using recycled products in our manufacturing shops and changing to materials that are eco-friendly. The polystyrene
            we use for 3D sculpts doesn’t include Ozone CFC’s and HCFC’s. The medium density fiber board we use is phase
            2 carb compliant and 100% recovered and recycled materials.</p>
        <p class="page-section__secondary-caption">Paint.</p>
        <p class="page-section__paragraph">Even our paint is becoming eco-friendly, as we’ve switched all of our primers, airbrush paints and top coat paints
            to a water-borne acrylic paint and clear coat. We are proud to have implemented these initial steps to provide
            the same high-quality, huge WOW-factor environments with a bit more “earth friendly” spin!</p>
    </div>
</div>
FAQS;
            $layout = '1column';
            $pageDataUpdate = [
                'faqs' => [
                    'url_key'         => 'faqs',
                    'title'           => 'FAQ\'s',
                    'content_heading' => 'FAQ\'s',
                    'content'         => $faqsPageContent,
                    'page_layout'     => $layout,
                    'store_id'        => [0]
                ],
            ];
            $this->updateCmsPages($pageDataUpdate);
        }
        if (version_compare($context->getVersion(), '0.0.12') < 0) {
            $termsPageContent = <<<TERMS
<div class="page-content">
    <div class="page-section">
        <h2 class="page-seciton__caption">1. Acceptance of Terms</h2>
        <p class="page-section__paragraph">Worlds of Wow provides content and services within the domain and subdomains of worldsofwow.com (collectively
            referred to as the “site”). By accessing this site, you are agreeing to be bound by these Terms of Use, all
            applicable laws and regulations, and agree that you are responsible for compliance with any applicable local
            laws. If you do not agree with any of these terms, you are prohibited from using or accessing this site.</p>
        <p class="page-section__paragraph">Any claim relating to Worlds of Wow’s web site shall be governed by the laws of the State of Texas without regard
            to its conflict of law provisions.</p>
    </div>

    <div class="page-section">
        <h2 class="page-seciton__caption">2. Disclaimer</h2>
        <p class="page-section__paragraph">The content on Worlds of Wow’s web site is provided "as is." Worlds of Wow makes no warranties, expressed or
            implied, including without limitation, implied warranties or conditions of merchantability, fitness for a
            particular purpose, or non-infringement of intellectual property or other violation of rights. Worlds of
            Wow does not warrant or make any representations concerning the accuracy, likely results, or reliability
            of the use of the materials on its web site or to materials and sites linked to or from this site.</p>
        <p class="page-section__paragraph">Further, the content on this website may contain technical, typographical, or photographic errors. Worlds of
            Wow does not guarantee any of the content on its website to be complete, accurate, or up-to-date. Accessing
            this site’s content, as well as downloading any contained or linked content is done at your own discretion.
            Worlds of Wow may update or revise site content at any time without notice, but does not commit to make such
            changes.</p>
    </div>


    <div class="page-section">
        <h2 class="page-seciton__caption">3. Limitations</h2>
        <p class="page-section__paragraph">Neither Worlds of Wow, nor its affiliates, will be held liable for any damages due to the use or access, or inability
            to use or access, the content contained within this website. In jurisdictions where there are restrictions
            on implied warranties, or limitations of liability are not allowed, these limitations may not apply.</p>
    </div>

    <div class="page-section">
        <h2 class="page-seciton__caption">4. Copyright</h2>
        <p class="page-section__paragraph">©2017 Worlds of Wow All Rights Reserved.</p>
        <p class="page-section__paragraph">By accessing this site and its content acknowledge and agree that all content contained within the site is protected
            by copyrights, trademarks, service marks, patents, and other intellectual property laws. You are only permitted
            to use the content as authorized by us, or its specific provider.</p>
        <p class="page-section__paragraph">Specifically, you may: temporarily download one copy of the content on Worlds of Wow's web site for personal,
            non-commercial transitory viewing only. This is the grant of a license, not a transfer of title.</p>
        <p class="page-section__paragraph">Specifically, you may not: modify or copy the content, use for any commercial purpose or display, attempt to
            decompile or reverse engineer any software on Worlds of Wow’s site, remove any copyright or other notations
            from the content, nor transfer the content to another person or “mirror” the content to another server.</p>
    </div>

    <div class="page-section">
        <h2 class="page-seciton__caption">5. Privacy Policy</h2>
        <p class="page-section__paragraph">This policy exists to inform you, the website visitor, how we (Worlds of Wow) collect and use information gained
            from the use of this site, and other means of communication.</p>
    </div>

    <div class="page-section">
        <h2 class="page-seciton__caption">6. Information We Collect</h2>
        <p class="page-section__paragraph">Worlds of Wow collects data throughout the course of each visitor’s time on the site. This data includes:</p>
        <div class="page-section__paragraph page-bullets">
            <ul class="page-bullets__list">
                <li>the domain from which you access the internet</li>
                <li>the date and time of your access to the pages within our site</li>
                <li>the address of the site you used to link to or access our site</li>
            </ul>
        </div>
        <p class="page-section__paragraph">This information is collected for statistical purposes to asses what information on our site is most/least popular,
            what pages may need updated, and performance of our web site in comparison to previous and industry trends.</p>
        <p class="page-section__paragraph">In addition, Worlds of Wow collects personally identifiable information when you specifically choose to provide
            such information. This personally identifiable information may be used for any of the following:</p>
        <div class="page-section__paragraph page-bullets">
            <ul class="page-bullets__list">
                <li>to respond to specific requests for correspondence or materials</li>
                <li>to register visitors for contests, email subscriptions, surveys, and other online activities</li>
                <li>to fulfill prizes</li>
                <li>to send notices of promotions and offers from Worlds of Wow</li>
                <li>to process employment applications and/or resumés</li>
            </ul>
        </div>
    </div>

    <div class="page-section">
        <h2 class="page-seciton__caption">7. How Your Information May Be Shared</h2>
        <p class="page-section__paragraph">Worlds of Wow will not sell, rent, loan, or transfer personally identifiable information except as noted in this
            policy.</p>
        <p class="page-section__paragraph">Worlds of Wow may share personally identifiable information to parties outside of Worlds of Wow in the following
            circumstances:</p>
        <div class="page-section__paragraph page-bullets">
            <ul class="page-bullets__list">
                <li>in the event of, or as part of preliminary due diligence related to, a corporate sale, merger, acquisition,
                    or similar event</li>
                <li>working with third party companies to support the Site’s technical operation or execute a specific promotion
                    or program (such as providing responses to any Comments Page, conduct surveys, or maintain a database
                    of visitor information, etc. </li>
                <li>in connection with processing submitted resume and/or employment application, Worlds of Wow and its affiliates
                    may disclose your Personal Information to any other third party necessary to further process your
                    resume and/or employment application</li>
            </ul>
        </div>
    </div>

    <div class="page-section">
        <h2 class="page-seciton__caption">Opt-In</h2>
        <p class="page-section__paragraph">When choosing to Opt-In to digital communications from Worlds of Wow you are authorizing the use of your email
            address and other personally identifiable information to be used for the purposes of:</p>
        <div class="page-section__paragraph page-bullets">
            <ul class="page-bullets__list">
                <li>notification of company and industry related news and events</li>
                <li>marketing and promotional messages about our company, products, services, and events</li>
            </ul>
        </div>
    </div>
</div>
TERMS;
            $layout = '1column';
            $pageDataUpdate = [
                'terms-and-conditions' => [
                    'url_key'         => 'terms-and-conditions',
                    'title'           => 'Terms and Conditions',
                    'content_heading' => 'Terms and Conditions',
                    'content'         => $termsPageContent,
                    'page_layout'     => $layout,
                    'store_id'        => [0]
                ],
            ];
            $this->updateCmsPages($pageDataUpdate);
        }

        if (version_compare($context->getVersion(), '0.0.13') < 0) {
            $homePageContent = <<<HOME
<div class="hero-banner">
    <img src="{{media url="wysiwyg/hero.png"}}" alt="herobanner">
    <div class="hero-banner__content">
        <div class="hero-banner__content-text">
                <span class="hero-banner__text">
                    Creating Cool, Fun Environments for Kids and Families
                </span>
            <a href="{{config path="web/secure/base_url"}}request-a-quote" class="hero-banner__link">Get started now!</a>
        </div>
    </div>
    <a href="#" data-section="block-products-list" class="next-section-scroller js-hook__next-section-scroller">
        <span class="next-section-scroller__icon"></span>
        <span class="next-section-scroller__arrow"></span><span class="next-section-scroller__arrow"></span>
    </a>
</div>

<!-- NEWEST CREATIONS widget here -->
{{widget type="Skynix\CatalogWidget\Block\Product\ProductsList" title="NEWEST CREATIONS" show_pager="0" show_link="1" products_count="4" template="Skynix_CatalogWidget::product/widget/content/grid.phtml" id_path="category/6" conditions_encoded="^[`1`:^[`type`:`Magento||CatalogWidget||Model||Rule||Condition||Combine`,`aggregator`:`all`,`value`:`1`,`new_child`:``^],`1--1`:^[`type`:`Magento||CatalogWidget||Model||Rule||Condition||Product`,`attribute`:`category_ids`,`operator`:`==`,`value`:`3`^]^]"}}

<div class="about-block">
    <div class="about-block__image">
        <img src="{{media url="wysiwyg/2efbdc_edcea12a31aa4ad38747c2816bf0edda_mv2_d_4032_3024_s_4_2.jpg"}}">
    </div>
    <div class="about-block__container">
        <div class="about-block__header">ABOUT WORLDS OF WOW</div>
        <div class="about-block__descr">Worlds of Wow! is a great company to partner with when you are looking to create themed environments and children’s playgrounds that engage kids and families. Churches, Pediatric Dental Offices, Family Entertainment Centers, Military Recreation Centers and YMCA's are where we do our best work.</div>
        <a href="/about" class="about-block__btn">View More</a>
    </div>
    <a href="#" data-section="block-banners" class="about-block__arrow js-hook__next-section-scroller"><img src="{{media url="wysiwyg/arrow-pointing-to-left.svg"}}"> DOWN</a>
</div>

<div class="widget block block-banners banner-items-wrapper">
    <ul class="banner-items js-hook__banners-slick">
        <li class="banner-item">
            <div class="banner-item-content">
                <img class="page-header__bg" src="{{media url="wysiwyg/homepage-our-work.jpg"}}">
                <div class="banner__content banner__content--red">
                    <div class="banner__content-text">
                        <h3 class="banner__title">Our work</h3>
                        <span class="banner__text">
                            We create ridiculously cool environments for kids and families. Take a look at some of our most popular themes.
                        </span>
                        <a href="/themes" class="banner__link">Learn more</a>
                    </div>
                </div>
            </div>
        </li>
        <li class="banner-item">
            <div class="banner-item-content">
                <img class="page-header__bg" src="{{media url="wysiwyg/homepage-about-us.jpg"}}">
                <div class="banner__content banner__content--green">
                    <div class="banner__content-text">
                        <h3 class="banner__title">About us</h3>
                        <span class="banner__text">
                            With over 600 completed projects across the country, we are the experts in themed environments and play.
                        </span>
                        <a href="/about" class="banner__link">Learn more</a>
                    </div>
                </div>
            </div>
        </li>
        <li class="banner-item" aria-hidden="true">
            <div class="banner-item-content">
                <img class="page-header__bg" src="{{media url="wysiwyg/designer-at-worlds-of-wow.jpg"}}">
                <div class="banner__content banner__content--blue">
                    <div class="banner__content-text">
                        <h3 class="banner__title">Our services</h3>
                        <span class="banner__text">
                            From concept to design to installation, we are your one-stop shop for theming and indoor playgrounds.
                        </span>
                        <a href="/process" class="banner__link">See All Services</a>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>

<div class="view-block">
    <div class="view-block-item">
        <div class="view-block-item__img">
            <img src="{{media url="wysiwyg/kids-indoor-toddler-play.jpg"}}">
        </div>
        <div class="view-block-item__content">
            <header class="view-block-item__header">VIEW BY THEME</header>
            <p class="view-block-item__p">Donec sed odio dui. Nullam quis risus eget<br> urna mollis ornare vel eu leo.</p>
            <a href="/themes" class="wow-button wow-button--yellow">View More</a>
        </div>
    </div>
    <div class="view-block-item">
        <div class="view-block-item__img">
            <img src="{{media url="wysiwyg/kelly-sikkema-155209-unsplash.jpg"}}">
        </div>
        <div class="view-block-item__content">
            <header class="view-block-item__header">VIEW BY CLIENTS</header>
            <p class="view-block-item__p">Donec sed odio dui. Nullam quis risus eget<br> urna mollis ornare vel eu leo.</p>
            <a href="#" class="wow-button wow-button--red">View More</a>
        </div>
    </div>
</div>

<!-- Latest blog posts widget here -->
{{widget type="TemplateMonster\Blog\Block\Widget\PostList" widget_status="1" title="Latest blog posts" post_amount="3" post_amount_per_row="3" is_enable_carousel="0" template="widget/post_list.phtml"}}
HOME;

            $this->page->load('home')
                ->setContent($homePageContent)
                ->setContentHeading('')
                ->setLayoutUpdateXml('<move element="form.subscribe" destination="page.wrapper" after="main.content"/>');
            $this->page->save();
        }

        if (version_compare($context->getVersion(), '0.0.14') < 0) {

            $this->store = $this->storeManager->getStore('default');
            $category = $this->categoryFactory
                ->create()
                ->loadByAttribute('url_key', 'facility');

            $setup->getConnection()->delete('catalog_category_entity_text', ['entity_id = ?' => $category->getId(), 'store_id = ?' => $this->store->getId()] );
            $this->updateMegamenu([

                'facility'        => [
                    'url_key' => 'facility',
                    'mm_turn_on'  => true,
                    'configurator' => [
                        [
                            'width' => 6,
                            'css_class' => 'megamenu-submenu-column',
                        ],
                        [
                            'width' => 6,
                            'css_class' => 'megamenu-submenu-image',
                        ],
                    ]
                ]
            ]);

        }

        if (version_compare($context->getVersion(), '0.0.15') < 0) {
            $termsPageContent = <<<TERMS
<div class="page-content">
    <div class="page-section">
        <h2 class="page-seciton__caption">1. Acceptance of Terms</h2>
        <p class="page-section__paragraph">Worlds of Wow provides content and services within the domain and subdomains of worldsofwow.com (collectively
            referred to as the “site”). By accessing this site, you are agreeing to be bound by these Terms of Use, all
            applicable laws and regulations, and agree that you are responsible for compliance with any applicable local
            laws. If you do not agree with any of these terms, you are prohibited from using or accessing this site.</p>
        <p class="page-section__paragraph">Any claim relating to Worlds of Wow’s web site shall be governed by the laws of the State of Texas without regard
            to its conflict of law provisions.</p>
    </div>

    <div class="page-section">
        <h2 class="page-seciton__caption">2. Disclaimer</h2>
        <p class="page-section__paragraph">The content on Worlds of Wow’s web site is provided "as is." Worlds of Wow makes no warranties, expressed or
            implied, including without limitation, implied warranties or conditions of merchantability, fitness for a
            particular purpose, or non-infringement of intellectual property or other violation of rights. Worlds of
            Wow does not warrant or make any representations concerning the accuracy, likely results, or reliability
            of the use of the materials on its web site or to materials and sites linked to or from this site.</p>
        <p class="page-section__paragraph">Further, the content on this website may contain technical, typographical, or photographic errors. Worlds of
            Wow does not guarantee any of the content on its website to be complete, accurate, or up-to-date. Accessing
            this site’s content, as well as downloading any contained or linked content is done at your own discretion.
            Worlds of Wow may update or revise site content at any time without notice, but does not commit to make such
            changes.</p>
    </div>


    <div class="page-section">
        <h2 class="page-seciton__caption">3. Limitations</h2>
        <p class="page-section__paragraph">Neither Worlds of Wow, nor its affiliates, will be held liable for any damages due to the use or access, or inability
            to use or access, the content contained within this website. In jurisdictions where there are restrictions
            on implied warranties, or limitations of liability are not allowed, these limitations may not apply.</p>
    </div>

    <div class="page-section">
        <h2 class="page-seciton__caption">4. Copyright</h2>
        <p class="page-section__paragraph">©2017 Worlds of Wow All Rights Reserved.</p>
        <p class="page-section__paragraph">By accessing this site and its content acknowledge and agree that all content contained within the site is protected
            by copyrights, trademarks, service marks, patents, and other intellectual property laws. You are only permitted
            to use the content as authorized by us, or its specific provider.</p>
        <p class="page-section__paragraph">Specifically, you may: temporarily download one copy of the content on Worlds of Wow's web site for personal,
            non-commercial transitory viewing only. This is the grant of a license, not a transfer of title.</p>
        <p class="page-section__paragraph">Specifically, you may not: modify or copy the content, use for any commercial purpose or display, attempt to
            decompile or reverse engineer any software on Worlds of Wow’s site, remove any copyright or other notations
            from the content, nor transfer the content to another person or “mirror” the content to another server.</p>
    </div>

    <div class="page-section">
        <h2 class="page-seciton__caption">5. Privacy Policy</h2>
        <p class="page-section__paragraph">This policy exists to inform you, the website visitor, how we (Worlds of Wow) collect and use information gained
            from the use of this site, and other means of communication.</p>
    </div>

    <div class="page-section">
        <h2 class="page-seciton__caption">6. Information We Collect</h2>
        <p class="page-section__paragraph">Worlds of Wow collects data throughout the course of each visitor’s time on the site. This data includes:</p>
        <div class="page-section__paragraph page-bullets">
            <ul class="page-bullets__list">
                <li>the domain from which you access the internet</li>
                <li>the date and time of your access to the pages within our site</li>
                <li>the address of the site you used to link to or access our site</li>
            </ul>
        </div>
        <p class="page-section__paragraph">This information is collected for statistical purposes to asses what information on our site is most/least popular,
            what pages may need updated, and performance of our web site in comparison to previous and industry trends.</p>
        <p class="page-section__paragraph">In addition, Worlds of Wow collects personally identifiable information when you specifically choose to provide
            such information. This personally identifiable information may be used for any of the following:</p>
        <div class="page-section__paragraph page-bullets">
            <ul class="page-bullets__list">
                <li>to respond to specific requests for correspondence or materials</li>
                <li>to register visitors for contests, email subscriptions, surveys, and other online activities</li>
                <li>to fulfill prizes</li>
                <li>to send notices of promotions and offers from Worlds of Wow</li>
                <li>to process employment applications and/or resumés</li>
            </ul>
        </div>
    </div>

    <div class="page-section">
        <h2 class="page-seciton__caption">7. How Your Information May Be Shared</h2>
        <p class="page-section__paragraph">Worlds of Wow will not sell, rent, loan, or transfer personally identifiable information except as noted in this
            policy.</p>
        <p class="page-section__paragraph">Worlds of Wow may share personally identifiable information to parties outside of Worlds of Wow in the following
            circumstances:</p>
        <div class="page-section__paragraph page-bullets">
            <ul class="page-bullets__list">
                <li>in the event of, or as part of preliminary due diligence related to, a corporate sale, merger, acquisition,
                    or similar event</li>
                <li>working with third party companies to support the Site’s technical operation or execute a specific promotion
                    or program (such as providing responses to any Comments Page, conduct surveys, or maintain a database
                    of visitor information, etc. </li>
                <li>in connection with processing submitted resume and/or employment application, Worlds of Wow and its affiliates
                    may disclose your Personal Information to any other third party necessary to further process your
                    resume and/or employment application</li>
            </ul>
        </div>
    </div>

    <div class="page-section">
        <h2 class="page-seciton__caption">8. Opt-In</h2>
        <p class="page-section__paragraph">When choosing to Opt-In to digital communications from Worlds of Wow you are authorizing the use of your email
            address and other personally identifiable information to be used for the purposes of:</p>
        <div class="page-section__paragraph page-bullets">
            <ul class="page-bullets__list">
                <li>notification of company and industry related news and events</li>
                <li>marketing and promotional messages about our company, products, services, and events</li>
            </ul>
        </div>
    </div>
</div>
TERMS;
            $layout = '1column';
            $pageDataUpdate = [
                'terms-and-conditions' => [
                    'url_key'         => 'terms-and-conditions',
                    'title'           => 'Terms and Conditions',
                    'content_heading' => 'Terms and Conditions',
                    'content'         => $termsPageContent,
                    'page_layout'     => $layout,
                    'store_id'        => [0]
                ],
            ];
            $this->updateCmsPages($pageDataUpdate);
        }

        if (version_compare($context->getVersion(), '0.0.16') < 0) {
            $homePageContent = <<<HOME
<div class="hero-banner">
    <img src="{{media url="wysiwyg/hero.png"}}" alt="herobanner">
    <div class="hero-banner__content">
        <div class="hero-banner__content-text">
                <span class="hero-banner__text">
                    Creating Cool, Fun Environments for Kids and Families
                </span>
            <a href="{{config path="web/secure/base_url"}}request-a-quote" class="hero-banner__link">Get started now!</a>
        </div>
    </div>
    <a href="#" data-section="block-products-list" class="next-section-scroller js-hook__next-section-scroller">
        <span class="next-section-scroller__icon"></span>
        <span class="next-section-scroller__arrow"></span><span class="next-section-scroller__arrow"></span>
    </a>
</div>

<!-- NEWEST CREATIONS widget here -->
{{widget type="Skynix\CatalogWidget\Block\Product\ProductsList" title="NEWEST CREATIONS" show_pager="0" show_link="1" products_count="4" template="Skynix_CatalogWidget::product/widget/content/grid.phtml" id_path="category/6" conditions_encoded="^[`1`:^[`type`:`Magento||CatalogWidget||Model||Rule||Condition||Combine`,`aggregator`:`all`,`value`:`1`,`new_child`:``^],`1--1`:^[`type`:`Magento||CatalogWidget||Model||Rule||Condition||Product`,`attribute`:`category_ids`,`operator`:`==`,`value`:`3`^]^]"}}

<div class="about-block">
    <div class="about-block__image">
        <img src="{{media url="wysiwyg/2efbdc_edcea12a31aa4ad38747c2816bf0edda_mv2_d_4032_3024_s_4_2.jpg"}}">
    </div>
    <div class="about-block__container">
        <div class="about-block__header">ABOUT WORLDS OF WOW</div>
        <div class="about-block__descr">Worlds of Wow! is a great company to partner with when you are looking to create themed environments and children’s playgrounds that engage kids and families. Churches, Pediatric Dental Offices, Family Entertainment Centers, Military Recreation Centers and YMCA's are where we do our best work.</div>
        <a href="/about" class="about-block__btn">View More</a>
    </div>
    <a href="#" data-section="block-banners" class="about-block__arrow js-hook__next-section-scroller"><img src="{{media url="wysiwyg/arrow-pointing-to-left.svg"}}"> DOWN</a>
</div>

<div class="widget block block-banners banner-items-wrapper">
    <ul class="banner-items js-hook__banners-slick">
        <li class="banner-item">
            <div class="banner-item-content">
                <img class="page-header__bg" src="{{media url="wysiwyg/homepage-our-work.jpg"}}">
                <div class="banner__content banner__content--red">
                    <div class="banner__content-text">
                        <h3 class="banner__title">Our work</h3>
                        <span class="banner__text">
                            We create ridiculously cool environments for kids and families. Take a look at some of our most popular themes.
                        </span>
                        <a href="/themes" class="banner__link">See Themes</a>
                    </div>
                </div>
            </div>
        </li>
        <li class="banner-item">
            <div class="banner-item-content">
                <img class="page-header__bg" src="{{media url="wysiwyg/homepage-about-us.jpg"}}">
                <div class="banner__content banner__content--green">
                    <div class="banner__content-text">
                        <h3 class="banner__title">About us</h3>
                        <span class="banner__text">
                            With over 600 completed projects across the country, we are the experts in themed environments and play.
                        </span>
                        <a href="/about" class="banner__link">Learn more</a>
                    </div>
                </div>
            </div>
        </li>
        <li class="banner-item" aria-hidden="true">
            <div class="banner-item-content">
                <img class="page-header__bg" src="{{media url="wysiwyg/designer-at-worlds-of-wow.jpg"}}">
                <div class="banner__content banner__content--blue">
                    <div class="banner__content-text">
                        <h3 class="banner__title">Our services</h3>
                        <span class="banner__text">
                            From concept to design to installation, we are your one-stop shop for theming and indoor playgrounds.
                        </span>
                        <a href="/process" class="banner__link">See All Services</a>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>

<div class="view-block">
    <div class="view-block-item">
        <div class="view-block-item__img">
            <img src="{{media url="wysiwyg/kids-indoor-toddler-play.jpg"}}">
        </div>
        <div class="view-block-item__content">
            <header class="view-block-item__header">VIEW BY THEME</header>
            <p class="view-block-item__p">Donec sed odio dui. Nullam quis risus eget<br> urna mollis ornare vel eu leo.</p>
            <a href="/themes" class="wow-button wow-button--yellow">View More</a>
        </div>
    </div>
    <div class="view-block-item">
        <div class="view-block-item__img">
            <img src="{{media url="wysiwyg/kelly-sikkema-155209-unsplash.jpg"}}">
        </div>
        <div class="view-block-item__content">
            <header class="view-block-item__header">VIEW BY CLIENTS</header>
            <p class="view-block-item__p">Donec sed odio dui. Nullam quis risus eget<br> urna mollis ornare vel eu leo.</p>
            <a href="#" class="wow-button wow-button--red">View More</a>
        </div>
    </div>
</div>

<!-- Latest blog posts widget here -->
{{widget type="TemplateMonster\Blog\Block\Widget\PostList" widget_status="1" title="Latest blog posts" post_amount="3" post_amount_per_row="3" is_enable_carousel="0" template="widget/post_list.phtml"}}
HOME;

            $this->page->load('home')
                ->setContent($homePageContent)
                ->setContentHeading('')
                ->setLayoutUpdateXml('<move element="form.subscribe" destination="page.wrapper" after="main.content"/>');
            $this->page->save();
        }

        if (version_compare($context->getVersion(), '0.0.17') < 0) {

            $this->updateCategories([
                'facility'        => [
                    'url_key'          => 'facility',
                    'name'             => 'Services',
                    'meta_title'       => 'Services',
                    'display_mode'     => Category::DM_PRODUCT,
                    'is_active'        => true,
                    'is_anchor'        => false,
                    'include_in_menu'  => true,
                    'mm_turn_on'       => 1,
                    'mm_css_class'     => 'menu-colored-item menu-colored-item--blue megamenu-submenu',
                ],
            ]);
            $this->fixUrlRewrites();
        }

        if (version_compare($context->getVersion(), '0.0.18') < 0) {
            $homePageContent = <<<HOME
<div class="hero-banner">
    <img src="{{media url="wysiwyg/hero.png"}}" alt="herobanner">
    <div class="hero-banner__content">
        <div class="hero-banner__content-text">
                <span class="hero-banner__text">
                    Creating Cool, Fun Environments for Kids and Families
                </span>
            <a href="{{config path="web/secure/base_url"}}request-a-quote" class="hero-banner__link">Get started now!</a>
        </div>
    </div>
    <a href="#" data-section="block-products-list" class="next-section-scroller js-hook__next-section-scroller">
        <span class="next-section-scroller__icon"></span>
        <span class="next-section-scroller__arrow"></span><span class="next-section-scroller__arrow"></span>
    </a>
</div>

<!-- NEWEST CREATIONS widget here -->
{{widget type="Skynix\CatalogWidget\Block\Product\ProductsList" title="NEWEST CREATIONS" show_pager="0" show_link="1" products_count="4" template="Skynix_CatalogWidget::product/widget/content/grid.phtml" id_path="category/6" conditions_encoded="^[`1`:^[`type`:`Magento||CatalogWidget||Model||Rule||Condition||Combine`,`aggregator`:`all`,`value`:`1`,`new_child`:``^],`1--1`:^[`type`:`Magento||CatalogWidget||Model||Rule||Condition||Product`,`attribute`:`category_ids`,`operator`:`==`,`value`:`3`^]^]"}}

<div class="about-block">
    <div class="about-block__image">
        <img src="{{media url="wysiwyg/2efbdc_edcea12a31aa4ad38747c2816bf0edda_mv2_d_4032_3024_s_4_2.jpg"}}">
    </div>
    <div class="about-block__container">
        <div class="about-block__header">ABOUT WORLDS OF WOW</div>
        <div class="about-block__descr">Worlds of Wow! is a great company to partner with when you are looking to create themed environments and children’s playgrounds that engage kids and families. Churches, Pediatric Dental Offices, Family Entertainment Centers, Military Recreation Centers and YMCA's are where we do our best work.</div>
        <a href="/about" class="about-block__btn">View More</a>
    </div>
    <a href="#" data-section="block-banners" class="about-block__arrow js-hook__next-section-scroller"><img src="{{media url="wysiwyg/arrow-pointing-to-left.svg"}}"> DOWN</a>
</div>

<div class="widget block block-banners banner-items-wrapper">
    <ul class="banner-items js-hook__banners-slick">
        <li class="banner-item">
            <div class="banner-item-content">
                <img class="page-header__bg" src="{{media url="wysiwyg/homepage-our-work.jpg"}}">
                <div class="banner__content banner__content--red">
                    <div class="banner__content-text">
                        <h3 class="banner__title">Our work</h3>
                        <span class="banner__text">
                            We create ridiculously cool environments for kids and families. Take a look at some of our most popular themes.
                        </span>
                        <a href="/themes" class="banner__link">See Themes</a>
                    </div>
                </div>
            </div>
        </li>
        <li class="banner-item">
            <div class="banner-item-content">
                <img class="page-header__bg" src="{{media url="wysiwyg/homepage-about-us.jpg"}}">
                <div class="banner__content banner__content--green">
                    <div class="banner__content-text">
                        <h3 class="banner__title">About us</h3>
                        <span class="banner__text">
                            With over 600 completed projects across the country, we are the experts in themed environments and play.
                        </span>
                        <a href="/about" class="banner__link">Learn more</a>
                    </div>
                </div>
            </div>
        </li>
        <li class="banner-item" aria-hidden="true">
            <div class="banner-item-content">
                <img class="page-header__bg" src="{{media url="wysiwyg/designer-at-worlds-of-wow.jpg"}}">
                <div class="banner__content banner__content--blue">
                    <div class="banner__content-text">
                        <h3 class="banner__title">Our services</h3>
                        <span class="banner__text">
                            From concept to design to installation, we are your one-stop shop for theming and indoor playgrounds.
                        </span>
                        <a href="/process" class="banner__link">See All Services</a>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>

<div class="view-block">
    <div class="view-block-item">
        <div class="view-block-item__img">
            <img src="{{media url="wysiwyg/kids-indoor-toddler-play.jpg"}}">
        </div>
        <div class="view-block-item__content">
            <header class="view-block-item__header">VIEW BY THEME</header>
            <p class="view-block-item__p">Donec sed odio dui. Nullam quis risus eget<br> urna mollis ornare vel eu leo.</p>
            <a href="/themes" class="wow-button wow-button--yellow">View More</a>
        </div>
    </div>
    <div class="view-block-item">
        <div class="view-block-item__img">
            <img src="{{media url="wysiwyg/kelly-sikkema-155209-unsplash.jpg"}}">
        </div>
        <div class="view-block-item__content">
            <header class="view-block-item__header">VIEW BY CLIENTS</header>
            <p class="view-block-item__p">Donec sed odio dui. Nullam quis risus eget<br> urna mollis ornare vel eu leo.</p>
            <a href="/market" class="wow-button wow-button--red">View More</a>
        </div>
    </div>
</div>

<!-- Latest blog posts widget here -->
{{widget type="TemplateMonster\Blog\Block\Widget\PostList" widget_status="1" title="Latest blog posts" post_amount="3" post_amount_per_row="3" is_enable_carousel="0" template="widget/post_list.phtml"}}
HOME;

            $this->page->load('home')
                ->setContent($homePageContent)
                ->setContentHeading('')
                ->setLayoutUpdateXml('<move element="form.subscribe" destination="page.wrapper" after="main.content"/>');
            $this->page->save();
        }

        if (version_compare($context->getVersion(), '0.0.19') < 0) {

            $urlInterface = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\UrlInterface');
            $baseUrl = $urlInterface->getBaseUrl();
            $url = $baseUrl . 'process' . DIRECTORY_SEPARATOR . 'pdf' . DIRECTORY_SEPARATOR . 'download';

            $ourProcessPageContent   = <<<PROCCESS
<div class="page-content">
	<div class="page-section">
		<h2 class="page-section__caption">Design Consultation</h2>
		<p class="page-section__paragraph">This gets the creativity started. Once you sign your creative agreement, we do an onsite and/or virtual consultation to
			generate a creative brief.</p>
		<p class="page-section__paragraph">We become familiar with your space, vision, style, budget and more. Typically it's a one day visit that includes meetings
			and lots of fun.</p>
	</div>
	<div class="page-section">
		<h2 class="page-section__caption">Concept Phase</h2>
		<p class="page-section__paragraph">Dreams & ideas come to life on paper. We start with sketches and seek your input. Sometimes we hit a home run the first
			time, sometimes it takes a few tries to get it just right.</p>
		<p class="page-section__paragraph">We then wrap up with a production agreement. This is a written document of what's in the concept sketches and renderings,
			and the work that will be completed in your space.</p>
	</div>
	<div class="page-section">
		<h2 class="page-section__caption">Design Phase</h2>
		<p class="page-section__paragraph">Our design team takes the production agreement, rough sketches and concept elements and converts them into elevation drawings.
			This is where the rough sketches snap into focus and becomes a polished work of art!</p>
	</div>
	<div class="page-section">
		<h2 class="page-section__caption">Production Art Phase</h2>
		<p class="page-section__paragraph">Visions come to life in this phase as we physically produce the final artwork to be placed on your walls, creating your
			ridiculously cool original theme.</p>
	</div>
	<div class="page-section">
		<h2 class="page-section__caption">Fabrication Phase</h2>
		<p class="page-section__paragraph">We handcraft each of the unique pieces for your project at this point. Our install team produces the 2D appliques, 3D elements
			and play attractions for the final installation.</p>
	</div>
	<div class="page-section">
		<h2 class="page-section__caption">Installation Phase</h2>
		<p class="page-section__paragraph">Our teams are coordinated, the unique elements are shipped to your site and installation begins! The size of the project
			always dictates our time on your site, typically 5 - 14 days.</p>
		<p class="page-section__paragraph">During this phase you will see the transformation taking place. The final step is the sign off form indicating the project
			is complete. Our goal is to make you very, very happy!</p>
	</div>
	<div class="page-section">
		<h2 class="page-section__caption">Enjoyment Phase</h2>
		<p class="page-section__paragraph">This is where you and your team enjoy the amazing environment that you and Worlds of Wow created together.</p>
		<p class="page-section__paragraph">Through the successful completion of each project, our hope and mission is to equip the local church to impact their community
			by providing environments that foster life change.</p>
	</div>
	<div class="page-section page-section--centered">
		<a class="page-section__download-btn" href="{$url}" target="_Blank">Click Here to Download Our Process</a>
	</div>

	<hr>
	<div class="page-section page-section--centered">
		<h2 class="page-section__caption">
			So what are you waiting for? Let's get started today!
		</h2>
	</div>
</div>
PROCCESS;

            $layout = '1column';
            $pagesDataUpdate = [
                'process'              => [
                    'url_key'         => 'process',
                    'title'           => 'Our Process',
                    'content_heading' => 'Our Process',
                    'content'         => $ourProcessPageContent,
                    'page_layout'     => $layout,
                    'store_id'        => [0]
                ]
            ];
            $this->updateCmsPages($pagesDataUpdate);
        }

        if (version_compare($context->getVersion(), '0.0.20') < 0) {
            $categoriesData = [
                'request_a_quote' => [
                    'url_key' => 'request-a-quote',
                    'name' => 'Request a quote',
                    'meta_title' => 'Request a quote',
                    'display_mode' => Category::DM_PAGE,
                    'is_active' => true,
                    'is_anchor' => false,
                    'include_in_menu' => true,
                    'mm_turn_on' => 0,
                    'mm_css_class' => 'menu-colored-item menu-colored-item--yellow',
                    'custom_layout_update' => '<referenceContainer name="content">
                                                    <block class="Skynix\Forms\Block\Quote\Form" after="-" name="product.contact.form" template="Skynix_Forms::quote/form.phtml" />
                                                </referenceContainer>',
                ],
            ];
            $this->updateCategories($categoriesData);
        }

        if (version_compare($context->getVersion(), '0.0.21') < 0) {
            $page = $this->pageFactory->create();
            $page->setTitle('Thank You')
                ->setIdentifier('thank-you')
                ->setIsActive(true)
                ->setPageLayout('1column')
                ->setStores([0])
                ->setUrl('thank-you')
                ->setContent('<div class="padding-s">
                                  <div class="page-title category-title">
                                    <h1>Thank You</h1>
                                  </div>
                                  <div class="category-description std">
                                    <p><span>Thank you for contacting <strong>Worlds of WOW</strong>!</span></p>
                                    <p><span>Please stay tuned. One of our representatives will be in touch soon.</span></p>
                                  </div>
                              </div>')
                ->save();
        }

        if (version_compare($context->getVersion(), '0.0.22') < 0) {
            $homePageContent = <<<HOME
<div class="hero-banner">
    <img src="{{media url="wysiwyg/hero.png"}}" alt="herobanner">
    <div class="hero-banner__content">
        <div class="hero-banner__content-text">
                <span class="hero-banner__text">
                    Creating Cool, Fun Environments for Kids and Families
                </span>
            <a href="{{config path="web/secure/base_url"}}request-a-quote" class="hero-banner__link">Get started now!</a>
        </div>
    </div>
    <a href="#" data-section="block-products-list" class="next-section-scroller js-hook__next-section-scroller">
        <span class="next-section-scroller__icon"></span>
        <span class="next-section-scroller__arrow"></span><span class="next-section-scroller__arrow"></span>
    </a>
</div>

<!-- NEWEST CREATIONS widget here -->
{{widget type="Skynix\CatalogWidget\Block\Product\ProductsList" title="NEWEST CREATIONS" show_pager="0" show_link="1" products_count="4" template="Skynix_CatalogWidget::product/widget/content/grid.phtml" id_path="category/6" conditions_encoded="^[`1`:^[`type`:`Magento||CatalogWidget||Model||Rule||Condition||Combine`,`aggregator`:`all`,`value`:`1`,`new_child`:``^],`1--1`:^[`type`:`Magento||CatalogWidget||Model||Rule||Condition||Product`,`attribute`:`category_ids`,`operator`:`==`,`value`:`3`^]^]"}}

<div class="about-block">
    <div class="about-block__image">
        <img src="{{media url="wysiwyg/2efbdc_edcea12a31aa4ad38747c2816bf0edda_mv2_d_4032_3024_s_4_2.jpg"}}">
    </div>
    <div class="about-block__container">
        <div class="about-block__header">ABOUT WORLDS OF WOW</div>
        <div class="about-block__descr">Worlds of Wow! is a great company to partner with when you are looking to create themed environments and children’s playgrounds that engage kids and families. Churches, Pediatric Dental Offices, Family Entertainment Centers, Military Recreation Centers and YMCA's are where we do our best work.</div>
        <a href="/about" class="about-block__btn">View More</a>
    </div>
    <a href="#" data-section="block-banners" class="about-block__arrow js-hook__next-section-scroller"><img src="{{media url="wysiwyg/arrow-pointing-to-left.svg"}}"> DOWN</a>
</div>

<div class="widget block block-banners banner-items-wrapper">
    <ul class="banner-items js-hook__banners-slick">
        <li class="banner-item">
            <div class="banner-item-content">
                <img class="page-header__bg" src="{{media url="wysiwyg/homepage-our-work.jpg"}}">
                <div class="banner__content banner__content--red">
                    <div class="banner__content-text">
                        <h3 class="banner__title">Our work</h3>
                        <span class="banner__text">
                            We create ridiculously cool environments for kids and families. Take a look at some of our most popular themes.
                        </span>
                        <a href="/themes" class="banner__link">See Themes</a>
                    </div>
                </div>
            </div>
        </li>
        <li class="banner-item">
            <div class="banner-item-content">
                <img class="page-header__bg" src="{{media url="wysiwyg/homepage-about-us.jpg"}}">
                <div class="banner__content banner__content--green">
                    <div class="banner__content-text">
                        <h3 class="banner__title">About us</h3>
                        <span class="banner__text">
                            With over 600 completed projects across the country, we are the experts in themed environments and play.
                        </span>
                        <a href="/about" class="banner__link">Learn more</a>
                    </div>
                </div>
            </div>
        </li>
        <li class="banner-item" aria-hidden="true">
            <div class="banner-item-content">
                <img class="page-header__bg" src="{{media url="wysiwyg/designer-at-worlds-of-wow.jpg"}}">
                <div class="banner__content banner__content--blue">
                    <div class="banner__content-text">
                        <h3 class="banner__title">Our services</h3>
                        <span class="banner__text">
                            From concept to design to installation, we are your one-stop shop for theming and indoor playgrounds.
                        </span>
                        <a href="/process" class="banner__link">See All Services</a>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>

<div class="view-block">
    <div class="view-block-item">
        <div class="view-block-item__img">
            <img src="{{media url="wysiwyg/kids-indoor-toddler-play.jpg"}}">
        </div>
        <div class="view-block-item__content">
            <header class="view-block-item__header">VIEW BY THEME</header>
            <p class="view-block-item__p">Donec sed odio dui. Nullam quis risus eget<br> urna mollis ornare vel eu leo.</p>
            <a href="/themes" class="wow-button wow-button--yellow">View More</a>
        </div>
    </div>
    <div class="view-block-item">
        <div class="view-block-item__img">
            <img src="{{media url="wysiwyg/kelly-sikkema-155209-unsplash.jpg"}}">
        </div>
        <div class="view-block-item__content">
            <header class="view-block-item__header">VIEW BY CLIENTS</header>
            <p class="view-block-item__p">Donec sed odio dui. Nullam quis risus eget<br> urna mollis ornare vel eu leo.</p>
            <a href="/market" class="wow-button wow-button--red">View More</a>
        </div>
    </div>
</div>

<!-- Latest blog posts widget here -->
{{widget type="TemplateMonster\Blog\Block\Widget\PostList" widget_status="1" title="Latest blog posts" post_amount="3" post_amount_per_row="3" is_enable_carousel="0" template="widget/post_list.phtml"}}
HOME;

            $this->page->load('home')
                ->setContent($homePageContent)
                ->setContentHeading('')
                ->setLayoutUpdateXml('');
            $this->page->save();
        }

        if (version_compare($context->getVersion(), '0.0.23') < 0) {
            $homePageContent = <<<HOME
<div class="hero-banner">
    <img src="{{media url="wysiwyg/hero.png"}}" alt="herobanner">
    <div class="hero-banner__content">
        <div class="hero-banner__content-text">
                <span class="hero-banner__text">
                    Creating Cool, Fun Environments for Kids and Families
                </span>
            <a href="{{config path="web/secure/base_url"}}request-a-quote" class="hero-banner__link">Get started now!</a>
        </div>
    </div>
    <a href="#" data-section="block-products-list" class="next-section-scroller js-hook__next-section-scroller">
        <span class="next-section-scroller__icon"></span>
        <span class="next-section-scroller__arrow"></span><span class="next-section-scroller__arrow"></span>
    </a>
</div>

<!-- NEWEST CREATIONS widget here -->
{{widget type="Skynix\CatalogWidget\Block\Product\ProductsList" title="NEWEST CREATIONS" show_pager="0" show_link="1" products_count="4" template="Skynix_CatalogWidget::product/widget/content/grid.phtml" id_path="category/6" conditions_encoded="^[`1`:^[`type`:`Magento||CatalogWidget||Model||Rule||Condition||Combine`,`aggregator`:`all`,`value`:`1`,`new_child`:``^],`1--1`:^[`type`:`Magento||CatalogWidget||Model||Rule||Condition||Product`,`attribute`:`category_ids`,`operator`:`==`,`value`:`3`^]^]"}}

<div class="about-block">
    <div class="about-block__image">
        <img src="{{media url="wysiwyg/2efbdc_edcea12a31aa4ad38747c2816bf0edda_mv2_d_4032_3024_s_4_2.jpg"}}">
    </div>
    <div class="about-block__container">
        <div class="about-block__header">ABOUT WORLDS OF WOW!</div>
        <div class="about-block__descr">Worlds of Wow! is a great company to partner with when you are looking to create themed environments and children’s playgrounds that engage kids and families. Churches, Pediatric Dental Offices, Family Entertainment Centers, Military Recreation Centers and YMCA's are where we do our best work.</div>
        <a href="/about" class="about-block__btn">View More</a>
    </div>
    <a href="#" data-section="block-banners" class="about-block__arrow js-hook__next-section-scroller"><img src="{{media url="wysiwyg/arrow-pointing-to-left.svg"}}"> DOWN</a>
</div>

<div class="widget block block-banners banner-items-wrapper">
    <ul class="banner-items js-hook__banners-slick">
        <li class="banner-item">
            <div class="banner-item-content">
                <img class="page-header__bg" src="{{media url="wysiwyg/homepage-our-work.jpg"}}">
                <div class="banner__content banner__content--red">
                    <div class="banner__content-text">
                        <h3 class="banner__title">Our work</h3>
                        <span class="banner__text">
                            We create ridiculously cool environments for kids and families. Take a look at some of our most popular themes.
                        </span>
                        <a href="/themes" class="banner__link">See Themes</a>
                    </div>
                </div>
            </div>
        </li>
        <li class="banner-item">
            <div class="banner-item-content">
                <img class="page-header__bg" src="{{media url="wysiwyg/homepage-about-us.jpg"}}">
                <div class="banner__content banner__content--green">
                    <div class="banner__content-text">
                        <h3 class="banner__title">About us</h3>
                        <span class="banner__text">
                            With over 600 completed projects across the country, we are the experts in themed environments and play.
                        </span>
                        <a href="/about" class="banner__link">Learn more</a>
                    </div>
                </div>
            </div>
        </li>
        <li class="banner-item" aria-hidden="true">
            <div class="banner-item-content">
                <img class="page-header__bg" src="{{media url="wysiwyg/designer-at-worlds-of-wow.jpg"}}">
                <div class="banner__content banner__content--blue">
                    <div class="banner__content-text">
                        <h3 class="banner__title">Our services</h3>
                        <span class="banner__text">
                            From concept to design to installation, we are your one-stop shop for theming and indoor playgrounds.
                        </span>
                        <a href="/process" class="banner__link">See All Services</a>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>

<div class="view-block">
    <div class="view-block-item">
        <div class="view-block-item__img">
            <img src="{{media url="wysiwyg/kids-indoor-toddler-play.jpg"}}">
        </div>
        <div class="view-block-item__content">
            <header class="view-block-item__header">VIEW BY THEME</header>
            <p class="view-block-item__p">Donec sed odio dui. Nullam quis risus eget<br> urna mollis ornare vel eu leo.</p>
            <a href="/themes" class="wow-button wow-button--yellow">View More</a>
        </div>
    </div>
    <div class="view-block-item">
        <div class="view-block-item__img">
            <img src="{{media url="wysiwyg/kelly-sikkema-155209-unsplash.jpg"}}">
        </div>
        <div class="view-block-item__content">
            <header class="view-block-item__header">VIEW BY CLIENTS</header>
            <p class="view-block-item__p">Donec sed odio dui. Nullam quis risus eget<br> urna mollis ornare vel eu leo.</p>
            <a href="/market" class="wow-button wow-button--red">View More</a>
        </div>
    </div>
</div>

<!-- Latest blog posts widget here -->
{{widget type="TemplateMonster\Blog\Block\Widget\PostList" widget_status="1" title="Latest blog posts" post_amount="3" post_amount_per_row="3" is_enable_carousel="0" template="widget/post_list.phtml"}}
HOME;

            $this->page->load('home')
                ->setContent($homePageContent)
                ->setContentHeading('')
                ->setLayoutUpdateXml('');
            $this->page->save();
        }

        if (version_compare($context->getVersion(), '0.0.24') < 0) {


            $this->updateMegamenu([

                'market'          => [
                    'url_key' => 'market',
                    'mm_turn_on'  => true,
                    'configurator' => [
                        [
                            'width' => 4,
                            'css_class' => 'megamenu-submenu-column',
                        ],
                        [
                            'width' => 4,
                            'css_class' => 'megamenu-submenu-column',
                        ],
                        [
                            'width' => 4,
                            'css_class' => 'megamenu-submenu-image',
                        ],
                    ]
                ],
            ]);

        }

        if (version_compare($context->getVersion(), '0.0.25') < 0) {

            $connection = $setup->getConnection();

            $query = $connection
                ->select()
                ->from('eav_attribute', ['attribute_id'])
                ->joinLeft('eav_entity_type', 'eav_entity_type.entity_type_id = eav_attribute.entity_type_id', [])
                ->where('eav_entity_type.entity_type_code = "catalog_product"')
                ->where('eav_attribute.attribute_code = "description"');
            $attrId = $connection->fetchOne($query);

            $query = $connection
                ->select()
                ->from('catalog_product_entity_text', ['value_id', 'entity_id'])
                ->where('catalog_product_entity_text.store_id = 0')
                ->where('catalog_product_entity_text.attribute_id = ?', $attrId);
            $data = $connection->fetchAll($query);


            foreach ($data as $item) {
                $query = $connection
                    ->select()
                    ->from('catalog_product_entity_text', ['entity_id'])
                    ->where('entity_id = ?', $item['entity_id'])
                    ->where('store_id = 1')
                    ->where('attribute_id = ?', $attrId);
                $dataAttr = $connection->fetchAll($query);

                if(!empty($dataAttr)) {
                    $connection->delete('catalog_product_entity_text', ['entity_id = ?' => $item['entity_id'], 'attribute_id = ?' => $attrId, 'store_id = ?' => 0]);
                    $connection->update('catalog_product_entity_text', ['store_id' => 0], ['entity_id = ?' => $item['entity_id'], 'attribute_id = ?' => $attrId, 'store_id = ?' => 1]);
                }
            }
        }

        if (version_compare($context->getVersion(), '0.0.26') < 0) {
            $this->updateStaticBlocks([
                'homepage-herobanner'        => [
                    'title'     => 'Hero Banner',
                    'stores'    => [0],
                    'is_active' => true,
                    'content'   => <<<HEROBANNER
<div class="hero-banner">
    <img src="{{media url="wysiwyg/hero.png"}}" alt="herobanner">
    <div class="hero-banner__content">
        <div class="hero-banner__content-text">
                <span class="hero-banner__text">
                    Creating Cool, Fun Environments for Kids and Families
                </span>
            <a href="{{config path="web/secure/base_url"}}request-a-quote" class="hero-banner__link">Get started now!</a>
        </div>
    </div>
    <a href="#" data-section="block-products-list" class="next-section-scroller js-hook__next-section-scroller">
        <span class="next-section-scroller__icon"></span>
        <span class="next-section-scroller__arrow"></span><span class="next-section-scroller__arrow"></span>
    </a>
</div>
HEROBANNER
                ],
                'homepage-about-wow'         => [
                    'title'     => 'About WOW',
                    'stores'    => [0],
                    'is_active' => true,
                    'content'   => <<<ABOUT
<div class="about-block">
    <div class="about-block__image">
        <img src="{{media url="wysiwyg/2efbdc_edcea12a31aa4ad38747c2816bf0edda_mv2_d_4032_3024_s_4_2.jpg"}}">
    </div>
    <div class="about-block__container">
        <div class="about-block__header">ABOUT WORLDS OF WOW!</div>
        <div class="about-block__descr">Worlds of Wow! is a great company to partner with when you are looking to create themed environments and children’s playgrounds that engage kids and families. Churches, Pediatric Dental Offices, Family Entertainment Centers, Military Recreation Centers and YMCA's are where we do our best work.</div>
        <a href="/about" class="about-block__btn">View More</a>
    </div>
    <a href="#" data-section="block-banners" class="about-block__arrow js-hook__next-section-scroller"><img src="{{media url="wysiwyg/arrow-pointing-to-left.svg"}}"> DOWN</a>
</div>
ABOUT
                ],
                'homepage-work-and-services' => [
                    'title'     => 'Our Work & Services',
                    'stores'    => [0],
                    'is_active' => true,
                    'content'   => <<<WORKANDSERVICES
<div class="widget block block-banners banner-items-wrapper">
    <ul class="banner-items js-hook__banners-slick">
        <li class="banner-item">
            <div class="banner-item-content">
                <img class="page-header__bg" src="{{media url="wysiwyg/homepage-our-work.jpg"}}">
                <div class="banner__content banner__content--red">
                    <div class="banner__content-text">
                        <h3 class="banner__title">Our work</h3>
                        <span class="banner__text">
                            We create ridiculously cool environments for kids and families. Take a look at some of our most popular themes.
                        </span>
                        <a href="/themes" class="banner__link">See Themes</a>
                    </div>
                </div>
            </div>
        </li>
        <li class="banner-item">
            <div class="banner-item-content">
                <img class="page-header__bg" src="{{media url="wysiwyg/homepage-about-us.jpg"}}">
                <div class="banner__content banner__content--green">
                    <div class="banner__content-text">
                        <h3 class="banner__title">About us</h3>
                        <span class="banner__text">
                            With over 600 completed projects across the country, we are the experts in themed environments and play.
                        </span>
                        <a href="/about" class="banner__link">Learn more</a>
                    </div>
                </div>
            </div>
        </li>
        <li class="banner-item" aria-hidden="true">
            <div class="banner-item-content">
                <img class="page-header__bg" src="{{media url="wysiwyg/designer-at-worlds-of-wow.jpg"}}">
                <div class="banner__content banner__content--blue">
                    <div class="banner__content-text">
                        <h3 class="banner__title">Our services</h3>
                        <span class="banner__text">
                            From concept to design to installation, we are your one-stop shop for theming and indoor playgrounds.
                        </span>
                        <a href="/process" class="banner__link">See All Services</a>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>
WORKANDSERVICES
                ],
                'homepage-view'              => [
                    'title'     => 'View products',
                    'stores'    => [0],
                    'is_active' => true,
                    'content'   => <<<VIEWPRODUCTS
<div class="view-block">
    <div class="view-block-item">
        <div class="view-block-item__img">
            <img src="{{media url="wysiwyg/kids-indoor-toddler-play.jpg"}}">
        </div>
        <div class="view-block-item__content">
            <header class="view-block-item__header">VIEW BY THEME</header>
            <p class="view-block-item__p">Need some ideas? See some examples<br>of our most popular themes.</p>
            <a href="/themes" class="wow-button wow-button--yellow">View Themes</a>
        </div>
    </div>
    <div class="view-block-item">
        <div class="view-block-item__img">
            <img src="{{media url="wysiwyg/kelly-sikkema-155209-unsplash.jpg"}}">
        </div>
        <div class="view-block-item__content">
            <header class="view-block-item__header">VIEW BY CLIENTS</header>
            <p class="view-block-item__p">We create original designs for every client.<br>Take a look at each unique project.</p>
            <a href="/market" class="wow-button wow-button--red">View Projects</a>
        </div>
    </div>
</div>
VIEWPRODUCTS
                ],
            ]);

            $homePageContent = <<<HOME
<!-- Hero Banner Block; identifier: homepage-herobanner -->
{{widget type="Magento\Cms\Block\Widget\Block" template="widget/static_block/default.phtml" block_id="homepage-herobanner"}}

<!-- NEWEST CREATIONS widget here -->
{{widget type="Skynix\CatalogWidget\Block\Product\ProductsList" title="NEWEST CREATIONS" show_pager="0" show_link="1" products_count="4" template="Skynix_CatalogWidget::product/widget/content/grid.phtml" id_path="category/6" conditions_encoded="^[`1`:^[`type`:`Magento||CatalogWidget||Model||Rule||Condition||Combine`,`aggregator`:`all`,`value`:`1`,`new_child`:``^],`1--1`:^[`type`:`Magento||CatalogWidget||Model||Rule||Condition||Product`,`attribute`:`category_ids`,`operator`:`==`,`value`:`3`^]^]"}}

<!-- About Block; identifier: homepage-about-wow -->
{{widget type="Magento\Cms\Block\Widget\Block" template="widget/static_block/default.phtml" block_id="homepage-about-wow"}}

<!-- Work&Services Block; identifier: homepage-work-and-services -->
{{widget type="Magento\Cms\Block\Widget\Block" template="widget/static_block/default.phtml" block_id="homepage-work-and-services"}}

<!-- View Block; identifier: homepage-view -->
{{widget type="Magento\Cms\Block\Widget\Block" template="widget/static_block/default.phtml" block_id="homepage-view"}}

<!-- Latest blog posts widget here -->
{{widget type="TemplateMonster\Blog\Block\Widget\PostList" widget_status="1" title="Latest blog posts" post_amount="3" post_amount_per_row="3" is_enable_carousel="0" template="widget/post_list.phtml"}}
HOME;

            $this->page->load('home')
                ->setContent($homePageContent)
                ->setContentHeading('')
                ->setLayoutUpdateXml('');
            $this->page->save();
        }

        if (version_compare($context->getVersion(), '0.0.27') < 0) {
            $this->updateStaticBlocks([
                'footer-links' => [
                    'title' => 'Footer Links',
                    'stores' => [0],
                    'is_active' => true,
                    'content' => <<<FOOTERLINKS
        <div class="footer__links-container">
            <a href="{{config path="web/secure/base_url"}}about" class="footer__link">About us</a>
            <a href="{{config path="web/secure/base_url"}}process" class="footer__link">Our process</a>
            <a href="{{config path="web/secure/base_url"}}faqs" class="footer__link">FAQs</a>
            <a href="{{config path="web/secure/base_url"}}contact" class="footer__link">Contact us</a>
            <a href="{{config path="web/secure/base_url"}}reviews" class="footer__link">Reviews</a>
            <a href="{{config path="web/secure/base_url"}}blog" class="footer__link">Blog</a>
            <a href="{{config path="web/secure/base_url"}}request-a-quote" class="footer__link">Request a Quote</a>
            <a href="{{config path="web/secure/base_url"}}portfolio" class="footer__link">Portfolio</a>
        </div>
FOOTERLINKS
                ],
                'footer-follow-us-links' => [
                    'title' => 'Follow us',
                    'stores' => [0],
                    'is_active' => true,
                    'content' => <<<FOOTERFOLLOWUSLINKS
        <div class="footer__media-icons-container">
            <a href="https://www.facebook.com/worldsofwow/" target="_blank" class="footer__media"><span class="footer__media-facebook"></span></a>
            <a href="https://twitter.com/worldsofwow" target="_blank" class="footer__media"><span class="footer__media-twitter"></span></a>
            <a href="https://www.youtube.com/user/worldsofwow1" target="_blank" class="footer__media"><span class="footer__media-youtube"></span></a>
            <a href="https://www.instagram.com/worldsofwow" target="_blank" class="footer__media"><span class="footer__media-instagram"></span></a>
        </div>
        <div class="footer__media-icons-container">
            <a href="https://www.flickr.com/photos/worldsofwow/" target="_blank" class="footer__media"><span class="footer__media-flickr"></span></a>
            <a href="https://www.pinterest.com/worldsofwow" target="_blank" class="footer__media"><span class="footer__media-printerest"></span></a>
            <a href="https://www.linkedin.com/company/worlds-of-wow-inc./" target="_blank" class="footer__media"><span class="footer__media-linkedin"></span></a>
        </div>
FOOTERFOLLOWUSLINKS
                ]
            ]);
        }

        if (version_compare($context->getVersion(), '0.0.28') < 0) {
            $this->helper->setDefaultConfig(ProductHelper::XML_PATH_PRODUCT_URL_USE_CATEGORY, '1');

            $this->fixCategoriesUrlKey([
                'services' => [
                    'old_url_key' => 'facilities',
                    'new_url_key' => 'services',
                ]
            ]);

            $this->fixProductDescriptions($setup->getConnection());
        }

        if (version_compare($context->getVersion(), '0.0.29') < 0) {

            list($categoryRequestId, $categoryMarketId) = $this->getCategoriesIdsByUrlKeys(['request-a-quote', 'markets']);

            $homePageContent = <<<HOME
<!-- Hero Banner Block; identifier: homepage-herobanner -->
{{widget type="Magento\Cms\Block\Widget\Block" template="widget/static_block/default.phtml" block_id="homepage-herobanner"}}

<!-- NEWEST CREATIONS widget here -->
{{widget type="Skynix\CatalogWidget\Block\Product\ProductsList" title="NEWEST CREATIONS" show_pager="0" show_link="1" products_count="4" template="Skynix_CatalogWidget::product/widget/content/grid.phtml" id_path="category/{$categoryRequestId}" conditions_encoded="^[`1`:^[`type`:`Magento||CatalogWidget||Model||Rule||Condition||Combine`,`aggregator`:`all`,`value`:`1`,`new_child`:``^],`1--1`:^[`type`:`Magento||CatalogWidget||Model||Rule||Condition||Product`,`attribute`:`category_ids`,`operator`:`==`,`value`:`{$categoryMarketId}`^]^]"}}

<!-- About Block; identifier: homepage-about-wow -->
{{widget type="Magento\Cms\Block\Widget\Block" template="widget/static_block/default.phtml" block_id="homepage-about-wow"}}

<!-- Work&Services Block; identifier: homepage-work-and-services -->
{{widget type="Magento\Cms\Block\Widget\Block" template="widget/static_block/default.phtml" block_id="homepage-work-and-services"}}

<!-- View Block; identifier: homepage-view -->
{{widget type="Magento\Cms\Block\Widget\Block" template="widget/static_block/default.phtml" block_id="homepage-view"}}

<!-- Latest blog posts widget here -->
{{widget type="TemplateMonster\Blog\Block\Widget\PostList" widget_status="1" title="Latest blog posts" post_amount="3" post_amount_per_row="3" is_enable_carousel="0" template="widget/post_list.phtml"}}
HOME;

            $this->page->load('home')
                ->setContent($homePageContent)
                ->setContentHeading('')
                ->setLayoutUpdateXml('');
            $this->page->save();
        }

        if (version_compare($context->getVersion(), '0.0.30') < 0) {
            $this->updateCategories([
                'markets' => [
                    'url_key' => 'markets',
                    'custom_layout_update' => <<<LAYOUTUPDATE
<referenceContainer name="content">
    <block class="Magento\Framework\View\Element\Template" name="map" template="Skynix_Backend::html/map.phtml" after="-" />
</referenceContainer>
LAYOUTUPDATE
                ]
            ]);
        }

        if (version_compare($context->getVersion(), '0.0.31') < 0) {
            $this->resolveAreaCode();

            /**
             * @var $categoriesCollectorion CategoryCollection
             */
            $categoriesCollectotion = $this->categoryCollectionFactory
                ->create()
                ->addAttributeToFilter('url_key', ['markets', 'services', 'themes'])
                ->load();

            $categoriesData = [];
            foreach ($categoriesCollectotion as $category) {
                $categoriesData[$category->getUrlKey()] = [
                    'url' => $category->getUrl(),
                ];
            }

            $staticBlockData['themes'] = [
                'title' => 'Themes',
                'stores' => [0],
                'is_active' => true,
                'content' => <<<THEMES
<img src="{{media url="wysiwyg/IMG_3495.jpg"}}" alt="Themes" />
<a class="category-link" href="{$categoriesData['themes']['url']}"><span>Learn More &gt;</span></a>
THEMES
            ];

            $block = $this->blockFactory->create()->load('facility', 'identifier');
            if ($block->getId()) {
                $block->setIdentifier('services')
                    ->addData([
                        'title' => 'Services',
                        'stores' => [0],
                        'is_active' => true,
                        'content' => <<<SERVICES
<img src="{{media url="wysiwyg/services.jpg"}}" alt="Services" />
<a class="category-link" href="{$categoriesData['services']['url']}"><span>Learn More &gt;</span></a>
SERVICES
                    ])->save();
            } else {
                $staticBlockData['services'] = [
                    'title' => 'Services',
                    'stores' => [0],
                    'is_active' => true,
                    'content' => <<<SERVICES
<img src="{{media url="wysiwyg/services.jpg"}}" alt="Services" />
<a class="category-link" href="{$categoriesData['services']['url']}"><span>Learn More &gt;</span></a>
SERVICES
                ];
            }

            $block = $this->blockFactory->create()->load('market', 'identifier');
            if ($block->getId()) {
                $block->setIdentifier('markets')
                    ->addData([
                        'title' => 'Markets',
                        'stores' => [0],
                        'is_active' => true,
                        'content' => <<<MARKET
<img src="{{media url="wysiwyg/37638104101_5c55c3cdd7_z.jpg"}}" alt="Markets" />
<a class="category-link" href="{$categoriesData['markets']['url']}"><span>Learn More &gt;</span></a>
MARKET
                    ])->save();
            } else {
                $staticBlockData['markets'] = [
                    'title' => 'Markets',
                    'stores' => [0],
                    'is_active' => true,
                    'content' => <<<MARKET
<img src="{{media url="wysiwyg/37638104101_5c55c3cdd7_z.jpg"}}" alt="Markets" />
<a class="category-link" href="{$categoriesData['markets']['url']}"><span>Learn More &gt;</span></a>
MARKET
                ];
            }

            $this->updateStaticBlocks($staticBlockData);
            $this->updateMegamenu([
                'markets'  => [
                    'url_key' => 'markets',
                    'mm_turn_on'  => true,
                    'configurator' => [
                        [
                            'width' => 4,
                            'css_class' => 'megamenu-submenu-column',
                        ],
                        [
                            'width' => 4,
                            'css_class' => 'megamenu-submenu-column',
                        ],
                        [
                            'width' => 4,
                            'css_class' => 'megamenu-submenu-image',
                        ],
                    ]
                ],
                'services' => [
                    'url_key'      => 'services',
                    'mm_turn_on'   => true,
                    'configurator' => [
                        [
                            'width'     => 6,
                            'css_class' => 'megamenu-submenu-column',
                        ],
                        [
                            'width'     => 6,
                            'css_class' => 'megamenu-submenu-image',
                        ],
                    ]
                ],
            ]);
        }

        if (version_compare($context->getVersion(), '0.0.32') < 0) {
            $this->updateMegamenu([
                'markets'  => [
                    'url_key'      => 'markets',
                    'mm_turn_on'   => true,
                    'mm_css_class' => 'menu-colored-item menu-colored-item--red megamenu-submenu',
                    'configurator' => [
                        [
                            'width' => 4,
                            'css_class' => 'megamenu-submenu-column',
                        ],
                        [
                            'width' => 4,
                            'css_class' => 'megamenu-submenu-column',
                        ],
                        [
                            'width' => 4,
                            'css_class' => 'megamenu-submenu-image',
                        ],
                    ]
                ],
                'services' => [
                    'url_key'      => 'services',
                    'mm_turn_on'   => true,
                    'mm_css_class' => 'menu-colored-item menu-colored-item--blue megamenu-submenu',
                    'configurator' => [
                        [
                            'width'     => 6,
                            'css_class' => 'megamenu-submenu-column',
                        ],
                        [
                            'width'     => 6,
                            'css_class' => 'megamenu-submenu-image',
                        ],
                    ]
                ],
                'themes'          => [
                    'url_key'      => 'themes',
                    'mm_turn_on'   => true,
                    'mm_css_class' => 'menu-colored-item menu-colored-item--green megamenu-submenu',
                    'configurator' => [
                        [
                            'width'     => 6,
                            'css_class' => 'megamenu-submenu-column',
                        ],
                        [
                            'width'     => 6,
                            'css_class' => 'megamenu-submenu-image',
                        ],
                    ]
                ]
            ]);
        }

        if (version_compare($context->getVersion(), '0.0.33') < 0) {
            $this->helper->setDefaultConfig(ProductHelper::XML_PATH_USE_PRODUCT_CANONICAL_TAG, '1');
            $this->helper->setDefaultConfig(CategoryHelper::XML_PATH_USE_CATEGORY_CANONICAL_TAG, '1');
        }

        if (version_compare($context->getVersion(), '0.0.34') < 0) {
            $robots = <<<ROBOTS
User-agent: *
Disallow: /index.php/
Disallow: /*?
Disallow: /checkout/
Disallow: /app/
Disallow: /lib/
Disallow: /*.php$
Disallow: /pkginfo/
Disallow: /report/
Disallow: /var/
Disallow: /catalog/
Disallow: /customer/
Disallow: /sendfriend/
Disallow: /review/
Disallow: /*SID=
ROBOTS;

            $this->helper->setDefaultConfig('design/search_engine_robots/custom_instructions', $robots);
        }

        if (version_compare($context->getVersion(), '0.0.35') < 0) {
            $this->updateStaticBlocks([
                'homepage-herobanner'        => [
                    'title'     => 'Hero Banner',
                    'stores'    => [Store::DEFAULT_STORE_ID],
                    'is_active' => true,
                    'content'   => <<<HEROBANNER
<div class="hero-banner">
    <img src="{{media url="wysiwyg/hero.png"}}" alt="herobanner">
    <div class="hero-banner__content">
        <div class="hero-banner__content-text">
                <span class="hero-banner__text">
                    Creating Cool, Fun Environments for Kids and Families
                </span>
            <a href="{{config path="web/secure/base_url"}}request-a-quote" class="hero-banner__link">Get started now!</a>
        </div>
    </div>
</div>
HEROBANNER
                ],
            ]);
        }

        if (version_compare($context->getVersion(), '0.0.36') < 0) {
            $this->helper->setDefaultConfig(StoreManager::XML_PATH_SINGLE_STORE_MODE_ENABLED, '1');
        }

        if (version_compare($context->getVersion(), '0.0.37') < 0) {
            $this->updateStaticBlocks([
                'homepage-herobanner'        => [
                    'title'     => 'Hero Banner',
                    'stores'    => [Store::DEFAULT_STORE_ID],
                    'is_active' => true,
                    'content'   => <<<HEROBANNER
<div class="hero-banner">
    <img src="{{media url="wysiwyg/hero.png"}}" alt="herobanner">
    <div class="hero-banner__content">
        <div class="hero-banner__content-text">
                <h1 class="hero-banner__text">
                    Creating Cool, Fun Environments for Kids and Families
                </h1>
            <a href="{{config path="web/secure/base_url"}}request-a-quote" class="hero-banner__link">Get started now!</a>
        </div>
    </div>
</div>
HEROBANNER
                ],
            ]);
        }

        $setup->endSetup();
    }

    /**
     * Setup & fix category data
     *
     * @param $categoriesData
     */
    protected function fixCategories($categoriesData)
    {
        $this->resolveAreaCode();
        $this->initStore();

        if (count($categoriesData) > 0) {
            foreach ($categoriesData as $categoryInfo) {
                $category = $this->categoryFactory->create();
                $category->loadByAttribute('url_key', $categoryInfo['url_key']);
                if (!$category) {
                    $parentCategoryId = $parentCategoryId ?? $this->store->getStoreRootCategoryId();
                    echo "Creating category: {$categoryInfo['url_key']}\n";
                    /** @var Category $parentCategory */
                    $parentCategory = $this->categoryFactory->create();
                    $parentCategory = $parentCategory->load($parentCategoryId);

                    /** @var Category $category */
                    $category = $this->categoryFactory->create();
                    $category->setStoreId($this->store->getId());
                    $category->addData($categoryInfo['data']);
                    $category->setPath($parentCategory->getPath());
                    $category->setParentId($parentCategory->getId());
                    $category->setAttributeSetId($category->getDefaultAttributeSetId());
                    $category->save();
                } else {
                    echo "Category exists: {$categoryInfo['url_key']}\n";
                    echo "Store Id=" . $this->store->getId();
                    $category->setStoreId($this->store->getId());

                    foreach ($categoryInfo['data'] as $key => $value) {
                        $category->setData($key, $value);
                    }

                    $category->save();
                }
            }
        }
    }

    /**
     * Set store to product in $products
     *
     * @param $products
     */
    protected function fixProductWebsites($products)
    {
        echo "\nFixing products...";
        $this->resolveAreaCode();
        foreach ($products as $product) {
            /**
             * @var $product Product
             */
            $product->setWebsiteIds([1])->setVisibility(4)->save();
        }
    }

    /**
     * Fixes product descriptions:
     * moves all descriptions
     *      from store view with id 1 (Default Store View)
     *      to admin store view id = 0 (for all Store Views)
     *
     * @param AdapterInterface $connection
     */
    protected function fixProductDescriptions($connection) {

        echo "\nStart fixProductDescriptions;\n";
        $query = $connection
            ->select()
            ->from('eav_attribute', ['attribute_id'])
            ->joinLeft('eav_entity_type', 'eav_entity_type.entity_type_id = eav_attribute.entity_type_id', [])
            ->where('eav_entity_type.entity_type_code = "catalog_product"')
            ->where('eav_attribute.attribute_code = "description"');
        $attrId = $connection->fetchOne($query);
        echo "\n\tattribute_id for description is" . $attrId;

        $query = $connection
            ->select()
            ->from('catalog_product_entity_text', ['value_id', 'entity_id'])
            ->where('catalog_product_entity_text.store_id = 1')
            ->where('catalog_product_entity_text.attribute_id = ?', $attrId);
        $data = $connection->fetchAll($query);

        foreach ($data as $item) {
            echo "\n\titem:\n";
            echo print_r($item, true);
            $query = $connection
                ->select()
                ->from('catalog_product_entity_text', ['entity_id'])
                ->where('entity_id = ?', $item['entity_id'])
                ->where('store_id = 0')
                ->where('attribute_id = ?', $attrId);
            $dataAttr = $connection->fetchAll($query);

            if(!empty($dataAttr)) {
                echo "\tdataAttr is not empty!";
                $deleted = $connection->delete('catalog_product_entity_text', ['entity_id = ?' => $item['entity_id'], 'attribute_id = ?' => $attrId, 'store_id = ?' => 0]);
                echo "\tDeleted $deleted row(s)";
            }
            $updated = $connection->update('catalog_product_entity_text', ['store_id' => 0], ['entity_id = ?' => $item['entity_id'], 'attribute_id = ?' => $attrId, 'store_id = ?' => 1]);
            echo "\tUpdated $updated row(s)";
        }
        echo "\nend fixProductDescriptions\n";
    }

    /**
     * Retrieve product collection
     *
     * @param null|int $categoryId
     * @return Collection
     */
    private function getProductCollection($categoryId = null)
    {
        /**
         * @var $productCollection Collection
         */
        $productCollection = null;

        if ($categoryId != null) {
            $productCollection = $this->categoryFactory
                ->create()
                ->load($categoryId)
                ->getProductCollection();
        } else {
            $productCollection = $this->productCollectionFactory->create();
        }

        return $productCollection->addAttributeToSelect('*');
    }

    /**
     * Updates or creates CMS Pages with given data
     *
     * @param $pagesData
     */
    protected function updateCmsPages($pagesData)
    {
        foreach ($pagesData as $identifier => $pageData) {
            /** @var $page Page */
            $page = $this->pageFactory->create();
            $page->load($identifier);
            if (!$page->getId()) {
                echo "\n Creating page: " . $identifier;
                $page->setIdentifier($identifier);
            }
            echo "\nAdd Data for: " . $identifier . " with url key: " . $pageData['url_key'] . ". ";
            $page->addData($pageData);
            $page->save();
        }
    }

    /**
     * Updates categories with new data
     *
     * @param $categoriesData
     */
    protected function updateCategories($categoriesData)
    {
        echo "\nUpdating categories...";
        $this->initStore();

        if (count($categoriesData) > 0) {
            foreach ($categoriesData as $categoryInfo) {
                /**
                 * @var $category Category
                 */
                $category = $this->categoryCollectionFactory
                    ->create()
                    ->addAttributeToFilter('url_key', $categoryInfo['url_key'])
                    ->setPage(1, 1)
                    ->load()
                    ->getFirstItem();

                if (!$category->getId()) {
                    $parentCategoryId = $parentCategoryId ?? $this->store->getStoreRootCategoryId();
                    echo "\n\t\tNew category with url key: {$categoryInfo['url_key']}";

                    /**
                     * @var Category $parentCategory
                     */
                    $parentCategory = $this->categoryFactory->create();
                    $parentCategory = $parentCategory->load($parentCategoryId);

                    /**
                     * @var Category $category
                     */
                    $category = $this->categoryFactory->create();
                    $category->setPath($parentCategory->getPath());
                    $category->setParentId($parentCategory->getId());
                    $category->setAttributeSetId($category->getDefaultAttributeSetId());
                } else {
                    echo "\n\tUpdating category " . $category->getId() . " (from " . $categoryInfo['url_key'] . ")";
                }

                $category
                    ->setStoreId($this->store->getId())
                    ->addData($categoryInfo);
                echo "\n\t\tSaving...";
                $category->save();
            }
        }
    }

    /**
     * Sets admin area
     * If it is already set and you set it again - it throws an exception,
     * and if it is not set and you try to get it - it throws an exception!
     */
    protected function resolveAreaCode()
    {
        try {
            $this->state->getAreaCode();
        } catch (\Exception $e) {
            $this->state->setAreaCode('adminhtml');
        }
    }

    /**
     * Gets store and storeManager
     */
    protected function initStore()
    {
        $this->store = $this->storeManager->getStore(Store::ADMIN_CODE);
        $this->storeManager->setCurrentStore($this->store->getCode());
    }

    /**
     * Updates megamenu
     *
     * @param $megaMenuCategorySetup
     */
    protected function updateMegamenu($megaMenuCategorySetup)
    {
        /**
         * Setup mm_configuration data for TemplateMonster_Megamenu category configuration
         *
         * Products
         *      \"$id\":\"$position\",\"$id\":\"$position\",\"$id\":\"$position\"
         * example:
         *      \"7\",\"8\",\"9\",\"10\",\"11\",\"12\",\"13\",\"14\"
         *
         * Text
         * example:
         *      Products - 7,8,9,10,11,12,13,14
         *
         * mm_configuration examples:
         *      [[{"width":"6","css_class":"","entities":[{"value":"products:{\"7\",\"8\",\"9\",\"10\",\"11\",\"12\",\"13\",\"14\"}","text":"Products - Products - 7,8,9,10,11,12,13,14"}]}]]
         * [[
         *  {"width":"4","css_class":"","entities":[{"value":"products:{\"1\":\"\",\"2\":\"\",\"3\":\"\"}","text":"Products - 1,2,3"}]},
         *  {"width":"4","css_class":"","entities":[{"value":"products:{\"4\":\"\",\"5\":\"\",\"6\":\"\"}","text":"Products - 4,5,6"}]},
         *  {"width":"4","css_class":"","entities":[]}
         * ]]
         *
         * [[
         *  {"width":"4","css_class":"","entities":[{"value":"products:{\"1\":\"\",\"2\":\"\",\"3\":\"\"}","text":"Products - 1,2,3"}]},
         *  {"width":"4","css_class":"","entities":[{"value":"products:{\"4\":\"\",\"5\":\"\",\"6\":\"\"}","text":"Products - 4,5,6"}]},
         *  {"width":"4","css_class":"","entities":[{"value":"block:1","text":"Market"}]}
         * ]]
         *
         * [[{"width":"4","css_class":"megamenu-submenu-column","entities":[{"value":"products:{\"1\":\"\",\"2\":\"\",\"3\":\"\"}","text":"Products - 1,2,3"}]},{"width":"4","css_class":"megamenu-submenu-column","entities":[{"value":"products:{\"4\":\"\",\"5\":\"\",\"6\":\"\"}","text":"Products - 4,5,6"}]},{"width":"4","css_class":"megamenu-submenu-image","entities":[]}]]
         *
         */
        echo "\nUpdating megamenu... ";
        $this->initStore();

        if ($megaMenuCategorySetup) {
            foreach ($megaMenuCategorySetup as $categoryInfo) {
                /**
                 * @var $category Category
                 */
                $category = $this->categoryFactory
                    ->create()
                    ->loadByAttribute('url_key', $categoryInfo['url_key']);

                if (isset($categoryInfo['mm_css_class'])) {
                    $category->setData('mm_css_class', $categoryInfo['mm_css_class']);
                }

                $productCollection = $category->getProductCollection();
                echo "\n\tCheck category " . $category->getName() . " (id " . $category->getId() . "; children: " . $category->getChildrenCount() . " / " . count($productCollection) . ")";
               
                if (count($productCollection) > 0) {
                    echo "\n\t\tThis category has " .count($productCollection) . " child(ren).";
                    $productIds = [];

                    foreach ($productCollection as $product) {
                        $productIds[] = $product->getId();
                        echo "\n\t\t\tProduct: " . $product->getId();
                    }

                    $productInColumns = array_chunk($productIds, ceil(count($productIds) / (count($categoryInfo['configurator']) - 1)));
                    $productColumnJson = [];

                    foreach ($productInColumns as $productColumn) {
                        $productColumnJson[] = $this->helper->getConfiguratorJson($productColumn);
                    }

                    $configuratorString = '[[';
                    $blockWidth = 12;
                    for ($i = 0; $i < count($categoryInfo['configurator']) - 1; $i++) {
                        $configuratorString .= '{"width":"' . $categoryInfo['configurator'][$i]['width'] . '","css_class":"' . $categoryInfo['configurator'][$i]['css_class'] . '","entities":[{"value":"products:{' . $productColumnJson[$i][0] . '}","text":"Products - ' . $productColumnJson[$i][1] . '"}]},';
                        $blockWidth -= (int)$categoryInfo['configurator'][$i]['width'];
                    }
                    $block = $this->blockFactory->create()->load($category->getUrlKey(), 'identifier');
                    $configuratorString .= '{"width":"' . $blockWidth . '","css_class":"megamenu-submenu-image","entities":[{"value":"block:' . $block->getId() . '","text":"' . $category->getName() . '"}]}';
                    $configuratorString .= ']]';
                    echo "\n\t\tConfigurator string:\n" . $configuratorString . "\n";
                    $category
                        ->setStoreId($this->store->getId())
                        ->setData('mm_configurator', $configuratorString)
                        ->setData('mm_turn_on', 1);
                    $category->save();
                }
            }
        }
    }

    /**
     * Updates or creates static CMS Blocks from array data
     *
     * @param array $staticBlocksData
     */
    protected function updateStaticBlocks($staticBlocksData)
    {
        if ($staticBlocksData) {
            foreach ($staticBlocksData as $identifier => $staticBlockData) {
                /**
                 * @var $block Block
                 */
                $block = $this->blockFactory->create();
                $block->load($identifier, 'identifier');
                $block->setIdentifier($identifier);
                $block->addData($staticBlockData);
                $block->save();
            }
        }
    }

    /**
     * Creates category and assign products to it
     *
     * @param $categoryData
     * @param int $parentCategoryId
     */
    protected function createCategory($categoryData, $parentCategoryId = 2)
    {
        $this->resolveAreaCode();
        $this->initStore();

        if ($categoryData) {
            /**
             * @var $category Category
             */
            $category = $this->categoryFactory->create();
            $category = $category->loadByAttribute('url_key', $categoryData['url_key']);
            if (!$category) {
                $parentCategoryId = $parentCategoryId ?? $this->store->getStoreRootCategoryId();
                echo "\nCreating category: {$categoryData['url_key']}";
                /** @var Category $parentCategory */
                $parentCategory = $this->categoryFactory->create();
                $parentCategory = $parentCategory->load($parentCategoryId);

                /** @var Category $category */
                $category = $this->categoryFactory->create();
                $category->setStoreId($this->store->getId());
                $category->addData($categoryData);
                $category->setPath($parentCategory->getPath());
                $category->setParentId($parentCategory->getId());
                $category->setAttributeSetId($category->getDefaultAttributeSetId());
                $category->save();
            }
        }
    }

    protected function fixProductCategories($productCategoriesData = [])
    {
        if ($productCategoriesData) {
            foreach ($productCategoriesData as $sku => $categories) {
                $this->categoryLinkManagement->assignProductToCategories(
                    $sku,
                    $this->getCategoriesIdsByUrlKeys($categories)
                );
            }
        }
    }

    /**
     * Retrieve array of category ids by its url_key
     *
     * @param $categories
     * @return array
     */
    protected function getCategoriesIdsByUrlKeys($categories)
    {
        $ids = [];

        if ($categories) {
            foreach ($categories as $urlKey) {
                /**
                 * @var $category Category
                 */
                $category = $this->categoryCollectionFactory
                    ->create()
                    ->addAttributeToFilter('url_key', $urlKey)
                    ->setPage(
                        1,
                        1
                    )->load()
                    ->getFirstItem();
                $ids[] = $category->getId();
            }
        }

        return $ids;
    }

    /**
     * Removed '.html' from UrlRewrites
     */
    protected function fixUrlRewrites() {
        echo "\nFixing URL_Rewrites...";

        /**
         * @var $urlRewritesCollection UrlRewriteCollection
         */
        $urlRewritesCollection = $this->urlRewritesCollectionFactory->create();
        foreach ($urlRewritesCollection as $urlRewrite) {
            /**
             * @var $urlRewrite UrlRewrite
             */
            $oldRequestPath = $urlRewrite->getData('request_path');
            echo "\n\tURL Rewrite( id:" . $urlRewrite->getId() . "; request_path:" . $oldRequestPath . "; target_path:" . $urlRewrite->getTargetPath() . ")";
            if (strpos($oldRequestPath, '.html')) {
                $fixedRequestPath = str_replace('.html', '', $oldRequestPath);
                echo "\n\t\tFixing: " . $oldRequestPath . " -> " . $fixedRequestPath;
                $urlRewrite->setRequestPath($fixedRequestPath);
                echo "\tSaving...";
                $urlRewrite->save();
            }
        }
    }

    protected function fixCategoriesUrlKey($categoriesData)
    {
        echo "\nFixing category url-keys...\n";
        $this->resolveAreaCode();
        $this->initStore();

        if (count($categoriesData) > 0) {
            foreach ($categoriesData as $categoryInfo) {
                /**
                 * @var $category Category
                 */
                echo "\n\tLoading category...";
                $category = $this->categoryCollectionFactory
                    ->create()
                    ->addAttributeToFilter('url_key', ['eq' => $categoryInfo['old_url_key']])
                    ->load()
                    ->getFirstItem();

                //$category->loadByAttribute('url_key', $categoryInfo['old_url_key']);
                echo "\n\tCategory data: ";
                echo print_r($category->getData());

                if ($category && $category->getId()) {
                    echo " successfuly loaded Category with id = " . $category->getId();
                    $category->setStoreId(Store::DEFAULT_STORE_ID);
                    echo "\n\tSet Store Id = " . Store::DEFAULT_STORE_ID;
                    $category->setUrlKey($categoryInfo['new_url_key']);
                    echo "\n\tSet New Url Key = " . $categoryInfo['new_url_key'];
                    $category->save();
                    echo "\n\tSaved!\n";
                    echo "\n\t\tCategory new Url-key is " . $category->getUrlKey() . " with id = " . $category->getId();
                } else {
                    echo "Unable to load category with such url_key: " . $categoryInfo['old_url_key'] . " (id is " . $category->getId() . ")";
                }
            }
        }

        echo "\nFixed!\n";
    }
}