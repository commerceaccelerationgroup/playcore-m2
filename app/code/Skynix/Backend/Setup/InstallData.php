<?php
/**
 * Created by Skynix Team.
 * User: oleksii bulba
 * Date: 20.06.18
 * Time: 14:14
 */

namespace Skynix\Backend\Setup;

use Magento\Catalog\Setup\CategorySetup;
use Magento\Cms\Model\Block;
use Magento\Cms\Model\BlockFactory;
use Magento\Framework\App\State;
use Magento\Framework\Module\StatusFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Api\CategoryLinkManagementInterface;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManager;
use Skynix\Backend\Helper\Data;
use TemplateMonster\Blog\Helper\Data as TemplateMonsterBlogData;


class InstallData implements InstallDataInterface
{
    /**
     * @var StoreManager
     */
    protected $_storeManager;

    /**
     * @var CategoryFactory
     */
    protected $categoryFactory;

    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * @var ProductCollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * @var BlockFactory
     */
    protected $blockFactory;

    /**
     * @var State
     */
    protected $state;

    /**
     * @var CategorySetup
     */
    protected $categorySetup;

    /**
     * @var CategoryLinkManagementInterface
     */
    protected $categoryLinkManagement;

    /**
     * @var Store
     */
    protected $store;

    /**
     * @var \Magento\Setup\Model\ModuleStatusFactory
     */
    protected $moduleStatusFactory;

    /**
     * @var Data
     */
    protected $helper;

    public function __construct(
        StoreManager $storeManager,
        CategoryFactory $categoryFactory,
        ProductFactory $productFactory,
        ProductCollectionFactory $productCollectionFactory,
        BlockFactory $blockFactory,
        CategoryLinkManagementInterface $categoryLinkManagement,
        StatusFactory $moduleStatusFactory,
        State $state,
        Data $helper
    )
    {
        $this->_storeManager = $storeManager;
        $this->categoryFactory = $categoryFactory;
        $this->productFactory = $productFactory;
        $this->blockFactory = $blockFactory;
        $this->categoryLinkManagement = $categoryLinkManagement;
        $this->moduleStatusFactory = $moduleStatusFactory;
        $this->state = $state;
        $this->helper = $helper;
        $this->productCollectionFactory = $productCollectionFactory;
    }

    /**
     * Installs data for a module
     *
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     * @return void
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $marketDescription                        = <<<DESC
<p>At Worlds of Wow, we like to transform the spaces and places of the world so they’re a little more accommodating for those who are still growing up. We’ve transformed a variety of spaces to include creative children’s areas for extraordinary fun. We work with Churches, Dental Offices, YMCA’s, Military Bases, Family Entertainment Centers, and others. When we help you and your company create a custom themed environment, the world is simply your oyster! We’ve got tons of ideas that we’re eager to get started on, but we’ll slow it down and give you a glimpse into what we have to offer. The work we do is rooted in fun and the stories are embedded within each design.</p>
<p>There’s nothing we love more than creating ridiculously fun, cool, and awesome environments for kids and their parents. When you transform simple spaces into something that is truly extraordinary, something special happens. We’ve seen it time and time again. Inevitably, these children’s areas, lobbies, and welcoming spaces will become a part of your nature and your community’s DNA.</p>
DESC;
        $marketMetaDescription                    = <<<META
Welcome to the Worlds of Wow and the markets of play we serve. From indoor playgrounds to airbrush designs and wall murals, we create amazing themed environments for children.
META;
        $churchDescription                        = <<<DESC
<p>Churches across the country have proven that one of the best ways to minister to families and adults is through children and play. For both newly constructed and remodeled churches, we have seen space dedicated to children's areas increase dramatically over the past decade. These churches are building children's ministries that create a buzz and cause a "Wow!"</p>
<img />
<p>Simply said, a visible investment in your Children’s Ministry sends a tangible message to your community that you care and are passionate about kids and families. Children’s spaces can be used for community outreach in several ways, which could potentially drive traffic to the church even on weekdays. Having a children’s play space and themed environment opens the doors for opportunities such as Mother’s Day Out, bible studies, daycare and other activities. Children will find both adventure and comfort when they walk into an environment full of theme and play. When a children’s space is full of life, both children and parents are happy.</p>
<h2>Why Choose Worlds of Wow</h2>
<p>The two main reasons to choose WOW are experience and value. The most important elements of your project are getting the right design and proper installation. Nobody else offers the wealth of experience and creativity that we provide for both of these crucial elements.</p>
<p>We will be here to guide you through the entire process of making your Children’s Ministry shine. We understand ministry as the majority of our team are current and/or former pastors. Every project starts with a personal relationship, and we will work with you throughout the project to answer any questions you have. We do both theming and play, so we are able to design the entire environment to ensure your visions are made a reality.</p>
<h2>Playgrounds for Your Church</h2>  
<p>Consider adding a <a href="#">playground</a> to your Children’s Ministry. Indoor playgrounds can provide a safe place for children in your community to play. You are serving your community by providing a place for families to come together. You will reach families by focusing on the children. The families will feel valued, which helps the ministry grow. Creating environments that are built for and themed to fit kids at each age group is a great way to make your community feel welcome. You can serve your community by providing soft, sculpted foam in a fun, safely enclosed space with colorful rubberized flooring for toddlers and soft contained climbing play places for grade-schoolers.</p>
<p>Whether it’s church nursery, Children’s Ministry, or youth, we are  the solution!<br>Often, themed entrances, hallways, large group rooms and even classrooms share a common theme that simply “ages” with the kids, while continuing the theme with unique areas for each age group. We can add a mix of theme and play to create a brilliant design that will bring smiles to children and parents alike.</p>
<h2>From the Churches</h2>
<p>“We experienced a 35 percent increase in overall attendance -- nursery, preschool and elementary age. The increase is attributed to the new facility environment which includes new spaces, great decor, a state-of-the-art security/check-in system and a super location."</p>
<p>-Ruth Charlson, Second Baptist Baytown</p>
<p>"We have seen about a 10 percent increase in young families under the age of 40 which is reflected similarly in our preschool/children areas. It is not an uncommon sight to see families taking pictures of their children by the scenes. This has helped change the attitude of workers and parents alike in a positive way."</p>
<p>-Mark Harrison, Spring Baptist Church</p>
<p>"TK’s Place is a unique facility that will simply make your jaw drop and say WOW. Worlds of Wow created this amazing environment that will help our Children’s Ministry attract families throughout the lake area. There is nothing like this in Southwest Louisiana."</p>
<p>-Dawn Brand, preschool minister at Trinity Baptist Church</p>
DESC;
        $churchMetaDescription                    = <<<META
For the ultimate kid's play room or church playground call Wow. We provide fun environments for children that assist with the imagery and teaching of youth ministries.
META;
        $dentalMedicalOfficeDesignDescription     = <<<DESC
<h2>Wow! Dental & Medical Office Design</h2>
<p>Wow creates customized, unique environments for Dental and Medical offices. We're specific to your theme and design. A pediatric dentist office should be a fun, memorable place where kids love and want to go - so we design and create ridiculously cool, fun environments that impact kids and families of all ages. Be unique and stylize your waiting room and office today! Themed pediatric dental offices are some of the most exciting projects we design and build here at Wow! Adding Wow to your kid friendly environment is more than adding a few colors and shapes - it's about creating depth, dimension style, and most importantly... telling a story!</p>
<img >
<p>Aside from our years of experience with theme and play, we are the only company that provides theming and play together under one roof. We are your one-stop-shop!</p>
<p>Why does that matter? Two really big reasons – budget and continuity. One company, one design team working together to maximize your dollars to achieve maximum impact with your theming and indoor play. Having one team design, fabricate & install your theme is a huge benefit due to the big cost savings from working with one company.</p>
<p>Expertise, Relationship, Focus, Technology Professionalism<br>These aren’t just words. They are a promise we strive for in each phase for every project, with every client Wow partners with.</p>
DESC;
        $dentalMedicalOfficeDesignMetaDescription = <<<META
Parents and children feel relaxed when your pediatric dental office and waiting room is designed for kids. Wow can transform your dental or medical office to a fun, engaging play space.
META;
        $familyCentersDescription                 = <<<DESC
<h2>orlds of Wow Custom Family Entertainment Centers</h2>
<p>Wow! play structures and themes are simply stated, super cool! Lots of places for children to hide, play, climb, run, jump, and most importantly, act like a kid! Wow! designs and builds every play structure to exclusively fit your space, and we can even theme the structure to give it that ultra cool look. Need a jungle, tree house, or maybe a launch pad for a giant space rocket? We can do that! We can even design and build a soft play space for your smallest guests.</p>
<p>Besides the fact that we've got decades of play experience, we are one of few firms that can offer theming and play designs at one shop.</p>
<p>Why is this so important? Two great reasons come to mind – your budget and keeping things consistent. A single ally working with you to ensure your budget is used correctly to achieve desired results that produce.</p>
DESC;
        $familyCentersMetaDescription             = <<<META
Our theme designs for family entertainment centers and fun zones are complex fun. We can bring your vision to life, whether it's animal-themed, sports-themed, or something more.
META;
        $MilitaryRecreationCentersDescription     = <<<DESC
<h2>Add Wow! to your Military Recreation Center</h2>
<p>Need some room design ideas? A realistically scaled helicopter with interactive play elements, a helipad printed carpet, passageways, stairs and slides. An indoor playground disguised as a sunken pirate ship with hidden rooms, tunnels and aquatic sea life floating about the walls and ceiling. An adventure awaits! The theme comes to life in your children's play area, bringing the outdoors…indoors! Add Wow! to attract new family units, emphasize the family focus, promote healthy lifestyles, enhance social skills, and build early childhood development for your base! We are proud to work with all of our armed forces.</p>
<p>Aside from having lots of theme and play experience, we are one of few organizations that can provide theming and playgrounds at one location. Our design team is your main resource!</p>
<p>Is that very important? There are two reasons we say YES – budget and continuity. Working with just one team for design and installation helps you maximize your dollars to get a great return on your theming and indoor play investment.</p>
DESC;
        $MilitaryRecreationCentersMetaDescription = <<<META
Worlds of Wow specializes in new and renovated recreation centers for our armed forces. Call us today. Let's work together to design a military recreation center for your members.
META;
        $ymcasDescription                         = <<<DESC
<h2>Worlds of Wow! at The Y</h2>
<p>The YMCA is a remarkable and timeless institution woven into the DNA of our communities, generations past, present and future yet modern day challenges include competing for family memberships are real and not going away anytime soon! Worlds of Wow provides unique designs, interactive play promotion, original theming, truly soft play environments, collaborative themes, and so much more! Insert the Wow! factor to add value to existing memberships, attract new family units, emphasize the family focus, promote healthy lifestyles, enhance social skills, and build early childhood development for your facility!</p>
<p>Aside from the level of experience we've garnered over the years, we are one of few companies that provides theming and play under one roof.</p>
<p>Why is this a big deal? Two very good reasons – your budget and continuity. A single company, a single design team working with you to maximize your money to achieve the best results with your play theme.</p>
DESC;
        $ymcasMetaDescription                     = <<<META
Worlds of Wow! designs YMCA youth center spaces. From play room designs to sports themes for conference rooms or children's areas we can bring your concept to life.
META;
        $facilityMetaDescription                  = <<<META
Worlds of Wow offers custom design services including Pediatric Office Themes, Indoor Playgrounds, 3D Art, Animal Sculptures, Wall Murals, Airbrush Designs, and more for kids.
META;
        $designAndConceptDescription              = <<<DESC
<p>You have a dream. A vision you want to share with others. Whether it’s to capture the imagination of children in a themed design or create a fun atmosphere for families to gather in, we work with you to bring those dreams to life visually in unique concepts for you.</p>
<p>This is where the look and feel of your project is established, and it’s where you are the most involved. Our job is to take the abstract ideas and dreams you have about your space or facility and makes them tangible in the form of visual representations of the areas you would like themed. It is the foundation that carries the project forward to completion. We can build a themed environment based off a napkin sketch or a detailed brief. The goal is to take every bit of information and create something that, when you see it, you say, "That's it!"</p>
DESC;
        $designAndConceptMetaDescription          = <<<META
Our artistic process begins with sketches or digital imagery of your design concept. Once we understand your desire, we then begin to create lifelike elements that begin to take shape.
META;
        $themingDescription                       = <<<DESC
<p>We tell a new story with each design.We can assist with stage design, theming for various ages and stages, and any other design idea you may have.</p>
<p>With over 12 years and over 400 completed projects, we are the experts when it comes to church children’s ministry theming, pediatric dental theming, amusement and retail theming. We know what it takes from the very first conversation as we dream together to the very last nail put into place.</p>
<p>We love the creative process of working with you through the development of concepts, characters, elements and stories.</p>
<h2>It’s just our role to bring this vision to life.</h2>
<p>Worlds of Wow designs and themes are always original, never borrowed from others. From check in, to hallways, to large group rooms, our themes are designed to tell a story and create destinations where your environments become the place kids want to be.</p>
<p>Theming is different for each and every person. At Worlds of Wow, we take you and your staff through a creative brief to fully understand your DNA, culture, vision and goals. For churches many of our staff have previously served in pastoral roles within churches so they truly understand where you’re coming from in regards to working with a church project.</p>
DESC;
        $themingMetaDescription                   = <<<META

META;
        $_3artAndCharactersDescription            = <<<DESC
<p>Through the magic of sculpting, carving and forming, art is brought to life for your waiting clients. Our talented team of sculptors will take a designed rendering and transform large blocks of polystyrene into beautifully thrilling life-like creations that look like they jumped out of the pages of our concept artists.</p>
<h2>Need some 3D sculpture ideas?</h2>
<p>A realistically scaled oak tree with interactive play elements, climbing branches, knot-hole windows, in-trunk passageways, stairs and slides, incorporate play elements into a seamlessly ordinary oak tree. A waiting room disguised as a sunken pirate ship with hidden rooms, tunnels and aquatic sea life floating about the walls and ceiling. Waiting never looked so good!</p>
<p>An alpine adventure awaits! Log cabins, mountain scenery, log benches, campfires, starry nights, wild flora and fauna, babbling brooks, and animal filled meadows. Is that a red tailed fox? A beaver behind that tree? Look! a bushy tailed squirrel! The forest comes to life in your children’s space, bringing the outdoors…indoors! Let Wow carve your custom creation! Make your office come to life on the inside or outside with ridiculously creative 3-dimensional designs. The imagination is the only limiting factor when it comes to 3D design for your kid's environment.</p>
DESC;
        $_3dartAndCharactersMetaDescription       = <<<META
Worlds of Wow specializes in 3D character sculptures and fun, animal sculptures for children's play spaces. Our design and concept process will help create your vision.
META;
        $digitalWallCoveringDescription           = <<<DESC
<p>The original wall-coverings or digital murals created by Worlds of Wow provide maximum design impact with durable, certified commercial grade material specifically developed for digital designs.</p>
<p>Don't confuse wall-covering with wallpaper, these two products don't play in the same league! Allow our team to 'WOW you with an amazing original design.</p>
<p>All artwork is created in-house by our art team uniquely for your space, for your story to be told, for your desired impact to be made.</p>
<p>We only print on a commercial grade wall-covering that provides a unique micro-finish engraving treatment yielding state-of-the-art image quality to your digitally applied image.</p>
<p>We are experts in application. There is a process that needs to be strictly followed in order to achieve long-lasting results. We use adhesives, not pastes. Pastes will lead to split seams and curled edges.</p>
<p>The material we use helps protect against mildew growth. An antimicrobial agent added in the production process ensures that, when properly installed, our Digital Wall-coverings will not experience any of the problems associated with surface fungal growth. This is especially important for the dental and medical communities we serve.</p>
<p>The method of cleaning is simple – a natural sponge and mild soap solution.</p>
DESC;
        $digitalWallCoveringMetaDescription       = <<<META
Digital murals and wall coverings are a custom solution at Worlds of Wow. These quality wall applications included anti-microbial agents to help protect against mildew.
META;
        $airbrusgingDescription                   = <<<DESC
<p>The airbrush art most are familiar with typically relates to small t-shirt shops and sidewalk caricature artists. Seldom do we associate beautiful murals, delicate scenery, and creative word-art displays with this same technique. But the truth is, many of our projects incorporate incredible air brush artistry into numerous facets of the final product.</p>
<p>In the right hands, the technique utilized with airbrush design gently feathers in layers of color with soft edges, providing a 3 dimensional affect to the artwork. An extremely fine degree of atomization is what allows an artist to create such smooth blending effects using the airbrush.</p>
<p>The technique allows for the blending of two or more colors in a seamless way, with one color slowly becoming another color. Freehand airbrushed images have a floating quality, with softly defined edges between colors, and between foreground and background colors.</p>
<p>A skilled airbrush artist can produce paintings of photographic realism or can simulate almost any painting medium. Airbrush art is an excellent choice for detailed wall murals and vignettes. The artist may concept the design onsite and make changes or adjustments as they work, providing tremendous design flexibility and the opportunity to extend the mural or scenery to other areas.</p>
DESC;
        $airbrusgingMetaDescription               = <<<META
We offer airbrushing services for wall art where the surface may not be right for a different solution. Our airbrush designs brighten children's areas with fun. creative themes.
META;
        $indoorOutdoorDescription                 = <<<DESC
<p>Worlds of Wow! Funscapes is an innovative family-fun leader in the world of indoor and outdoor playgrounds. We bring new ideas and creativity to every project.</p>
<p>Our design team creates original, one-of-a-kind indoor and outdoor playground structures that fit your vision, space and budget perfectly. We are one of the select few indoor playground manufacturers based in the USA and only use American-made materials and equipment.</p>
<p>We are your one-stop shop for both indoor play attractions and custom themed environments. Branding and custom themed environments can be the difference maker in delivering the “Wow Factor” for your facility. We design original themes and scenic elements to engage both kids and parents alike!</p>
<p>We provide 100% original design, in-house fabrication and installation. In addition, we can include additional items such as safety surfacing, parent seating, modular play features, airbrushed murals, 2D relief and 3D sculpted elements, logo design, and more.</p>
<p>We are committed to providing you with the highest quality and most durable indoor playground attractions possible. Our lead designer has over 18 years experience in designing indoor contained play features, including consulting on ADA and ASTM standards. Our installation manager has over 19 years experience installing playgrounds all over the world.</p>
<h2>Why Choose Us to Design Your Playground?</h2>
<p>The two main reasons are experience and value. The most important elements of your project are getting the right design and proper installation. Nobody else offers the wealth of experience and creativity that we provide for both of these crucial elements.</p>
<p>Every project starts with a personal relationship. We are 100% confident that you will not find a better experience or a better value for your indoor play needs than with Worlds of Wow! Funscapes.</p>
<h3>What We Provide</h3>
<p>Many of the elements that we provide are known by many names:</p>
<ul>
<li>Indoor Playgrounds</li>
<li>Soft Contained Play</li>
<li>Indoor Play Equipment</li>
<li>Soft Foam</li>
<li>Toddler Play</li>
<li>Theme Playground</li>
<li>Jungle Gyms</li>
<li>Edutainment Centers</li>
<li>Interactive Play</li>
<li>Toddler Playground Equipment</li>
<li>Commercial Playground Equipment</li>
<li>Play Gyms</li>
</ul>
<p>If you hear another “industry term”, just let us know and we’ll help level the playing field and de-mystify the experience for you. We collaborate with you whether you have a small or large budget. Connect with us today so together we can deliver the most “wow” factor!</p>
<p><b>Soft Contained Play</b>. Custom, American made and fabricated, commercial indoor playground equipment for all types of facilities and destinations. We’ll help you maximize your revenue!</p>
<img />
<p><b>Kids' Theme Design</b>. We bring your space to life using original airbrushed artwork or digital murals to create an impressive environment that delivers the “wow factor”.</p>
<img />
<p><b>Climbing Walls</b>. Get the kids active with your very own climbing wall. Whether themed or non-themed, they can be tailored to fit your level of difficulty and age group.</p>
<img />
DESC;
        $indoorOutdoorMetaDescription             = <<<META
For a custom-designed indoor playground or play room for your facility, contact Worlds of Wow. Our themed play environments are found at church, daycare, and retail family fun centers.
META;
        $signageAndLogosDescription               = <<<DESC
Worlds of Wow creates customized, unique signs, specific to your theme and design. Need outdoor signs? We do those too, even 3D, sculpted and backlit for a totally cool visual effect. Don’t settle for mundane and boring.
DESC;

        $this->resolveAreaCode();

        $this->store = $this->_storeManager->getStore(Store::ADMIN_CODE);
        $this->_storeManager->setCurrentStore($this->store->getCode());

        $categoryPortfolioData = [
            'portfolio'       => [
                'data' => [
                    'url_key'          => 'portfolio',
                    'name'             => 'Portfolio',
                    'meta_title'       => 'Portfolio',
                    'display_mode'     => Category::DM_PRODUCT,
                    'is_active'        => true,
                    'is_anchor'        => false,
                    'include_in_menu'  => false,
                    'meta_description' => '',
                    'description'      => '',
                    'mm_turn_on'       => false,
                ]
            ],
        ];
        $categoriesData = [
            'market'          => [
                'data'     => [
                    'url_key'          => 'market',
                    'name'             => 'Market',
                    'meta_title'       => 'Market',
                    'display_mode'     => Category::DM_PRODUCT,
                    'is_active'        => true,
                    'is_anchor'        => false,
                    'include_in_menu'  => true,
                    'meta_description' => $marketMetaDescription,
                    'description'      => $marketDescription,
                    'mm_turn_on'       => true,
                    'mm_css_class'     => 'menu-colored-item menu-colored-item--red megamenu-submenu',
                    'configurator'     => [
                        [
                            'width' => 4,
                            'css_class' => 'megamenu-submenu-column',
                        ],
                        [
                            'width' => 4,
                            'css_class' => 'megamenu-submenu-column',
                        ],
                        [
                            'width' => 4,
                            'css_class' => 'megamenu-submenu-image',
                        ],
                    ]
                ],
                'children' => [
                    'Church/Ministry'              => [
                        'data'     => [
                            'url_key'          => 'church-play',
                            'name'             => 'Churches and Religious Facilities',
                            'meta_title'       => 'Churches and Religious Facilities',
                            'sku'              => 'church-play',
                            'status'           => true,
                            'visibility'       => 4,
                            'tax_class_id'     => 0,
                            'type_id'          => 'simple',
                            'price'            => 0,
                            'weight'           => 0,
                            'meta_description' => $churchMetaDescription,
                            'description'      => $churchDescription,
                        ],
                    ],
                    'Pediatric Dental/Medical'     => [
                        'data'     => [
                            'url_key'          => 'pediatric-dental-office-design',
                            'name'             => 'Pediatric Dental/Medical',
                            'meta_title'       => 'Pediatric Dental/Medical',
                            'sku'              => 'pediatric-dental-office-design',
                            'status'           => true,
                            'visibility'       => 4,
                            'tax_class_id'     => 0,
                            'type_id'          => 'simple',
                            'price'            => 0,
                            'weight'           => 0,
                            'meta_description' => $dentalMedicalOfficeDesignMetaDescription,
                            'description'      => $dentalMedicalOfficeDesignDescription,
                        ],
                    ],
                    'Family Entertainment Centers' => [
                        'data'     => [
                            'url_key'          => 'family-entertainment-centers',
                            'name'             => 'Family Entertainment Centers',
                            'meta_title'       => 'Family Entertainment Centers',
                            'sku'              => 'family-entertainment-centers',
                            'status'           => true,
                            'visibility'       => 4,
                            'tax_class_id'     => 0,
                            'type_id'          => 'simple',
                            'price'            => 0,
                            'weight'           => 0,
                            'meta_description' => $familyCentersDescription,
                            'description'      => $familyCentersMetaDescription,
                        ],
                    ],
                    'Military Recreation Centers'  => [
                        'data'     => [
                            'url_key'          => 'military-recreation-centers',
                            'name'             => 'Military Recreation Centers',
                            'meta_title'       => 'Military Recreation Centers',
                            'sku'              => 'military-recreation-centers',
                            'status'           => true,
                            'visibility'       => 4,
                            'tax_class_id'     => 0,
                            'type_id'          => 'simple',
                            'price'            => 0,
                            'weight'           => 0,
                            'meta_description' => $MilitaryRecreationCentersDescription,
                            'description'      => $MilitaryRecreationCentersMetaDescription,
                        ],
                    ],
                    'YMCA\'s'                      => [
                        'data'     => [
                            'url_key'          => 'ymca-design',
                            'name'             => 'YMCA\'s',
                            'meta_title'       => 'YMCA\'s',
                            'sku'              => 'ymca-design',
                            'status'           => true,
                            'visibility'       => 4,
                            'tax_class_id'     => 0,
                            'type_id'          => 'simple',
                            'price'            => 0,
                            'weight'           => 0,
                            'meta_description' => $ymcasDescription,
                            'description'      => $ymcasMetaDescription,
                        ],
                    ],
                    'Malls and Retail'             => [
                        'data'     => [
                            'url_key'          => 'retail-centers',
                            'name'             => 'Retail Centers',
                            'meta_title'       => 'Retail Centers',
                            'sku'              => 'retail-centers',
                            'status'           => true,
                            'visibility'       => 4,
                            'tax_class_id'     => 0,
                            'type_id'          => 'simple',
                            'price'            => 0,
                            'weight'           => 0,
                            'meta_description' => '',
                            'description'      => '',
                        ],
                    ]
                ]
            ],
            'facility'        => [
                'data'     => [
                    'url_key'          => 'facility',
                    'name'             => 'Facility',
                    'meta_title'       => 'Facility',
                    'display_mode'     => Category::DM_PRODUCT,
                    'is_active'        => true,
                    'is_anchor'        => false,
                    'include_in_menu'  => true,
                    'meta_description' => $facilityMetaDescription,
                    'description'      => '',
                    'mm_turn_on'       => true,
                    'mm_css_class'     => 'menu-colored-item menu-colored-item--blue megamenu-submenu',
                    'configurator'     => [
                        [
                            'width' => 6,
                            'css_class' => 'megamenu-submenu-column',
                        ],
                        [
                            'width' => 6,
                            'css_class' => 'megamenu-submenu-image',
                        ],
                    ]
                ],
                'children' => [
                    'Design and Concept'     => [
                        'data'     => [
                            'url_key'          => 'concept-services',
                            'name'             => 'Concept Services',
                            'meta_title'       => 'Concept Services',
                            'sku'              => 'concept-services',
                            'status'           => true,
                            'visibility'       => 4,
                            'tax_class_id'     => 0,
                            'type_id'          => 'simple',
                            'price'            => 0,
                            'weight'           => 0,
                            'meta_description' => $designAndConceptMetaDescription,
                            'description'      => $designAndConceptDescription,
                        ],
                    ],
                    'Theming'                => [
                        'data'     => [
                            'url_key'          => 'theming',
                            'name'             => 'Theming',
                            'meta_title'       => 'Theming',
                            'sku'              => 'theming',
                            'status'           => true,
                            'visibility'       => 4,
                            'tax_class_id'     => 0,
                            'type_id'          => 'simple',
                            'price'            => 0,
                            'weight'           => 0,
                            'meta_description' => $themingMetaDescription,
                            'description'      => $themingDescription,
                        ],
                    ],
                    '3D Art and Characters'  => [
                        'data'     => [
                            'url_key'          => '3d-art-and-characters',
                            'name'             => '3D Art & Characters',
                            'meta_title'       => '3D Art & Characters',
                            'sku'              => '3d-art-and-characters',
                            'status'           => true,
                            'visibility'       => 4,
                            'tax_class_id'     => 0,
                            'type_id'          => 'simple',
                            'price'            => 0,
                            'weight'           => 0,
                            'meta_description' => $_3dartAndCharactersMetaDescription,
                            'description'      => $_3artAndCharactersDescription,
                        ],
                    ],
                    'Digital Wall Coverings' => [
                        'data'     => [
                            'url_key'          => 'digital-wallcoverings',
                            'name'             => 'Digital Wall-coverings',
                            'meta_title'       => 'Digital Wall-coverings',
                            'sku'              => 'digital-wallcoverings',
                            'status'           => true,
                            'visibility'       => 4,
                            'tax_class_id'     => 0,
                            'type_id'          => 'simple',
                            'price'            => 0,
                            'weight'           => 0,
                            'meta_description' => $digitalWallCoveringMetaDescription,
                            'description'      => $digitalWallCoveringDescription,
                        ],
                    ],
                    'Airbrushing'            => [
                        'data'     => [
                            'url_key'          => 'airbrushing',
                            'name'             => 'Airbrushing',
                            'meta_title'       => 'Airbrushing',
                            'sku'              => 'airbrushing',
                            'status'           => true,
                            'visibility'       => 4,
                            'tax_class_id'     => 0,
                            'type_id'          => 'simple',
                            'price'            => 0,
                            'weight'           => 0,
                            'meta_description' => $airbrusgingMetaDescription,
                            'description'      => $airbrusgingDescription,
                        ],
                    ],
                    'Indoor/Outdoor Play'    => [
                        'data'     => [
                            'url_key'          => 'indoor-and-outdoor-playgrounds',
                            'name'             => 'Indoor/Outdoor Play',
                            'meta_title'       => 'Indoor/Outdoor Play',
                            'sku'              => 'indoor-and-outdoor-playgrounds',
                            'status'           => true,
                            'visibility'       => 4,
                            'tax_class_id'     => 0,
                            'type_id'          => 'simple',
                            'price'            => 0,
                            'weight'           => 0,
                            'meta_description' => $indoorOutdoorMetaDescription,
                            'description'      => $indoorOutdoorDescription,
                        ],
                    ],
                    'Signage and Logos'      => [
                        'data'     => [
                            'url_key'          => 'signage-and-logos',
                            'name'             => 'Signage and Logos',
                            'meta_title'       => 'Signage and Logos',
                            'sku'              => 'signage-and-logos',
                            'status'           => true,
                            'visibility'       => 4,
                            'tax_class_id'     => 0,
                            'type_id'          => 'simple',
                            'price'            => 0,
                            'weight'           => 0,
                            'meta_description' => '',
                            'description'      => $signageAndLogosDescription,
                        ],
                    ],
                    'Stage Design'           => [
                        'data'     => [
                            'url_key'          => 'kids-stage-design',
                            'name'             => 'Kids Stage Design',
                            'meta_title'       => 'Kids Stage Design',
                            'sku'              => 'kids-stage-design',
                            'status'           => true,
                            'visibility'       => 4,
                            'tax_class_id'     => 0,
                            'type_id'          => 'simple',
                            'price'            => 0,
                            'weight'           => 0,
                            'meta_description' => '',
                            'description'      => '',
                        ],
                    ]
                ]
            ],
            'themes'          => [
                'data'     => [
                    'url_key'          => 'themes',
                    'name'             => 'Themes',
                    'meta_title'       => 'Themes',
                    'display_mode'     => Category::DM_PRODUCT,
                    'is_active'        => true,
                    'is_anchor'        => false,
                    'include_in_menu'  => true,
                    'meta_description' => '',
                    'description'      => '',
                    'mm_turn_on'       => true,
                    'mm_css_class'     => 'menu-colored-item menu-colored-item--green megamenu-submenu',
                    'configurator'     => [
                        [
                            'width' => 6,
                            'css_class' => 'megamenu-submenu-column',
                        ],
                        [
                            'width' => 6,
                            'css_class' => 'megamenu-submenu-image',
                        ],
                    ]
                ],
                'children' => [
                    'Trees/Nature/Forest'         => [
                        'data' => [
                            'url_key'          => 'trees-nature-forest',
                            'name'             => 'Trees, Nature & Forest',
                            'meta_title'       => 'Trees, Nature & Forest',
                            'sku'              => 'trees-nature-forest',
                            'status'           => true,
                            'visibility'       => 4,
                            'tax_class_id'     => 0,
                            'type_id'          => 'simple',
                            'price'            => 0,
                            'weight'           => 0,
                            'meta_description' => '',
                            'description'      => '',
                        ],
                    ],
                    'Trains, Planes, Automobiles' => [
                        'data' => [
                            'url_key'          => 'trains-lanes-automobiles',
                            'name'             => 'Trains, Planes & Automobiles',
                            'meta_title'       => 'Trains, Planes & Automobiles',
                            'sku'              => 'trains-lanes-automobiles',
                            'status'           => true,
                            'visibility'       => 4,
                            'tax_class_id'     => 0,
                            'type_id'          => 'simple',
                            'price'            => 0,
                            'weight'           => 0,
                            'meta_description' => '',
                            'description'      => '',
                        ],
                    ],
                    'Ocean/Aquatic'               => [
                        'data' => [
                            'url_key'          => 'ocean-aquatic',
                            'name'             => 'Ocean & Aquatic',
                            'meta_title'       => 'Ocean & Aquatic',
                            'sku'              => 'ocean-aquatic',
                            'status'           => true,
                            'visibility'       => 4,
                            'tax_class_id'     => 0,
                            'type_id'          => 'simple',
                            'price'            => 0,
                            'weight'           => 0,
                            'meta_description' => '',
                            'description'      => '',
                        ],
                    ],
                    'Castle/Fairytale'            => [
                        'data' => [
                            'url_key'          => 'castle-fairytale',
                            'name'             => 'Castle & Fairytale',
                            'meta_title'       => 'Castle & Fairytale',
                            'sku'              => 'castle-fairytale',
                            'status'           => true,
                            'visibility'       => 4,
                            'tax_class_id'     => 0,
                            'type_id'          => 'simple',
                            'price'            => 0,
                            'weight'           => 0,
                            'meta_description' => '',
                            'description'      => '',
                        ],
                    ],
                    'Animals'                     => [
                        'data' => [
                            'url_key'          => 'animals',
                            'name'             => 'Animals',
                            'meta_title'       => 'Animals',
                            'sku'              => 'animals',
                            'status'           => true,
                            'visibility'       => 4,
                            'tax_class_id'     => 0,
                            'type_id'          => 'simple',
                            'price'            => 0,
                            'weight'           => 0,
                            'meta_description' => '',
                            'description'      => '',
                        ],
                    ],
                    'Geometric'                   => [
                        'data' => [
                            'url_key'          => 'geometric',
                            'name'             => 'Geometric',
                            'meta_title'       => 'Geometric',
                            'sku'              => 'geometric',
                            'status'           => true,
                            'visibility'       => 4,
                            'tax_class_id'     => 0,
                            'type_id'          => 'simple',
                            'price'            => 0,
                            'weight'           => 0,
                            'meta_description' => '',
                            'description'      => '',
                        ],
                    ]
                ]
            ],
            'request_a_quote' => [
                'data' => [
                    'url_key'          => 'request-a-quote',
                    'name'             => 'Request a quote',
                    'meta_title'       => 'Request a quote',
                    'display_mode'     => Category::DM_PAGE,
                    'is_active'        => true,
                    'is_anchor'        => false,
                    'include_in_menu'  => true,
                    'meta_description' => '',
                    'description'      => '',
                    'mm_css_class'     => 'menu-colored-item menu-colored-item--yellow',
                ],
            ],
        ];
        $staticBlocksData = [
            'market'   => [
                'title'     => 'Market',
                'stores'    => [0],
                'is_active' => true,
            ],
            'facility' => [
                'title'     => 'Facility',
                'stores'    => [0],
                'is_active' => true,
            ],
            'themes'   => [
                'title'     => 'Themes',
                'stores'    => [0],
                'is_active' => true,
            ],
        ];
        $configurationData = [
            Data::PATH_CONFIG_PHONE                          => '817-380-4215',
            Data::PATH_CONFIG_COUNTRY                        => 'US',
            Data::PATH_CONFIG_REGION                         => $this->helper->getRegionIdByName('Texas', 'US'),
            Data::PATH_CONFIG_CITY                           => 'Denton',
            Data::PATH_CONFIG_STREET_1                       => '1800 Shady Oaks Drive',
            Data::PATH_CONFIG_POSTCODE                       => '76205',
            Data::PATH_CONFIG_FOOTER_COPYRIGHT               => '&copy; 2018 Worlds of Wow!, A PlayCore Company. All Rights Reserved.',
            TemplateMonsterBlogData::CONFIG_PATH_MENU_ACTIVE => 0,
            'catalog/seo/product_url_suffix'                 => '',
            'catalog/seo/category_url_suffix'                => '',
            'catalog/frontend/grid_per_page_values'          => '8,10,12',
            'catalog/frontend/grid_per_page'                 => '8',
        ];

        $this->createCategories($categoryPortfolioData);
        $this->createCategories($categoriesData);
        $this->createStaticBlocks($staticBlocksData);
        $this->adjustBlogModules('Aheadworks_Blog');
        $this->configurationSetup($configurationData);

        $setup->endSetup();
    }

    /**
     * Creates categories
     *
     * @param array $categoryArray
     * @param int $parentCategoryId
     */
    protected function createCategories($categoryArray = [], $parentCategoryId = 2)
    {
        if (count($categoryArray) > 0) {
            foreach ($categoryArray as $categoryInfo) {
                $category = $this->categoryFactory->create();
                $category = $category->loadByAttribute('url_key', $categoryInfo['data']['url_key']);

                if (!$category) {
                    $parentCategoryId = $parentCategoryId ?? $this->store->getStoreRootCategoryId();
                    echo "Creating category: {$categoryInfo['data']['url_key']}\n";
                    /** @var Category $parentCategory */
                    $parentCategory = $this->categoryFactory->create();
                    $parentCategory = $parentCategory->load($parentCategoryId);

                    /** @var Category $category */
                    $category = $this->categoryFactory->create();
                    $category->setStoreId($this->store->getId());
                    $category->addData($categoryInfo['data']);
                    $category->setPath($parentCategory->getPath());
                    $category->setParentId($parentCategory->getId());
                    $category->setAttributeSetId($category->getDefaultAttributeSetId());
                    $category->save();
                }

                if (isset($categoryInfo['children'])) {
                    $productIds = [];

                    if (count($categoryInfo['children']) > 0) {
                        $productIds = $this->createProducts($categoryInfo['children'], [$category->getId(), $this->getCategoryIdByUrlKey('portfolio')]);
                    }

                    $productInColumns = array_chunk($productIds, ceil(count($productIds) / (count($categoryInfo['data']['configurator']) - 1)));
                    $productColumnJson = [];

                    foreach ($productInColumns as $productColumn) {
                        $productColumnJson[] = $this->helper->getConfiguratorJson($productColumn);
                    }

                    $configuratorString = '[[';
                    $blockWidth = 12;
                    for ($i = 0; $i < count($categoryInfo['data']['configurator']) - 1; $i++) {
                        $configuratorString .= '{"width":"' . $categoryInfo['data']['configurator'][$i]['width'] . '","css_class":"' . $categoryInfo['data']['configurator'][$i]['css_class'] . '","entities":[{"value":"products:{' . $productColumnJson[$i][0] . '}","text":"Products - ' . $productColumnJson[$i][1] . '"}]},';
                        $blockWidth -= (int)$categoryInfo['data']['configurator'][$i]['width'];
                    }
                    $block = $this->blockFactory->create()->load($category->getUrlKey(), 'identifier');
                    $configuratorString .= '{"width":"' . $blockWidth . '","css_class":"megamenu-submenu-image","entities":[{"value":"block:' . $block->getId() . '","text":"' . $category->getName() . '"}]}';
                    $configuratorString .= ']]';
                    echo "\n\t\tConfigurator string:\n" . $configuratorString . "\n";
                    $category
                        ->setData('mm_configurator', $configuratorString)
                        ->setData('mm_turn_on', 1);
                    $category->save();
                }
            }
        }
    }

    /**
     * Creates products and returns array of created product ids
     *
     * @param array $productArrayInfo
     * @param array $categoryIds
     * @return array
     */
    protected function createProducts($productArrayInfo = [], $categoryIds = [2])
    {
        $result = [];
        if (count($productArrayInfo) > 0) {
            foreach ($productArrayInfo as $productInfo) {
                /** @var Product $product */
                $product = $this->productFactory->create();
                $product->loadByAttribute('url_key', $productInfo['data']['url_key']);

                echo "Creating product: {$productInfo['data']['url_key']}\n";
                /** @var Category $category */
                $category   = $this->categoryFactory->create();
                $categoryId = $categoryIds[0] ?? $this->store->getStoreRootCategoryId();
                $category   = $category->load($categoryId);

                $product->setStoreId($this->store->getId());
                $product->setWebsiteIds([1]);
                $product->addData($productInfo['data']);
                $product->setAttributeSetId($product->getDefaultAttributeSetId());
                $product->setStockData([
                        'use_config_manage_stock' => 0,
                        'manage_stock' => 1,
                        'is_in_stock' => 1,
                        'qty' => 999999999
                ]);
                $product->save();
                $result[] = $product->getId();

                $this->categoryLinkManagement->assignProductToCategories(
                    $product->getSku(),
                    $categoryIds
                );
            }
        }
        return $result;
    }

    /**
     * Disable module
     *
     * @param $moduleName
     */
    protected function adjustBlogModules($moduleName)
    {
        /**
         * @var \Magento\Framework\Module\Status $status
         */
        $status = $this->moduleStatusFactory->create();
        $status->setIsEnabled(false, [$moduleName]);
    }

    /**
     * Saves configuration
     *
     * @param array $configArray
     */
    protected function configurationSetup($configArray = [])
    {
        if ($configArray) {
            foreach ($configArray as $path => $value) {
                $this->helper->setDefaultConfig($path, $value);
            }
        }
    }

    /**
     * Creates or updates static CMS block
     *
     * @param $staticBlocksData
     */
    protected function createStaticBlocks($staticBlocksData)
    {
        if ($staticBlocksData) {
            foreach ($staticBlocksData as $identifier => $staticBlockData) {
                /**
                 * @var $block Block
                 */
                $block = $this->blockFactory->create();
                $block->load($identifier, 'identifier');
                $block->setIdentifier($identifier);
                $block->addData($staticBlockData);
                $block->save();
            }
        }
    }

    /**
     * Sets admin area
     * If it is already set and you set it again - it throws an exception,
     * and if it is not set and you try to get it - it throws an exception!
     */
    protected function resolveAreaCode()
    {
        try {
            $this->state->getAreaCode();
        } catch (\Exception $e) {
            $this->state->setAreaCode('adminhtml');
        }
    }

    /**
     * Retrieve Category Id By Its Url_Key
     *
     * @param $urlKey
     * @return mixed
     */
    protected function getCategoryIdByUrlKey($urlKey)
    {
        return $this->categoryFactory->create()->loadByAttribute('url_key', $urlKey)->getId();
    }
}