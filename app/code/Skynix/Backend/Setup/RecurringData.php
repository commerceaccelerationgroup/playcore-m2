<?php
/**
 * Created by Skynix Team.
 * User: oleksii bulba
 * Date: 22.06.18
 * Time: 16:05
 */

namespace Skynix\Backend\Setup;


use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Theme\Model\Config;
use Magento\Theme\Model\ResourceModel\Theme\CollectionFactory;
use Magento\Store\Model\Store;

class RecurringData implements InstallDataInterface
{
    const THEME_NAME = 'TemplateMonster/theme068';

    /**
     * @var Config
     */
    private $_themeConfig;
    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;


    public function __construct(
        CollectionFactory $collectionFactory,
        Config $themeConfig
    ) {
        $this->_themeConfig = $themeConfig;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * Installs data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $themes = $this->collectionFactory->create()->loadRegisteredThemes();
        echo "Available themes:\n";

        /**
         * @var \Magento\Theme\Model\Theme $theme
         */
        foreach ($themes as $theme) {
            echo "Theme: {$theme->getCode()}";
            if ($theme->getCode() == self::THEME_NAME) {
                echo " applied!";
                $this->_themeConfig->assignToStore(
                    $theme,
                    [Store::DEFAULT_STORE_ID],
                    ScopeConfigInterface::SCOPE_TYPE_DEFAULT
                );
            }
            echo "\n";
        }

        $setup->endSetup();
    }
}