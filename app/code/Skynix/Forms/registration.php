<?php
/**
 * Created by Skynix Team.
 * User: oleksii bulba
 * Date: 5.07.18
 * Time: 19:09
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Skynix_Forms',
    __DIR__
);
