<?php
/**
 * Created by Skynix Team.
 * User: oleksii bulba
 * Date: 09.07.18
 * Time: 14:15
 */

namespace Skynix\Forms\Observer;


use Magento\Framework\Data\Tree\Node;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\UrlInterface;
use Skynix\Forms\Helper\Data as HelperData;

class Topmenu implements ObserverInterface
{
    /**
     * @var HelperData
     */
    protected $helper;

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * Topmenu constructor.
     *
     * @param HelperData   $helper
     * @param UrlInterface $urlBuilder
     */
    public function __construct(
        HelperData $helper,
        UrlInterface $urlBuilder
    ) {
        $this->helper = $helper;
        $this->urlBuilder = $urlBuilder;
    }

    public function execute(EventObserver $observer)
    {
        /**
         * @var $menu \Magento\Framework\Data\Tree\Node
         */
        $menu = $observer->getMenu();
        $tree = $menu->getTree();

        $data = [
            'name'      => __('Contact'),
            'id'        => 'contact',
            'url'       => $this->urlBuilder->getUrl('contact/'),
            'is_active' => '',
        ];

        $node = new Node($data, 'contact', $tree, $menu);
        $menu->addChild($node);

        return $this;
    }
}