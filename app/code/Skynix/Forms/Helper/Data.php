<?php
/**
 * Created by Skynix Team.
 * User: oleksii bulba
 * Date: 09.07.18
 * Time: 14:19
 */

namespace Skynix\Forms\Helper;


use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class Data
{
    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->_scopeConfig = $scopeConfig;
    }

    protected function getConfigValue($path, $scope)
    {
        return $this->_scopeConfig->getValue($path, $scope);
    }
}