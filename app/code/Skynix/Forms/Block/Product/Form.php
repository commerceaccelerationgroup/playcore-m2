<?php
/**
 * Created by Skynix Team.
 * User: oleksii bulba
 * Date: 13.07.18
 * Time: 11:55
 */

namespace Skynix\Forms\Block\Product;


use Magento\Catalog\Block\Product\View;

class Form extends View
{
    const FORM_ACTION_CONTACT = 'skynix/request/new';

    public function getFormAction()
    {
        return $this->getUrl(self::FORM_ACTION_CONTACT, ['_secure' => true]);
    }

    public function getProductSku()
    {
        return $this->getProduct()->getSku();
    }

    public function getProductId()
    {
        return $this->getProduct()->getId();
    }

    public function getCategoryId()
    {
        return $this->getProduct()->getCategoryId();
    }

    public function getProductName()
    {
        return $this->getProduct()->getName();
    }

    /**
     * Retrieve form data
     *
     * @return mixed
     */
    public function getFormData()
    {
        $data = $this->getData('form_data');
        if ($data === null) {
            $formData = $this->customerSession->getCustomerFormData(true);
            $data = new \Magento\Framework\DataObject();
            if ($formData) {
                $data->addData($formData);
                $data->setCustomerData(1);
            }
            if (isset($data['region_id'])) {
                $data['region_id'] = (int)$data['region_id'];
            }
            $this->setData('form_data', $data);
        }
        return $data;
    }

    /**
     * Retrieve customer region identifier
     *
     * @return mixed
     */
    public function getRegion()
    {
        if (null !== ($region = $this->getFormData()->getRegion())) {
            return $region;
        } elseif (null !== ($region = $this->getFormData()->getRegionId())) {
            return $region;
        }
        return null;
    }
}