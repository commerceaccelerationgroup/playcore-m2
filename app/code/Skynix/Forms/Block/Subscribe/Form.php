<?php
/**
 * Created by Skynix Team.
 * User: sergey marchuk
 * Date: 7/31/18
 * Time: 11:36 AM
 */

namespace Skynix\Forms\Block\Subscribe;

use Magento\Catalog\Block\Product\View;

class Form extends View
{
    const FORM_ACTION_SUBSCRIBE = 'newsletter/subscriber/new/';

    public function getFormAction()
    {
        return $this->getUrl(self::FORM_ACTION_SUBSCRIBE, ['_secure' => true]);
    }

    /**
     * Retrieve form data
     *
     * @return mixed
     */
    public function getFormData()
    {
        $data = $this->getData('form_data');
        if ($data === null) {
            $formData = $this->customerSession->getCustomerFormData(true);
            $data = new \Magento\Framework\DataObject();
            if ($formData) {
                $data->addData($formData);
            }
            if (isset($data['region_id'])) {
                $data['region_id'] = (int)$data['region_id'];
            }
            $this->setData('form_data', $data);
        }
        return $data;
    }

    /**
     * Retrieve customer region identifier
     *
     * @return mixed
     */
    public function getRegion()
    {
        if (null !== ($region = $this->getFormData()->getRegion())) {
            return $region;
        } elseif (null !== ($region = $this->getFormData()->getRegionId())) {
            return $region;
        }
        return null;
    }

    public function getIdentities()
    {
        $identities = [];

        if (is_array($this->getItems()) || is_object($this->getItems()))
        {
            foreach ($this->getItems() as $item)
            {
                $identities = array_merge($identities, $item->getIdentities());
            }
        }
        return $identities;
    }
}