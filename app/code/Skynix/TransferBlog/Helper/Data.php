<?php
/**
 * Created by Skynix Team.
 * User: oleksii bulba
 * Date: 29.08.18
 * Time: 16:01
 */

namespace Skynix\TransferBlog\Helper;


use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{
    /**
     * Cleans the string for identifier.
     * Replaces all spaces, hyphens and other special chars with dashes and cleans them (removes repeats and edge dashes)
     *
     * @param string $string
     * @return string
     */
    public function cleanString($string)
    {
        $identifier = preg_replace('/[^A-Za-z0-9\-]+/', '-', $string);
        $identifier = trim($identifier, "-");
        return strtolower($identifier);
    }
}