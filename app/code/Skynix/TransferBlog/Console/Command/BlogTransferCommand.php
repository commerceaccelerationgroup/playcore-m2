<?php
/**
 * Created by Skynix Team.
 * Date: 01.08.18
 * Time: 12:25
 */

namespace Skynix\TransferBlog\Console\Command;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\ObjectManagerInterface;
use Skynix\TransferBlog\Helper\Data;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\Xml\Parser;
use TemplateMonster\Blog\Model\PostFactory;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\AdapterInterface;

class BlogTransferCommand extends Command
{
    /**
     * @var Parser
     */
    private $parser;

    /**
     * @var PostFactory
     */
    private $postFactory;

    /**
     * @var AdapterInterface
     */
    private $defaultConection;

    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * BlogTransferCommand constructor.
     * @param Parser $parser
     * @param PostFactory $postFactory
     * @param ResourceConnection $resourceConnection
     * @param ObjectManagerInterface $objectManager
     * @param Data $helper
     */
    public function __construct(
        Parser $parser,
        PostFactory $postFactory,
        ResourceConnection $resourceConnection,
        ObjectManagerInterface $objectManager,
        Data $helper
    ) {
        parent::__construct();
        $this->parser = $parser;
        $this->postFactory = $postFactory;
        $this->defaultConection = $resourceConnection->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
        $this->_objectManager = $objectManager;
        $this->helper = $helper;
    }

    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->setName('skynix:transferBlog')->setDescription('Transfer blog content from xml file into a new blog');
        $this->addArgument('path', InputArgument::REQUIRED, __('Path to file with name of file xml'));//argument path to file
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     * @throws \Exception
     */
    protected function execute( InputInterface $input, OutputInterface $output )
    {
        $path = $input->getArgument('path');

        if ( file_exists( $path ) ) {
            $file = $path;
        } else {
            throw new \Exception("File " . $path . " not found");
        }

        if ( file_exists( $file ) ) {

            $parsedArray = $this->parser->load($file)->xmlToArray();

            if($parsedArray && isset($parsedArray['feed']['entry'])) {
                foreach ($parsedArray['feed']['entry'] as $key=>$item) {

                    if(isset($item['title']['_value'])) {
                        $data = null;
                        $data["title"]              = $item['title']['_value'];
                        $data["identifier"]         = $this->helper->cleanString($data["title"]);
                        $data["stores"]             = 0;
                        $data["is_visible"]         = true;
                        $data["comments_enabled"]   = true;
                        $data["creation_time"]      = $item['published'];
                        $data["update_time"]        = $item['updated'];
                        $data["content"]            = $item['content']['_value'];
                        $data["image"]              = null;
                        if(isset($item['author']['name']) && $item['author']['name'] != 'Anonymous') {
                            $data["author"]         = $item['author']['name'];
                        }

                        $url = $this->getImagesUrl($data["content"]);

                        /** @var \TemplateMonster\Blog\Model\Post $model */
                        $model = $this->postFactory->create();
                        $model->load($data["identifier"], 'identifier');

                        if($model->getId()) {
                            try{
                                $post = $this->defaultConection->query(
                                    $this->defaultConection
                                        ->select()
                                        ->from($this->defaultConection->getTableName('tm_blog_post'))
                                        ->columns('image')
                                        ->where('post_id = ?', $model->getId())
                                )->fetch();
                                $image = $post['image'];
                                $output->writeln('Image: ' . $image);

                                if (!empty($image)) {
                                    $data['image'] = $image;
                                } elseif ($savedImage = $this->saveImage($url, $data["identifier"], $output)) {
                                    $data['image'] = 'tm_blog' . $savedImage;
                                }

                                $this->defaultConection
                                    ->update(
                                        'tm_blog_post',
                                        [   'title'   => $data["title"],
                                            'content' => $data["content"],
                                            'image'   => $data["image"]
                                        ],
                                        ['post_id = ?' => $model->getId()]
                                    );
                                $output->writeln('Updated Post "' . $data["title"] . '"');
                            } catch (\Exception $e) {
                                $output->writeln($e->getMessage());
                                $output->writeln($e->getTraceAsString());
                            }
                        } else {
                            $model->setData($data);

                            if ($savedImage = $this->saveImage($url, $data["identifier"], $output)) {
                                $data['image'] = 'tm_blog' . $savedImage;
                                $output->writeln("\tpost image is downloaded from $url to {$data['image']} in the media folder");
                            }
                            $model->setData('image', $data['image']);

                            try{
                                $model->save();
                                $output->writeln('Created Post "' . $data["title"] . '"');
                            } catch (\Exception $e) {
                                $output->writeln($e->getMessage());
                                $output->writeln($e->getTraceAsString());
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Saves first valid image from URLs list and return saved image filename.
     * Returns false if URLs list has no valid image URL
     *
     * @param $imageUrls
     * @param $filename
     * @param OutputInterface $output
     * @return string|bool
     */
    protected function saveImage($imageUrls, $filename, OutputInterface $output)
    {
        $imageUrl = $this->getValidImageURL($imageUrls);

        if (!$imageUrl) {
            return false;
        }

        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $imageUrl);
        curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Cirkel');
        $query = curl_exec($curl_handle);
        curl_close($curl_handle);

        /**
         * @var $fileSystem \Magento\Framework\Filesystem
         */
        $fileSystem = $this->_objectManager->create('Magento\Framework\Filesystem');
        $mediaDirectory = $fileSystem->getDirectoryRead(DirectoryList::MEDIA);
        $destinationFolder = $mediaDirectory->getAbsolutePath('tm_blog');
        $ext = pathinfo($imageUrl, PATHINFO_EXTENSION);
        $filename = $this->validateFileName(self::_addDirSeparator($destinationFolder) . $filename . '.' . $ext);
        $destinationFile = self::_addDirSeparator($destinationFolder) . $filename;

        $output->writeln("Image will be written to file $destinationFile from $imageUrl");
        file_put_contents($destinationFile, $query);

        return '/' . $filename;
    }

    /**
     * Retrieves first valid image from URLs list
     * Returns false if URLs list has no valid image URL
     *
     * @param $imageUrls
     * @return string|bool
     */
    protected function getValidImageURL($imageUrls)
    {
        for ($i = 0; $i < count($imageUrls); $i++) {
            if (@getimagesize($imageUrls[$i])) {
                return $imageUrls[$i];
            }
        }

        return false;
    }

    /**
     * Retrieves image URLs from string using RegExp
     *
     * @param $postContent
     * @return bool
     */
    protected function getImagesUrl($postContent)
    {
        preg_match_all('#\bhttps?:\/\/[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|\/))#', $postContent, $match);
        if (count($match) > 0) {
            return $match[0];
        }

        return false;
    }

    /**
     * Adds Directory Separator
     *
     * @param $dir
     * @return string
     */
    protected static function _addDirSeparator($dir)
    {
        if (substr($dir, -1) != '/') {
            $dir .= '/';
        }
        return $dir;
    }

    /**
     * Validates destination filename:
     *  if the same name exists, index is being added and increased while new filename does not exist
     *
     * @param $destinationFile
     * @return string
     */
    protected function validateFileName($destinationFile)
    {
        $fileInfo = pathinfo($destinationFile);
        if (file_exists($destinationFile)) {
            $index = 1;
            $baseName = $fileInfo['filename'] . '.' . $fileInfo['extension'];
            while (file_exists($fileInfo['dirname'] . '/' . $baseName)) {
                $baseName = $fileInfo['filename'] . '_' . $index . '.' . $fileInfo['extension'];
                $index++;
            }
            $destFileName = $baseName;
        } else {
            return $fileInfo['basename'];
        }

        return $destFileName;
    }
}