<?php
/**
 * Created by Skynix Team.
 * Date: 1.08.18
 * Time: 12:08
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Skynix_TransferBlog',
    __DIR__
);