<?php
/**
 * Created by Skynix Team.
 * User: oleksii bulba
 * Date: 29.08.18
 * Time: 13:11
 */

namespace Skynix\TransferBlog\Setup;


use Skynix\TransferBlog\Helper\Data;
use TemplateMonster\Blog\Model\Post;
use TemplateMonster\Blog\Model\ResourceModel\Post\Collection as PostCollection;
use TemplateMonster\Blog\Model\ResourceModel\Post\CollectionFactory as PostCollectionFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var PostCollectionFactory
     */
    protected $postCollectionFactory;

    /**
     * @var Data
     */
    protected $helper;

    public function __construct(
        PostCollectionFactory $postCollectionFactory,
        Data $helper
    ) {
        $this->postCollectionFactory = $postCollectionFactory;
        $this->helper = $helper;
    }

    /**
     * Upgrades data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '0.0.2') < 0) {
            /**
             * @var $postCollection PostCollection
             * @var $post Post
             */
            echo "\nLoading post collection...";
            $postCollection = $this->postCollectionFactory->create();
            $i = 0;
            foreach ($postCollection as $post) {
                $i++;
                $identifier = $post->getData('identifier');
                echo "\n\tPost #$i";
                echo "\n\t\tBefore: $identifier";
                $identifier = $this->helper->cleanString($identifier);
                echo "\n\t\tAfter: $identifier";
                $post->setData('identifier', $identifier);
                $post->save();
            }
            echo "\nDone: $i post(s)!\n";
        }

        $setup->endSetup();
    }
}