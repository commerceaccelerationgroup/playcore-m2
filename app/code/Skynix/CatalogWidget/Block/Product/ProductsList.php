<?php
/**
 * Created by Skynix Team.
 * User: oleksii bulba
 * Date: 29.06.18
 * Time: 15:54
 */

namespace Skynix\CatalogWidget\Block\Product;


use Magento\CatalogUrlRewrite\Model\ProductUrlRewriteGenerator;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\UrlRewrite\Model\UrlFinderInterface;
use Magento\UrlRewrite\Service\V1\Data\UrlRewrite;

class ProductsList extends \Magento\CatalogWidget\Block\Product\ProductsList
{

    /**
     * Default value whether show pager or not
     */
    const DEFAULT_SHOW_LINK = false;

    /**
     * Prepared href attribute
     *
     * @var string
     */
    protected $_href;

    /**
     * Url finder for category
     *
     * @var UrlFinderInterface
     */
    protected $urlFinder;

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility,
        \Magento\Framework\App\Http\Context $httpContext,
        \Magento\Rule\Model\Condition\Sql\Builder $sqlBuilder,
        \Magento\CatalogWidget\Model\Rule $rule,
        \Magento\Widget\Helper\Conditions $conditionsHelper,
        array $data = [],
        Json $json = null,
        UrlFinderInterface $urlFinder
    ) {
        $this->urlFinder = $urlFinder;

        parent::__construct(
            $context,
            $productCollectionFactory,
            $catalogProductVisibility,
            $httpContext,
            $sqlBuilder,
            $rule,
            $conditionsHelper,
            $data,
            $json
        );
    }

    public function createCollection()
    {
        return parent::createCollection()->addAttributeToSort('created_at', 'desc');
    }


    /**
     * Prepare url using passed id path and return it
     * or return false if path was not found in url rewrites.
     *
     * @throws \RuntimeException
     * @return string|false
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function getHref()
    {
        if ($this->showLink()) {
            if ($this->_href === null) {
                if (!$this->getData('id_path')) {
                    throw new \RuntimeException('Parameter id_path is not set.');
                }
                $rewriteData = $this->parseIdPath($this->getData('id_path'));
                $href = false;
                $store = $this->hasStoreId() ? $this->_storeManager->getStore($this->getStoreId())
                    : $this->_storeManager->getStore();
                $filterData = [
                    UrlRewrite::ENTITY_ID => $rewriteData[1],
                    UrlRewrite::ENTITY_TYPE => $rewriteData[0],
                    UrlRewrite::STORE_ID => $store->getId(),
                ];
                if (!empty($rewriteData[2]) && $rewriteData[0] == ProductUrlRewriteGenerator::ENTITY_TYPE) {
                    $filterData[UrlRewrite::METADATA]['category_id'] = $rewriteData[2];
                }
                $rewrite = $this->urlFinder->findOneByData($filterData);
                if ($rewrite) {
                    $href = $store->getUrl('', ['_direct' => $rewrite->getRequestPath()]);
                    if (strpos($href, '___store') === false) {
                        $href .= (strpos($href, '?') === false ? '?' : '&') . '___store=' . $store->getCode();
                    }
                }
                $this->_href = $href;
            }
            return $this->_href;
        }
        return '';
    }

    /**
     * Parse id_path
     *
     * @param string $idPath
     * @throws \RuntimeException
     * @return array
     */
    protected function parseIdPath($idPath)
    {
        $rewriteData = explode('/', $idPath);

        if (!isset($rewriteData[0]) || !isset($rewriteData[1])) {
            throw new \RuntimeException('Wrong id_path structure.');
        }
        return $rewriteData;
    }

    /**
     * Return flag whether link need to be shown or not
     *
     * @return bool
     */
     function showLink()
    {
        if (!$this->hasData('show_link')) {
            $this->setData('show_link', self::DEFAULT_SHOW_LINK);
        }
        return (bool)$this->getData('show_link');
    }
}