<?php
/**
 * Created by Skynix Team.
 * User: oleksii bulba
 * Date: 19.07.18
 * Time: 19:49
 */

namespace Skynix\Leads\Setup;


use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Skynix\Leads\Model\Lead;

class InstallData implements InstallDataInterface
{
    /**
     * @var LeadSetupFactory
     */
    protected $leadSetupFactory;

    public function __construct(
        LeadSetupFactory $leadSetupFactory
    ) {
        $this->leadSetupFactory = $leadSetupFactory;
    }

    /**
     * Installs data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $leadEntity = Lead::ENTITY;
        /**
         * @var $leadSetup LeadSetup
         */
        $leadSetup = $this->leadSetupFactory->create(['setup' => $setup]);
        $leadSetup->installEntities();

        $attributesInfo = [
            'company'                 => [
                'type'           => 'varchar',
                'label'          => 'Company',
                'input'          => 'text',
                'required'       => false,
                'validate_rules' => '{"max_text_length":255,"min_text_length":1}',
                'global'         => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_WEBSITE,
                'position'       => 40,
            ],
            'street'                  => [
                'type'           => 'varchar',
                'label'          => 'Street Address',
                'input'          => 'text',
                'validate_rules' => '{"max_text_length":255,"min_text_length":1}',
                'global'         => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_WEBSITE,
                'position'       => 50,
            ],
            'city'                    => [
                'type'           => 'varchar',
                'label'          => 'City',
                'input'          => 'text',
                'validate_rules' => '{"max_text_length":255,"min_text_length":1}',
                'global'         => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_WEBSITE,
                'position'       => 60,
            ],
            'region'                  => [
                'type'       => 'varchar',
                'label'      => 'State/Province',
                'input'      => 'text',
                'backend'    => \Skynix\Leads\Model\ResourceModel\Lead\Attribute\Backend\Region::class,
                'required'   => false,
                'global'     => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_WEBSITE,
                'position'   => 70,
            ],
            'region_id'               => [
                'type'       => 'int',
                'label'      => 'State/Province',
                'input'      => 'hidden',
                'required'   => false,
                'source'     => \Skynix\Leads\Model\ResourceModel\Lead\Attribute\Source\Region::class,
                'backend'    => \Skynix\Leads\Model\ResourceModel\Lead\Attribute\Backend\Region::class,
                'global'     => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_WEBSITE,
                'position'   => 80,
            ],
            'postcode'                => [
                'type'           => 'varchar',
                'label'          => 'Zip/Postal Code',
                'input'          => 'text',
                'required'       => false,
                'validate_rules' => '[]',
                'global'         => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_WEBSITE,
                'position'       => 90,
            ],
            'telephone'               => [
                'type'           => 'varchar',
                'label'          => 'Phone Number',
                'input'          => 'text',
                'validate_rules' => '{"max_text_length":255,"min_text_length":1}',
                'global'         => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_WEBSITE,
                'position'       => 100,
            ],
            'method_of_contact'       => [
                'type'       => 'int',
                'label'      => 'Method of Contact',
                'input'      => 'select',
                'source'     => \Skynix\Leads\Model\ResourceModel\Lead\Attribute\Source\ContactMethod::class,
                'global'     => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_WEBSITE,
                'position'   => 110,
            ],
            'quote_type'              => [
                'type'       => 'int',
                'label'      => 'Type of Quote',
                'input'      => 'select',
                'source'     => \Skynix\Leads\Model\ResourceModel\Lead\Attribute\Source\Quote::class,
                'global'     => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_WEBSITE,
                'position'   => 120,
            ],
            'newsletter_subscription' => [
                'type'       => 'int',
                'label'      => 'Newsletter Subscription',
                'input'      => 'boolean',
                'backend'    => \Skynix\Leads\Model\ResourceModel\Lead\Attribute\Backend\Data\Boolean::class,
                'global'     => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_WEBSITE,
                'position'   => 130,
            ],
            'notes'                   => [
                'type'       => 'text',
                'label'      => 'Notes',
                'input'      => 'textarea',
                'global'     => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_WEBSITE,
                'position'   => 140,
            ],
            'found_via'               => [
                'type'       => 'varchar',
                'label'      => 'Found Via',
                'input'      => 'select',
                'source'     => \Skynix\Leads\Model\ResourceModel\Lead\Attribute\Source\FoundSource::class,
                'global'     => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_WEBSITE,
                'position'   => 150,
            ],
            'created_at'              => [
                'type'       => 'datetime',
                'label'      => 'Created At',
                'input'      => 'date',
                'required'   => false,
                'visible'    => false,
                'system'     => false,
                'global'     => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_WEBSITE,
                'position'   => 160,
            ],
            'product_id'              => [
                'type'       => 'int',
                'label'      => 'Product Id',
                'input'      => 'select',
                'source'     => \Skynix\Leads\Model\ResourceModel\Lead\Attribute\Source\Product::class,
                'global'     => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_WEBSITE,
                'position'   => 170,
            ],
        ];

        foreach ($attributesInfo as $attributeCode => $attributeParams) {
            $leadSetup->addAttribute($leadEntity, $attributeCode, $attributeParams);
        }

        $setup->endSetup();
    }
}