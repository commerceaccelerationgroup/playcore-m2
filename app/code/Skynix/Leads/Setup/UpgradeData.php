<?php
/**
 * Created by Skynix Team.
 * User: oleksii bulba
 * Date: 20.07.18
 * Time: 18:12
 */

namespace Skynix\Leads\Setup;


use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Skynix\Leads\Model\Lead;
use Skynix\Leads\Model\LeadFactory;
use Skynix\Leads\Model\ResourceModel\Lead\Attribute\Source\ContactMethod;
use Skynix\Leads\Model\ResourceModel\Lead\Attribute\Source\FoundSource;
use Skynix\Leads\Model\ResourceModel\Lead\Attribute\Source\Quote;

class UpgradeData implements UpgradeDataInterface
{

    protected $leadFactory;

    public function __construct(
        LeadFactory $leadFactory
    ) {
        $this->leadFactory = $leadFactory;
    }

    /**
     * Upgrades data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '0.0.2') < 0) {
            /**
             * @var $lead Lead
             */
            $this->leadFactory
                ->create()
                ->addData([
                    'first_name'              => 'John',
                    'last_name'               => 'Smith',
                    'email'                   => 'john.smith@example.com',
                    'company'                 => 'Earth Inc.',
                    'street'                  => '76 Ocean Street',
                    'city'                    => 'San Antonio',
                    'region'                  => 'Texas',
                    'postcode'                => '78213',
                    'telephone'               => '509-306-4485',
                    'method_of_contact'       => ContactMethod::CONTACT_BY_EMAIL,
                    'quote_type'              => Quote::QUOTE_TYPE_PLAYGROUND,
                    'newsletter_subscription' => 0,
                    'notes'                   => 'Hello! I love your service! Thank you',
                    'found_via'               => FoundSource::FOUND_VIA_OTHER,
                ])
                ->save();

            $this->leadFactory
                ->create()
                ->addData([
                    'first_name'              => 'Gary',
                    'last_name'               => 'Sinclair',
                    'email'                   => 'gary.sinclair@example.com',
                    'company'                 => 'Earthworks Yard Maintenance',
                    'street'                  => '76 Ocean Street',
                    'city'                    => 'Churchville',
                    'region'                  => 'Pennsylvania',
                    'postcode'                => '18966',
                    'telephone'               => '717-448-0384',
                    'method_of_contact'       => ContactMethod::CONTACT_BY_PHONE,
                    'quote_type'              => Quote::QUOTE_TYPE_BOTH,
                    'newsletter_subscription' => 1,
                    'notes'                   => '',
                    'found_via'               => FoundSource::FOUND_VIA_OTHER,
                ])
                ->save();
        }

        $setup->endSetup();
    }
}