<?php
/**
 * Created by Skynix Team.
 * User: oleksii bulba
 * Date: 19.07.18
 * Time: 19:50
 */

namespace Skynix\Leads\Setup;


use Magento\Eav\Setup\EavSetup;
use Skynix\Leads\Model\Lead;

class LeadSetup extends EavSetup
{
    /**
     * Retrieve default entity: lead
     *
     * @return array
     */
    public function getDefaultEntities() {

        $leadEntity = Lead::ENTITY;

        $entities = [
            $leadEntity => [
                'entity_model' => \Skynix\Leads\Model\ResourceModel\Lead::class, //the full resource model class name
                'table' => $leadEntity . '_entity',
                'attributes' => [
                    'first_name' => [
                        'type'           => 'static',
                        'label'          => 'First Name',
                        'input'          => 'text',
                        'validate_rules' => '{"max_text_length":255,"min_text_length":1}',
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_WEBSITE,
                        'sort_order'     => 10,
                        'position'       => 10,
                    ],
                    'last_name' => [
                        'type'           => 'static',
                        'label'          => 'Last Name',
                        'input'          => 'text',
                        'validate_rules' => '{"max_text_length":255,"min_text_length":1}',
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_WEBSITE,
                        'sort_order'     => 20,
                        'position'       => 20,
                    ],
                    'email' => [
                        'type'           => 'static',
                        'label'          => 'Email',
                        'input'          => 'text',
                        'required'       => true,
                        'validate_rules' => '{"input_validation":"email"}',
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_WEBSITE,
                        'sort_order'     => 30,
                        'position'       => 30,
                    ]
                ],
            ],
        ];

        return $entities;
    }
}