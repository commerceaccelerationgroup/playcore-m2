<?php
/**
 * Created by Skynix Team.
 * Date: 27.07.18
 * Time: 12:28
 */
namespace Skynix\Leads\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\ObjectManagerInterface;

class CustomerMigrateCommand extends Command
{
    const CONNECTION = 'custom';
    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    private $customConnection;

    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    private $defaultConection;

    /**
     * @var ObjectManagerInterface
     */
    private $_objectManager;

    /**
     * CustomerMigrateCommand constructor.
     * @param ResourceConnection $resourceConnection
     * @param ObjectManagerInterface $objectManager
     */
    public function __construct(
        ResourceConnection $resourceConnection,
        ObjectManagerInterface $objectManager
    ) {
        parent::__construct();
        $this->customConnection = $resourceConnection->getConnection(self::CONNECTION);
        $this->defaultConection = $resourceConnection->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
        $this->_objectManager   = $objectManager;
    }

    protected function configure()
    {
        $this->setName('skynix:customer_migrate')->setDescription('Command in Magento 2 to connect to the old DB - Customers table and pull this data and add/update new Leads');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dataCustomersMagento1 = $this->getDataOfAllCustomersMagento1DB();

        foreach ($dataCustomersMagento1 as $customer) {
            /** @var \Skynix\Leads\Model\Lead $lead */
            $lead = $this->_objectManager->create(\Skynix\Leads\Model\Lead::class);

            $query = $this->defaultConection
                ->select()
                ->from('skynix_lead_entity', ['entity_id'])
                ->where('skynix_lead_entity.email = ?', $customer['email']);
            $idCustomer = $this->defaultConection->fetchOne($query);

            if($idCustomer) {
                $lead = $lead->load($idCustomer, 'entity_id');
                $output->writeln('Update Data for customer with email ' . $customer['email']);
            } else {
                $output->writeln('Create customer with email ' . $customer['email']);
                $customer['created_at'] = date('Y-m-d H:i:s');
            }

            $lead->setData($customer)->save();
        }
    }

    /**
     * @return array
     */
    private function getDataOfAllCustomersMagento1DB()
    {
        $query = $this->customConnection->select()->from('customer_entity', ['email']);
        $customEntity = $this->customConnection->fetchAll($query);

        if($customEntity) {
            foreach ($customEntity as $key=>$item) {
                /*get data of customer*/
                $item['first_name'] = $this->getCustomerEnityData($item['email'], 'firstname');
                $item['last_name'] = $this->getCustomerEnityData($item['email'], 'lastname');

                /*get data of customer address*/
                $address = $this->getCustomerAddressData($item['email']);
                if($address) {
                    $item['company']    = isset($address['company'])    ? $address['company']   : null;
                    $item['street']     = isset($address['street'])     ? $address['street']    : null;
                    $item['city']       = isset($address['city'])       ? $address['city']      : null;
                    $item['region']     = isset($address['region'])     ? $address['region']    : null;
                    $item['region_id']  = isset($address['region_id'])  ? $address['region_id'] : null;
                    $item['postcode']   = isset($address['postcode'])   ? $address['postcode']  : null;
                    $item['telephone']  = isset($address['telephone'])  ? $address['telephone'] : null;
                }

                $item['newsletter_subscription']  = $this->getCustomerSubscription($item['email']);

                $customEntity[$key] = $item;
            }
        }
        
        return $customEntity;
    }

    /**
     * @param $email
     * @param $parameter
     * @return null|string
     */
    private function getCustomerEnityData($email, $parameter)
    {
        $query = $this->customConnection
            ->select()
            ->from('customer_entity_varchar', ['value'])
            ->joinLeft('customer_entity', 'customer_entity_varchar.entity_id = customer_entity.entity_id', [])
            ->joinLeft('eav_attribute', 'eav_attribute.attribute_id = customer_entity_varchar.attribute_id', [])
            ->where('customer_entity.email = ?', $email)
            ->where('eav_attribute.attribute_code = ?', $parameter);

        $data = $this->customConnection->fetchOne($query);

        return $data ? $data : null;
    }

    /**
     * @param $email
     * @return array|bool
     */
    private function getCustomerAddressData($email)
    {       
        $query = $this->customConnection
            ->select()
            ->from('customer_entity', [])
            ->joinLeft('customer_address_entity_varchar', 'customer_address_entity_varchar.entity_id = customer_entity.entity_id ', ['value'])
            ->joinLeft('eav_attribute', 'eav_attribute.attribute_id = customer_address_entity_varchar.attribute_id', ['attribute_code'])
            ->where('customer_entity.email = ?', $email);
        $data = $this->customConnection->fetchAll($query);
        
        if($data) {
            $customerAddress = [];
            foreach ($data as $item) {
                $customerAddress[$item['attribute_code']] = $item['value'];
            }
            return $customerAddress;
        }       
        
        return false;
    }

    /**
     * @param $email
     * @return bool
     */
    private function getCustomerSubscription($email)
    {
        $query = $this->customConnection
            ->select()
            ->from('newsletter_subscriber')
            ->where('subscriber_email = ?', $email);
        $data = $this->customConnection->fetchOne($query);

        return $data ? true : false;
    }

}