<?php
/**
 * Created by Skynix Team.
 * User: oleksii bulba
 * Date: 20.07.18
 * Time: 20:36
 */

namespace Skynix\Leads\Controller\Adminhtml\Leads;


use Magento\Framework\App\ResponseInterface;

class Delete extends Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Skynix_Leads::leads_manage';

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {

    }
}