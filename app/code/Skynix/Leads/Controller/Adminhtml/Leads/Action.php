<?php
/**
 * Created by Skynix Team.
 * User: oleksii bulba
 * Date: 20.07.18
 * Time: 20:38
 */

namespace Skynix\Leads\Controller\Adminhtml\Leads;


use Magento\Backend\App\Action as BackendAction;

abstract class Action extends BackendAction
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Skynix_Leads::leads';

    /**
     * Is the user allowed to view the leads grid.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed(self::ADMIN_RESOURCE);
    }
}