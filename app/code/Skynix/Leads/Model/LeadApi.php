<?php
/**
 * Created by PhpStorm.
 * User: smarchuk
 * Date: 7/24/18
 * Time: 1:51 PM
 */

namespace Skynix\Leads\Model;

use Skynix\Leads\Api\LeadApiInterface;
use Psr\Log\LoggerInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Catalog\Model\ProductFactory;
class LeadApi implements LeadApiInterface
{
    const HTTP_SUCCESS = 200;
    const HTTP_BAD_REQUEST = 400;
    const HTTP_OK ='ok';
    const HTTP_FAIL = 'fail';

    private $_objectManager;
    protected $logger;
    protected $request;
    public $resourceConnection;
    protected $_productloader;

    public function __construct(
        ObjectManagerInterface $objectManager,
        LoggerInterface $logger,
        Http $request,
        ResourceConnection $resourceConnection,
        ProductFactory $_productloader

    )
    {
        $this->_objectManager = $objectManager;
        $this->logger = $logger;
        $this->request = $request;
        $this->resourceConnection = $resourceConnection;
        $this->_productloader = $_productloader;

    }

    /**
     * Returns answer in JSON format created whether new Lead
     *
     * @api
     * @return
     */
    public function addLead()
    {
        $data = get_object_vars(json_decode($this->request->getContent()));

        if ( !isset($data['email']) || !isset($data['newsletter_subscription'])) {
            $response['status'] = self::HTTP_FAIL;
            $response['code'] = self::HTTP_BAD_REQUEST;
            $response['error_message'] = __('Required fields are empty.');
        } else {

            $leadData = [];
            if(isset($data['product'])) {
                $leadData['product_id'] = $this->getProductIdByName($data['product']);
            }
            if(isset($data['name'])) {
                $name = explode(' ', $data['name']);
                $leadData['first_name'] = $name[0];
                $leadData['last_name']  = isset($name[1]) ? $name[1] : null;
            }
            if(isset($data['organization'])) {
                $leadData['company'] = $data['organization'];
            }
            if(isset($data['street_address'])) {
                $leadData['street'] = $data['street_address'];
            }
            if(isset($data['city'])) {
                $leadData['city'] = $data['city'];
            }
            if(isset($data['state'])) {
                $leadData['region'] = $data['state'];
            }
            if(isset($data['zip'])) {
                $leadData['postcode'] = $data['zip'];
            }
            if(isset($data['phone'])) {
                $leadData['telephone'] = $data['phone'];
            }
            if(isset($data['email'])) {
                $leadData['email'] = $data['email'];
            }
            if(isset($data['method_of_contact'])) {
                $leadData['method_of_contact'] = $data['method_of_contact'];
            }
            if(isset($data['quote_type'])) {
                $leadData['quote_type'] = $data['quote_type'];
            }
            if(isset($data['newsletter_subscription'])) {
                $leadData['newsletter_subscription'] = $data['newsletter_subscription'];
            }
            if(isset($data['notes'])) {
                $leadData['notes'] = $data['notes'];
            }
            if(isset($data['found_via'])) {
                $leadData['found_via'] = $data['found_via'];
            }
            $leadData['created_at'] = date('Y-m-d H:i:s');

            $response['status'] = self::HTTP_OK;
            $lead = $this->_objectManager->create('Skynix\Leads\Model\Lead');
            $lead->setData($leadData);
            $lead->save();
            $response['lead_id'] = $lead->getId();
        }
        return json_encode($response);
    }

    public function fetchLeads()
    {
        $connection = $this->resourceConnection->getConnection();
        $select = $connection->select()
            ->from('skynix_lead_entity');

        $allLeadsBaseContainer = $connection->fetchAll($select);

        if(empty($allLeadsBaseContainer)) {
            return [];
        }

        $allLeadsFullContainer = [];

        foreach($allLeadsBaseContainer as $leadBase) {

            $leadFull['id'] = $leadBase['entity_id'];
            $leadFull['product'] = '';
            $leadFull['name'] = $leadBase['first_name'] . " " . $leadBase['last_name'];
            $leadFull['organization'] = '';
            $leadFull['street_address'] = '';
            $leadFull['city'] = '';
            $leadFull['state'] = '';
            $leadFull['zip'] = '';
            $leadFull['phone'] = '';
            $leadFull['email'] = $leadBase['email'];
            $leadFull['method_of_contact'] = 'phone';
            $leadFull['quote_type'] = 'Playground/Play Features';
            $leadFull['newsletter_subscription'] = true;
            $leadFull['notes'] = '';
            $leadFull['found_via'] ='';


            $leadAttributesSql = $connection->select()
                ->from('skynix_lead_entity_varchar')
                ->joinLeft('eav_attribute', 'skynix_lead_entity_varchar.attribute_id = eav_attribute.attribute_id');

            $leadAttributes = $connection->fetchAll($leadAttributesSql);

            if(!empty($leadAttributes)) {
                foreach ($leadAttributes as $leadAttribute) {
                    if($leadAttribute['attribute_code'] == 'city') {
                        $leadFull['city'] = $leadAttribute['value'];
                    }

                    if($leadAttribute['attribute_code'] == 'region') {
                        $leadFull['state'] = $leadAttribute['value'];
                    }

                    if($leadAttribute['attribute_code'] == 'postcode') {
                        $leadFull['zip'] = $leadAttribute['value'];
                    }

                    if($leadAttribute['attribute_code'] == 'telephone') {
                        $leadFull['phone'] = $leadAttribute['value'];
                    }

                    if($leadAttribute['attribute_code'] == 'street') {
                        $leadFull['street_address'] = $leadAttribute['value'];
                    }

                    if($leadAttribute['attribute_code'] == 'company') {
                        $leadFull['organization'] = $leadAttribute['value'];
                    }
                }
            }

            $allLeadsFullContainer[] = $leadFull;
        }

        $response = json_encode($allLeadsFullContainer);

        return $response;

    }

    public function getProductIdByName($name)
    {
        $product = $this->_productloader->create();
        $product = $product->loadByAttribute('name', $name);
        return $product ? $product->getId() : false;
    }


}