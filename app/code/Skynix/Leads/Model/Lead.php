<?php
/**
 * Created by Skynix Team.
 * User: oleksii bulba
 * Date: 19.07.18
 * Time: 18:04
 */

namespace Skynix\Leads\Model;


use Magento\Framework\Model\AbstractModel;

/**
 * Class Lead
 *
 * @method string getFirstName()
 * @method string getLastName()
 * @method void setName($name)
 * @method string getEmail()
 * @method void setCountryId($id)
 *
 *
 * @package Skynix\Leads\Model
 */
class Lead extends AbstractModel
{
    const ENTITY = 'skynix_lead';
    const COUNTRY_ID = 'US';

    protected function _construct() {
        /* Resource classname */
        $this->_init(ResourceModel\Lead::class);
    }

    public function beforeSave()
    {
        $this->setCountryId(self::COUNTRY_ID);
        return parent::beforeSave();
    }

    protected function _afterLoad()
    {
        if ($this->getId()) {
            $this->setName($this->getFirstName() . ' ' . $this->getLastName());
            $this->setCountryId(self::COUNTRY_ID);

        }
        return parent::_afterLoad();
    }
}