<?php
/**
 * Created by Skynix Team.
 * User: oleksii bulba
 * Date: 19.07.18
 * Time: 18:06
 */

namespace Skynix\Leads\Model\ResourceModel;


use Magento\Eav\Model\Entity\AbstractEntity;

class Lead extends AbstractEntity
{
    protected function _construct() {
        $this->_read = 'skynix_lead_read';
        $this->_write = 'skynix_lead_write';
    }

    public function getEntityType() {
        if(empty($this->_type)) {
            $this->setType(\Skynix\Leads\Model\Lead::ENTITY);
        }

        return parent::getEntityType();
    }
}