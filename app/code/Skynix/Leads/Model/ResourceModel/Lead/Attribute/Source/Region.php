<?php
/**
 * Created by Skynix Team.
 * User: oleksii bulba
 * Date: 20.07.18
 * Time: 11:19
 */

namespace Skynix\Leads\Model\ResourceModel\Lead\Attribute\Source;


use Magento\Directory\Model\ResourceModel\Region\Collection as RegionCollection;
use Magento\Directory\Model\ResourceModel\Region\CollectionFactory as RegionCollectionFactory;
use Magento\Eav\Model\Entity\Attribute\Source\Table;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory as OptionCollectionFactory;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory as AttributeOptionFactory;

class Region extends Table
{
    /**
     * @var CollectionFactory
     */
    protected $_regionsFactory;

    /**
     * @param OptionCollectionFactory $attrOptionCollectionFactory
     * @param AttributeOptionFactory $attrOptionFactory
     * @param RegionCollectionFactory $regionsFactory
     */
    public function __construct(
        OptionCollectionFactory $attrOptionCollectionFactory,
        AttributeOptionFactory $attrOptionFactory,
        RegionCollectionFactory $regionsFactory
    ) {
        $this->_regionsFactory = $regionsFactory;
        parent::__construct($attrOptionCollectionFactory, $attrOptionFactory);
    }

    /**
     * Retrieve all region options
     *
     * @param bool $withEmpty
     * @param bool $defaultValues
     * @return array
     */
    public function getAllOptions($withEmpty = true, $defaultValues = false)
    {
        if (!$this->_options) {
            $this->_options = $this->_createRegionsCollection()->load()->toOptionArray();
        }
        return $this->_options;
    }

    /**
     * @return RegionCollection
     */
    protected function _createRegionsCollection()
    {
        return $this->_regionsFactory->create();
    }
}