<?php
/**
 * Created by Skynix Team.
 * User: oleksii bulba
 * Date: 19.07.18
 * Time: 20:54
 */

namespace Skynix\Leads\Model\ResourceModel\Lead\Attribute\Source;


use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class FoundSource extends AbstractSource
{
    const FOUND_VIA_GOOGLE = 0;
    const FOUND_VIA_BING   = 1;
    const FOUND_VIA_YAHOO  = 2;
    const FOUND_VIA_OTHER  = 3;

    /**
     * Retrieve All options
     *
     * @return array
     */
    public function getAllOptions()
    {
        $this->_options = [
            ['value' => 0, 'label' => 'Google'],
            ['value' => 1, 'label' => 'Bing'],
            ['value' => 2, 'label' => 'Yahoo'],
            ['value' => 3, 'label' => 'Other'],
            //['value' => , 'label' => ''],
        ];
    }

    /**
     * @param $code
     * @return \Magento\Framework\Phrase|null
     */
    public function getTextValueByCode($code)
    {
        if($code !== null && is_numeric($code)) {
            if($code == 0) {
                return  __('Google');
            }
            if($code == 1) {
                return  __('Bing');
            }
            if($code == 2) {
                return  __('Yahoo');
            }
            if($code == 3) {
                return  __('Other');
            }
        }
        return  __('None');
    }
}