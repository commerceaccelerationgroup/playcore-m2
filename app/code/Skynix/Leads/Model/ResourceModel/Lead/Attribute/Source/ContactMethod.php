<?php
/**
 * Created by Skynix Team.
 * User: oleksii bulba
 * Date: 19.07.18
 * Time: 20:41
 */

namespace Skynix\Leads\Model\ResourceModel\Lead\Attribute\Source;


use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class ContactMethod extends AbstractSource
{
    const CONTACT_BY_PHONE = 0;
    const CONTACT_BY_EMAIL = 1;

    /**
     * Retrieve All options for Method of Contact Lead Attribute
     *
     * @return array
     */
    public function getAllOptions()
    {
        $this->_options = [
            ['value' => self::CONTACT_BY_PHONE, 'label' => __('Phone')],
            ['value' => self::CONTACT_BY_EMAIL, 'label' => __('Email')],
        ];

        return $this->_options;
    }

    /**
     * @param $code
     * @return \Magento\Framework\Phrase|null
     */
    public function getTextValueByCode($code)
    {
        if($code !== null && is_numeric($code)) {
            if($code == 0) {
                return  __('Phone');
            }
            if($code == 1) {
                return  __('Email');
            }
        }
        return __('None');
    }
}