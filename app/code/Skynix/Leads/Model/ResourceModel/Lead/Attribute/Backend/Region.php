<?php
/**
 * Created by Skynix Team.
 * User: oleksii bulba
 * Date: 20.07.18
 * Time: 11:17
 */

namespace Skynix\Leads\Model\ResourceModel\Lead\Attribute\Backend;


use Magento\Directory\Model\RegionFactory;
use Magento\Eav\Model\Entity\Attribute\Backend\AbstractBackend;

class Region extends AbstractBackend
{
    /**
     * @var RegionFactory
     */
    protected $_regionFactory;

    /**
     * @param RegionFactory $regionFactory
     */
    public function __construct(RegionFactory $regionFactory)
    {
        $this->_regionFactory = $regionFactory;
    }

    /**
     * Prepare object for save
     *
     * @param \Magento\Framework\DataObject $object
     * @return $this
     */
    public function beforeSave($object)
    {
        $region = $object->getData('region');
        if (is_numeric($region)) {
            $regionModel = $this->_createRegionInstance();
            $regionModel->load($region);
            if ($regionModel->getId() && $object->getCountryId() == $regionModel->getCountryId()) {
                $object->setRegionId($regionModel->getId())->setRegion($regionModel->getName());
            }
        }
        return $this;
    }

    /**
     * @return \Magento\Directory\Model\Region
     */
    protected function _createRegionInstance()
    {
        return $this->_regionFactory->create();
    }
}