<?php
/**
 * Created by Skynix Team.
 * User: oleksii bulba
 * Date: 19.07.18
 * Time: 21:08
 */

namespace Skynix\Leads\Model\ResourceModel\Lead\Attribute\Backend\Data;


use Magento\Eav\Model\Entity\Attribute\Backend\AbstractBackend;
use Skynix\Leads\Model\Lead;

class Boolean extends AbstractBackend
{
    /**
     * Prepare data before attribute save
     *
     * @param Lead $lead
     * @return $this
     */
    public function beforeSave($lead)
    {
        $attributeName = $this->getAttribute()->getName();
        $inputValue = $lead->getData($attributeName);
        $inputValue = $inputValue === null ? $this->getAttribute()->getDefaultValue() : $inputValue;
        $sanitizedValue = !empty($inputValue) ? '1' : '0';
        $lead->setData($attributeName, $sanitizedValue);
        return $this;
    }

    /**
     * @param $code
     * @return \Magento\Framework\Phrase|null
     */
    public function getTextValueByCode($code)
    {
        if($code !== null && is_numeric($code)) {
            if($code == 0) {
                return  __('NO');
            }
            if($code == 1) {
                return  __('YES');
            }
        }
        return __('None');
    }
}