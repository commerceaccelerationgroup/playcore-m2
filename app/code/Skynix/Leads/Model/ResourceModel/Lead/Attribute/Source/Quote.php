<?php
/**
 * Created by Skynix Team.
 * User: oleksii bulba
 * Date: 19.07.18
 * Time: 20:45
 */

namespace Skynix\Leads\Model\ResourceModel\Lead\Attribute\Source;


use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class Quote extends AbstractSource
{
    const QUOTE_TYPE_PLAYGROUND = 0;
    const QUOTE_TYPE_THEMING    = 1;
    const QUOTE_TYPE_BOTH       = 2;

    /**
     * Retrieve All options
     *
     * @return array
     */
    public function getAllOptions()
    {
        $this->_options = [
            ['value' => self::QUOTE_TYPE_PLAYGROUND, 'label' => __('Playground/Play Features')],
            ['value' => self::QUOTE_TYPE_THEMING,    'label' => __('Theming')],
            ['value' => self::QUOTE_TYPE_BOTH,       'label' => __('Combination of Theming and Playing')],
        ];
    }

    /**
     * @param $code
     * @return \Magento\Framework\Phrase|null
     */
    public function getTextValueByCode($code)
    {
        if($code !== null && is_numeric($code)) {
            if($code == 0) {
                return  __('Playground/Play Features');
            }
            if($code == 1) {
                return  __('Theming');
            }
            if($code == 2) {
                return  __('Combination of Theming and Playing');
            }
        }
        return __('None');
    }
}