<?php
/**
 * Created by Skynix Team.
 * User: oleksii bulba
 * Date: 19.07.18
 * Time: 20:17
 */

namespace Skynix\Leads\Model\ResourceModel\Lead\Attribute\Source;

use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class Product extends AbstractSource
{
    protected $productCollectionFactory;

    public function __construct(
        ProductCollectionFactory $productCollectionFactory
    ) {
        $this->productCollectionFactory = $productCollectionFactory;
    }

    /**
     * Retrieve All options
     *
     * @param bool $withEmpty
     * @return array
     */
    public function getAllOptions($withEmpty = true)
    {
        $productCollection = $this->productCollectionFactory
            ->create()
            ->addAttributeToSelect('id')
            ->addAttributeToSelect('name');

        foreach ($productCollection as $product) {
            /**
             * @var $product \Magento\Catalog\Model\Product
             */
            $this->_options[] = [
                'value' => $product->getId(),
                'label' => $product->getName(),
            ];
        }

        if ($withEmpty) {
            if (!$this->_options) {
                return [['value' => '0', 'label' => __('None')]];
            } else {
                return array_merge([['value' => '0', 'label' => __('None')]], $this->_options);
            }
        }

        return $this->_options;
    }
}