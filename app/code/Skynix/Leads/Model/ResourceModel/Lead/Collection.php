<?php
/**
 * Created by Skynix Team.
 * User: oleksii bulba
 * Date: 19.07.18
 * Time: 18:07
 */

namespace Skynix\Leads\Model\ResourceModel\Lead;


use Magento\Eav\Model\Entity\Collection\AbstractCollection;
use Skynix\Leads\Model\ResourceModel\Lead as LeadResourceModel;
use Skynix\Leads\Model\Lead as LeadModel;

class Collection extends AbstractCollection
{
    protected function _construct() {
        /*
         * Model classname,
         * Resource classname
         */
        $this->_init(
            LeadModel::class,
            LeadResourceModel::class
        );
    }
}