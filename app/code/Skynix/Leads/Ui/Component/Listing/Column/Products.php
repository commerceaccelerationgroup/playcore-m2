<?php
/**
 * Created by Skynix Team.
 * User: oleksii bulba
 * Date: 20.07.18
 * Time: 17:43
 */

namespace Skynix\Leads\Ui\Component\Listing\Column;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\App\ResourceConnection;
use Skynix\Leads\Model\ResourceModel\Lead\Attribute\Source\ContactMethod;
use Skynix\Leads\Model\ResourceModel\Lead\Attribute\Source\Quote;
use Skynix\Leads\Model\ResourceModel\Lead\Attribute\Backend\Data\Boolean as DataBoolean;
use Skynix\Leads\Model\ResourceModel\Lead\Attribute\Source\FoundSource;

class Products extends Column
{
    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * @var ContactMethod
     */
    private $contactMethod;

    /**
     * @var Quote
     */
    private $quote;

    /**
     * @var DataBoolean
     */
    private $boolean;

    /**
     * @var FoundSource
     */
    private $foundSource;

    /**
     * @var ResourceConnection
     */
    private $connection;

    /**
     * Products constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param ProductFactory $productFactory
     * @param array $components
     * @param array $data
     * @param ResourceConnection $resourceConnection
     * @param ContactMethod $contactMethod
     * @param Quote $quote
     * @param DataBoolean $boolean
     * @param FoundSource $foundSource
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        ProductFactory $productFactory,
        array $components = [],
        array $data = [],
        ResourceConnection $resourceConnection,
        ContactMethod $contactMethod,
        Quote $quote,
        DataBoolean $boolean,
        FoundSource $foundSource
    ) {
        $this->productFactory = $productFactory;
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->connection = $resourceConnection->getConnection();
        $this->contactMethod = $contactMethod;
        $this->quote = $quote;
        $this->boolean = $boolean;
        $this->foundSource = $foundSource;
    }

    /**
     * {@inheritdoc}
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {

                /* Name*/
                if(isset($item['first_name'])) {
                    $item['name'] = $item['first_name'];

                    if(isset($item['last_name'])) {
                        $item['name'] = $item['first_name'] . ' ' . $item['last_name'];
                    }
                }

                $dataAttrSkynixLeadEntityVarchar = $this->getAttrSkynixLeadEntityVarchar($item['entity_id']);
                $dataAttrSkynixLeadEntityInt = $this->getAttrSkynixLeadEntityInt($item['entity_id']);
                $item = array_merge($item, $dataAttrSkynixLeadEntityVarchar, $dataAttrSkynixLeadEntityInt);

                isset($item['product']) ? $product  = $item['product'] : $product = null;
                $item['product'] = $this->getProductName($product);

                isset($item['method_of_contact']) ? $methodContact = $item['method_of_contact'] : $methodContact = null;
                $item['method_of_contact'] = $this->contactMethod->getTextValueByCode($methodContact);

                isset($item['quote_type']) ? $quoteType = $item['quote_type'] : $quoteType = null;
                $item['quote_type'] = $this->quote->getTextValueByCode($quoteType);

                isset($item['newsletter_subscription']) ? $newsletterSubscription = $item['newsletter_subscription'] : $newsletterSubscription = null;
                $item['newsletter_subscription'] = $this->boolean->getTextValueByCode($newsletterSubscription);

                isset($item['found_via']) ? $foundVia = $item['found_via'] : $foundVia = null;
                $item['found_via'] = $this->foundSource->getTextValueByCode($foundVia);

            }
        }

        return $dataSource;
    }

    /**
     * Retrieves product name by its id
     *
     * @param $product_id
     * @return \Magento\Framework\Phrase|string
     */
    private function getProductName($product_id)
    {
        /**
         * @var $product Product
         */
        $product = $this->productFactory->create()->load($product_id);
        if ($product->getId()) {
            return $product->getName();
        }
        return __('None');
    }

    /**
     * @param $entityId
     * @return array
     */
    private function getAttrSkynixLeadEntityVarchar($entityId)
    {
        /*Attribute from skynix_lead_entity_varchar*/
        $query = $this->connection->select()
            ->from('skynix_lead_entity_varchar')
            ->joinLeft('eav_attribute', 'skynix_lead_entity_varchar.attribute_id = eav_attribute.attribute_id')
            ->where('skynix_lead_entity_varchar.entity_id = ?', $entityId);

        $dataAttr = $this->connection->fetchAll($query);
        $item = [];
        if(!empty($dataAttr)) {
            foreach ($dataAttr as $attr) {

                if($attr['attribute_code'] == 'company') {
                    $item['organization'] = $attr['value'];
                }
                if($attr['attribute_code'] == 'street') {
                    $item['street_address'] = $attr['value'];
                }
                if($attr['attribute_code'] == 'city') {
                    $item['city'] = $attr['value'];
                }
                if($attr['attribute_code'] == 'region') {
                    $item['state'] = $attr['value'];
                }
                if($attr['attribute_code'] == 'postcode') {
                    $item['zip'] = $attr['value'];
                }
                if($attr['attribute_code'] == 'telephone') {
                    $item['phone'] = $attr['value'];
                }
                if($attr['attribute_code'] == 'notes') {
                    $item['notes'] = $attr['value'];
                }
                if($attr['attribute_code'] == 'found_via') {
                    $item['found_via'] = $attr['value'];
                }
                if($attr['attribute_code'] == 'product_id') {
                    $item['product'] = $attr['value'];
                }

            }
        }
        return $item;
    }

    /**
     * @param $entityId
     * @return array
     */
    private function getAttrSkynixLeadEntityInt($entityId)
    {
        /*Attribute from skynix_lead_entity_int*/
        $query = $this->connection->select()
            ->from('skynix_lead_entity_int')
            ->joinLeft('eav_attribute', 'skynix_lead_entity_int.attribute_id = eav_attribute.attribute_id')
            ->where('skynix_lead_entity_int.entity_id = ?', $entityId);

        $dataAttr = $this->connection->fetchAll($query);
        $item = [];
        if(!empty($dataAttr)) {
            foreach ($dataAttr as $attr) {

                if($attr['attribute_code'] == ' ') {
                    $item['method_of_contact'] = $attr['value'];
                }
                if($attr['attribute_code'] == 'quote_type') {
                    $item['quote_type'] = $attr['value'];
                }
                if($attr['attribute_code'] == 'newsletter_subscription') {
                    $item['newsletter_subscription'] = $attr['value'];
                }
            }
        }
        return $item;
    }
}