<?php
/**
 * Created by PhpStorm.
 * User: smarchuk
 * Date: 7/24/18
 * Time: 10:32 AM
 */

namespace Skynix\Leads\Api;


interface LeadApiInterface
{
    /**
     * Returns answer in JSON format created whether new Lead
     *
     * @api
     * @return mixed
     */
    public function addLead();

    /**
     * Returns Leads in JSON format
     *
     * @api
     * @return mixed
     */
    public function fetchLeads();
}
